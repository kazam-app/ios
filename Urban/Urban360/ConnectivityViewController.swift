//
//  NetworkViewController.swift
//  Urban360
//
//  Created by Hector Maya on 30/09/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationActivateDelegate {
    func activateLocationSuccess()
}

class ConnectivityViewController: UIViewController {
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnScanCode: UIButton!
    
    var locationManager: CLLocationManager!
    var delegate: LocationActivateDelegate!
    
    override func viewDidLoad() {
        self.view.alpha = 0.0
        if btnLocation != nil{
            btnLocation.layer.borderColor = UIColor.blue.cgColor
            btnLocation.layer.borderWidth = 0.5
            btnLocation.layer.cornerRadius = 7.0
        }
        
        if btnScanCode != nil{
            btnScanCode.layer.cornerRadius = 7.0
        }
        
        self.view.fadeIn(withDuration: 0.5)
    }
    
    
    @IBAction func locationAction(_ sender: Any) {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .denied:
                print("Denied")
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                break
            case .notDetermined:
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.requestAlwaysAuthorization()
                break
            default:
                break
            }
        } else {
            if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    @IBAction func scanCodeAction(_ sender: Any) {
        let main = UIStoryboard(name: "PunchsCards", bundle: nil)
        let cameraVC  = main.instantiateViewController(withIdentifier: "CameraAccessVC") as! CameraAccessVC
        self.present(cameraVC, animated: true, completion: nil)
    }
    
    //ConnectivityViewControllerNetwork
    //ConnectivityViewControllerLocation
    //ConnectivityViewControllerBluetooth
}

extension ConnectivityViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse{
            if delegate != nil{
                delegate.activateLocationSuccess()
            }
        }
    }
}
