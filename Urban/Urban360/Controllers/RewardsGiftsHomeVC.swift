//
//  RewardsGiftsHomeVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/22/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class RewardsGiftsHomeVC: UIViewController {
    // MARK : - Properties
    let usrDefaults = UserDefaults.standard
    var activityIndicator : NVActivityIndicatorView!
    var punchsCards:[PunchCardListModel] = []
    var userDic:[String:AnyObject]! = [:]
    var currentMerchant:MerchantModel!
    //MARK: -Outlets
    
    @IBOutlet weak var tblPunchCards: UITableView!
    @IBOutlet weak var optSelected: UISegmentedControl!
    
    @IBOutlet weak var noAccountV: UIView!
    @IBOutlet weak var emptyRewardsV: UIView!
    @IBOutlet weak var lblMessageHeader: UILabel!
    @IBOutlet weak var spaceToHeaderCst: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
        
        if let  userDicTmp = usrDefaults.dictionary(forKey: "userSession"){
            if optSelected.selectedSegmentIndex == 0{
                tblPunchCards.fadeIn(withDuration: 0.25)
                noAccountV.fadeOut(withDuration: 0)
                userDic = userDicTmp as [String : AnyObject]
                if self.validateReachability(){
                    loadAllPunchsCards()
                }else{
                    self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
                    loadRewards()
                }
            }
        }else{
            tblPunchCards.fadeOut(withDuration: 0.25)
            noAccountV.fadeIn(withDuration: 0.5)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoMerchant" {
            let destinationVC = segue.destination as! MerchantDetailVC
            destinationVC.isNear = false
            destinationVC.currentMerchant = currentMerchant
        }

    }
    
    @IBAction func changeAction(_ sender: Any) {
         if !userDic.isEmpty{
            if optSelected.selectedSegmentIndex == 0{
                self.tblPunchCards.fadeIn(withDuration: 0)
                self.emptyRewardsV.fadeOut(withDuration: 0.25)
                UIView.animate(withDuration: 0.5, animations: {
                    self.spaceToHeaderCst.constant = 59
                    self.lblMessageHeader.alpha = 1
                    self.view.layoutIfNeeded()
                }, completion: { complete in
                    
                    if self.validateReachability(){
                        self.loadAllPunchsCards()
                    }
                    else{
                        self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
                    }
                })
            }
            else{
                UIView.animate(withDuration: 0.5, animations: {
                    self.spaceToHeaderCst.constant = 8
                    self.lblMessageHeader.alpha = 0
                    self.view.layoutIfNeeded()
                }, completion: { complete in
                    if self.validateReachability(){
                        self.loadRewards()
                    }
                    else{
                        self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
                    }
                })
                
            }
        }
         else{
            tblPunchCards.fadeOut(withDuration: 0.25)
            noAccountV.fadeIn(withDuration: 0.5)
        }
        
    }
    
    @IBAction func signupFBAction(_ sender: Any) {
        SignupBO.singupFB(controller: self)
    }
    
    @IBAction func signupMailAction(_ sender: Any) {
        SignupBO.signupMail(controller: self)
    }
    
    func loadAllPunchsCards(){
        self.activityIndicator.startAnimating()
        PunchsCardsManager.getAllPunchsCards(){ results, error in
            self.activityIndicator.stopAnimating()
            if error == nil {
                if results != nil {
                    self.punchsCards = results!
                    self.tblPunchCards.reloadData {
                    }
                    
                  
                }else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar obtener las tarjetas")
                }
            }
            else{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "Ocurrio un problema intenta de nuevo")
            }
        }
    }
    func loadRewards()  {
        self.activityIndicator.startAnimating()
        PunchsCardsManager.getAllPunchsCardsCompleted(){ results, error in
            self.activityIndicator.stopAnimating()
            if error == nil {
                if results != nil {
                    self.punchsCards = results!
                    self.tblPunchCards.reloadData {
                       // let height:CGFloat = self.tblPunchCards.contentSize.height
                       /// self.tblPunchCards.frame = CGRect(x: self.tblPunchCards.frame.origin.x, y: self.tblPunchCards.frame.origin.y, width: self.tblPunchCards.frame.size.width, height:height)
                       // self.view.layoutIfNeeded()
                    }
                    
                }else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar obtener las tarjetas")
                }
            }
            else{
                //self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "Ocurrio un problema intenta de nuevo")
                self.tblPunchCards.fadeOut(withDuration: 0.25)
                self.emptyRewardsV.fadeIn(withDuration: 0.5)
            }
        }
    }
    
    func getMerchantByPunchCard(merchantId:String) {
        self.activityIndicator.startAnimating()
        MerchantManager.merchantById(merchantId: merchantId){ currentMerchant, error in
            self.activityIndicator.stopAnimating()
            if error == nil{
                if currentMerchant != nil {
                    self.currentMerchant = currentMerchant!
                    self.performSegue(withIdentifier: "gotoMerchant", sender: self)
                }
            }
            
        }
    }
}
extension RewardsGiftsHomeVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentPunchCard = punchsCards[indexPath.row]
        print("Current merchant: \(currentPunchCard.merchantId!)")
        getMerchantByPunchCard(merchantId: currentPunchCard.merchantId!)
    }
    
}
extension RewardsGiftsHomeVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return punchsCards.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "punchCardCell") as! PunchCardTVC
        if optSelected.selectedSegmentIndex == 0{
            cell.setupCell(punchCard: punchsCards[indexPath.row], isCompleted: false)
            return cell
        }
        else{
            cell.setupCell(punchCard: punchsCards[indexPath.row], isCompleted: true)
            return cell
        }
        
    }
}
extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}
