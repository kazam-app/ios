//
//  RegisterUserLastVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/26/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Eureka
import NVActivityIndicatorView
class RegisterUserLastVC: FormViewController,UITextFieldDelegate {

    //MARK: - Properties
    var userLoginFacebook: UserModel!
    var currentUser:UserModel!
    var isSocialSignUp:Bool = false
    var activityIndicator : NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if userLoginFacebook != nil{
            isSocialSignUp = true
            addButtonOnNaviationBarLeft(title: "cancelActionKey".localized)
        }
        createForm()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoLocation"{
            let destinationVC = segue.destination as! LocationActivateVC
            destinationVC.currentUser = currentUser
        }
    }
    
    
    @IBAction func signupAction(_ sender: Any) {
        validateForm()
    }
    override func nextAction() {
         self.dismiss(animated: true, completion: nil)
    }
    func createForm() {
         form +++ Section()
            <<< EmailRow() {
                $0.tag  = "email"
                $0.placeholder = "Correo electrónico"
                $0.hidden = isSocialSignUp ? false : true
                $0.value = isSocialSignUp ? userLoginFacebook.email : ""
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleEmail())
                $0.validationOptions = .validatesOnChangeAfterBlurred
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Email")
                    //cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.cell.textField.textColor = .black
                        
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        row.placeholderColor = .red
                        row.placeholder = "Correo requerido"
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.placeholderColor = .gray
                        row.placeholder = "Correo electrónico"
                        row.cell.textField.textColor = .black
                        
                    }
            }
         <<< NameRow() {
         $0.tag  = "name"
         $0.value = isSocialSignUp ? userLoginFacebook.name : ""
         $0.placeholder = "Nombre"
         $0.placeholderColor = .gray
         $0.add(rule: RuleRequired())
         $0.validationOptions = .validatesOnChange
         }
         .cellSetup { cell, row in
         cell.imageView?.image = #imageLiteral(resourceName: "user")
         cell.row.updateCell()
         }
            .cellUpdate { cell, row in
                if !row.isValid {
                    row.cell.textField.textColor = .red
                }
                else{
                    row.cell.textField.textColor = .black
                    
                }
            }
            .onRowValidationChanged { cell, row in
                let rowIndex = row.indexPath!.row
                while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                    row.section?.remove(at: rowIndex + 1)
                }
                if !row.isValid {
                    row.placeholderColor = .red
                    row.placeholder = "Nombre requerido"
                    row.cell.textField.textColor = .red
                }
                else{
                    row.placeholderColor = .gray
                    row.placeholder = "Nombre"
                    row.cell.textField.textColor = .black
                    
                }
            }
         
         <<< TextRow(){
         $0.tag  = "lastnames"
         $0.value = isSocialSignUp ? userLoginFacebook.lastNames : ""
         $0.placeholder = "Apellido"
         $0.placeholderColor = .gray
         $0.add(rule: RuleRequired())
         $0.validationOptions = .validatesOnChange
         }
         .cellSetup { cell, row in
         cell.imageView?.image = #imageLiteral(resourceName: "user")
         cell.imageView?.backgroundColor = .clear
         cell.row.updateCell()
         }
            .cellUpdate { cell, row in
                if !row.isValid {
                    row.cell.textField.textColor = .red
                }
                else{
                    row.cell.textField.textColor = .black
                }
            }
            .onRowValidationChanged { cell, row in
                let rowIndex = row.indexPath!.row
                while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                    row.section?.remove(at: rowIndex + 1)
                }
                if !row.isValid {
                    row.placeholderColor = .red
                    row.placeholder = "Apellido requeridos"
                    row.cell.textField.textColor = .red
                }
                else{
                    row.placeholderColor = .gray
                    row.placeholder = "Nombre"
                    row.cell.textField.textColor = .black
                    
                }
            }

         <<< DateRow(){
         $0.tag  = "birthDay"
         $0.title = "Fecha de Nacimiento"
         $0.value = Date()
         let formatter = DateFormatter()
         //formatter.locale = .current
         formatter.dateFormat = "dd'/'MM'/'yyyy"
         $0.dateFormatter = formatter
         }
         .cellSetup { cell, row in
         cell.imageView?.image = #imageLiteral(resourceName: "Birthday")
         }
         
         <<<  ActionSheetRow<String>(){
         $0.tag  = "gender"
         $0.title = "Genero"
         $0.selectorTitle = "Seleccione Género"
         $0.options = ["Hombre","Mujer"]
         }
         .cellSetup { cell, row in
         cell.imageView?.image = #imageLiteral(resourceName: "Gender")
         }
    }
    
    func createAccount(currentUser:UserModel)  {
        self.activityIndicator?.startAnimating()
        AccountManager.validateUser(user: currentUser){ isValid, error in
            self.activityIndicator?.stopAnimating()
            if error == nil {
                if isValid!{
                    self.performSegue(withIdentifier: "gotoLocation", sender: self)
                }else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar crear el usuario por favor verifica e intenta nuevamente")
                }
            }
            else{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "No se pudo crear el usuario el correo ya existe por favor verifica e intenta de nuevo")
                //self.performSegue(withIdentifier: "gotoLocation", sender: self)
            }
        }
    }

    func validateForm(){
        let formData = form.values()
        if isSocialSignUp {
            let validateFormErrors = form.validate()
            if !validateFormErrors.isEmpty{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized, message: "fieldRequiredKey".localized)
            }
            else{
                currentUser = userLoginFacebook
                let birthDay = formData["birthDay"] as? Date
                currentUser.birthDate = birthDay?.stDateString(format: STDateFormat.date_only_signup)
                currentUser.name = formData["name"] as? String
                currentUser.email = formData["email"] as? String
                currentUser.lastNames = formData["lastnames"] as? String
                currentUser.gender = formData["gender"] as? String
                createAccount(currentUser: currentUser)
            }
        }
        else{
            let validateFormErrors = form.validate()
            if !validateFormErrors.isEmpty{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized, message: "fieldRequiredKey".localized)
            }
            else{
                
                let birthDay = formData["birthDay"] as? Date
                currentUser.birthDate = birthDay?.stDateString(format: STDateFormat.date_only_signup)
                currentUser.name = formData["name"] as? String
                currentUser.lastNames = formData["lastnames"] as? String
                currentUser.gender = formData["gender"] as? String
                createAccount(currentUser: currentUser)
            }
        }
    }
}
