//
//  MerchantDetailVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/29/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import AlamofireImage
import NVActivityIndicatorView
class MerchantDetailVC: UIViewController {
    
    // MARK: -Properties
    var currentMerchant:MerchantModel!
    var currentMerchantNear:MerchantLocationModel!
    var isNear:Bool = true
    let usrDefaults = UserDefaults.standard
    var activityIndicator : NVActivityIndicatorView!
    var userDic:[String:AnyObject]! = [:]
    var shops:[MerchantLocationModel] = []
    // MARK: -Outlets
    @IBOutlet weak var HeaderDetail: UIView!
    @IBOutlet weak var imgLogoMerchant: UIImageView!
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var lblMerchantCategory: UILabel!
    @IBOutlet weak var lblMerchantRaiting: UILabel!
    @IBOutlet weak var optMerchants: UISegmentedControl!
    @IBOutlet weak var punchCardV: UIView!
    @IBOutlet weak var containerMerchants: UIView!
    @IBOutlet weak var tblMerchants: UITableView!
    @IBOutlet weak var lblHeaderName: UILabel!
    
    @IBOutlet weak var containerNoAccountV: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isTranslucent = false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
        if self.validateReachability(){
            if let  userDicTmp = usrDefaults.dictionary(forKey: "userSession"){
                if optMerchants.selectedSegmentIndex == 0{
                    containerMerchants.fadeOut(withDuration: 0.25)
                    containerNoAccountV.fadeOut(withDuration: 0)
                    punchCardV.fadeIn(withDuration: 0.5)
                    userDic = userDicTmp as [String : AnyObject]
                    //loadAllPunchsCards()
                }
                else{
                    containerNoAccountV.fadeOut(withDuration: 0.25)
                    punchCardV.fadeIn(withDuration: 0.25)
                    containerMerchants.fadeIn(withDuration: 0.25)
                }
            }
            else{
                punchCardV.fadeOut(withDuration: 0.25)
                containerMerchants.fadeOut(withDuration: 0.25)
                containerNoAccountV.fadeIn(withDuration: 0.5)
            }
        }
        
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        if isNear{
            //navigationController?.navigationBar.barTintColor = currentMerchantNear.merchantColor
            if let color:String = currentMerchantNear.merchantColor {
                HeaderDetail.backgroundColor = UIColor(hexString: color)
                navigationController?.navigationBar.barTintColor = UIColor(hexString: color)
            }
        }else{
            if let color:String = currentMerchant.color {
                HeaderDetail.backgroundColor = UIColor(hexString: color)
                navigationController?.navigationBar.barTintColor = UIColor(hexString: color)
            }
        }
    }


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = UIColor.uiCornflowerBlue
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.black]
    }



    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoPunchsCards" {
            let destinationVC = segue.destination as! PunchCardsViewController
            if currentMerchant == nil &&  currentMerchantNear != nil{
                currentMerchant = MerchantModel()
                currentMerchant.budgetRating = currentMerchantNear.merchantBudgetRaiting
                currentMerchant.category = currentMerchantNear.category
                currentMerchant.merchantId = currentMerchantNear.merchantId
                currentMerchant.name = currentMerchantNear.name
                currentMerchant.urlImage = currentMerchantNear.urlImage
                currentMerchant.color = currentMerchantNear.merchantColor
            }
            
            destinationVC.merchant = currentMerchant
            destinationVC.isEmmbed = true
        }
    }
    
    
    @IBAction func changeAction(_ sender: Any) {
        if !userDic.isEmpty{
            if optMerchants.selectedSegmentIndex == 0{
                lblHeaderName.text = "Mis Sellos y Premios"
                containerMerchants.fadeOut(withDuration: 0.25)
                containerNoAccountV.fadeOut(withDuration: 0)
                self.punchCardV.fadeIn(withDuration: 0.5)
                if self.validateReachability(){
                    //self.loadAllPunchsCards()
                }
                else{
                    self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
                }
            }
            else{
                lblHeaderName.text = "Tiendas Participantes"
                punchCardV.fadeOut(withDuration: 0.25)
                containerNoAccountV.fadeOut(withDuration: 0.25)
                containerMerchants.fadeIn(withDuration: 0.5)
                if self.validateReachability(){
                    self.loadMerchants()
                }
                else{
                    self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
                }
            }
        }
        else{
            containerNoAccountV.fadeIn(withDuration: 0.5)
        }
    }
    
    @IBAction func signupFBAction(_ sender: Any) {
        SignupBO.singupFB(controller: self)
    }
    @IBAction func signupMailAction(_ sender: Any) {
        SignupBO.signupMail(controller: self)
    }
    
    
    func setupData(){
        if isNear{
            lblMerchantName.text = currentMerchantNear.merchantName
            lblMerchantCategory.text = currentMerchantNear.category
            lblMerchantRaiting.attributedText = "$$$$$".rateText(rate: currentMerchantNear.merchantBudgetRaiting ?? 0, colorHighlight: UIColor.white)
            let imageFilter = CircleFilter()
            if currentMerchantNear.urlImage! != ""{
                self.imgLogoMerchant.af_setImage(
                    withURL: URL(string: currentMerchantNear.urlImage!)!,
                    placeholderImage: UIImage(named: "MerchantLogoDetailPH"),
                    filter: imageFilter,
                    imageTransition: .crossDissolve(0.2)
                )
            }
            else{
                self.imgLogoMerchant.image = UIImage(named: "MerchantLogoDetailPH")
            }
        }
        else{
            lblMerchantName.text = currentMerchant.name
            lblMerchantCategory.text = currentMerchant.category
            lblMerchantRaiting.attributedText = "$$$$$".rateText(rate: currentMerchant.budgetRating ?? 0, colorHighlight: UIColor.white)
            
            let imageFilter = CircleFilter()
            if currentMerchant.urlImage! != ""{
                self.imgLogoMerchant.af_setImage(
                    withURL: URL(string: currentMerchant.urlImage!)!,
                    placeholderImage: UIImage(named: "MerchantLogoDetailPH"),
                    filter: imageFilter,
                    imageTransition: .crossDissolve(0.2)
                )
            }
            else{
                self.imgLogoMerchant.image = UIImage(named: "MerchantLogoDetailPH")
            }

        }
    }
    func loadMerchants(){
        self.activityIndicator.startAnimating()
        var merchantId:String = ""
        if isNear{
            merchantId = currentMerchantNear.merchantId!
        }
        else{
            merchantId = currentMerchant.merchantId!
            
        }
        MerchantManager.getListShopsByMerchant(merchantId: merchantId){ results, error in
            self.activityIndicator.stopAnimating()
            if error == nil{
                self.shops = results!
                self.tblMerchants.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .fade)
            }
            else{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar obtener las tiendas")
            }
        }

    }
    
}

extension MerchantDetailVC:UITableViewDelegate{
    
}
extension MerchantDetailVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shops.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shopCell") as! ShopTVC
        if isNear{
            let shop = shops[indexPath.row]
            shop.merchantName = currentMerchantNear.name
            cell.setupCell(shop: shop)
        }
        else{
            let shop = shops[indexPath.row]
            shop.merchantName = currentMerchant.name
            cell.setupCell(shop: shop)
        }
        return cell
    }
}
