//
//  NewsVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/17/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class NewsVC: UIViewController {
    //MARvar- Properties
    var news:[NewModel] = []
    var selectedNews:NewModel! = nil
    var activityIndicator : NVActivityIndicatorView? = nil
    let refreshControl = UIRefreshControl()
    //MARK: - Outlets
    @IBOutlet weak var tblNews: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        refreshControl.attributedTitle = NSAttributedString(string: "recargando noticias")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        tblNews.addSubview(refreshControl) // not required when using UITableViewController
        refreshControl.autoresizingMask = ([UIViewAutoresizing.flexibleRightMargin, UIViewAutoresizing.flexibleLeftMargin])
        self.activityIndicator = activityIndicator(view: self.view)
        if self.validateReachability(){
             loadNews()
        }
        else{
            self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
            tblNews.fadeOut(withDuration: 0.25)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadOnboarding()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh() {
        // Code to refresh table view
        refreshControl.beginRefreshing()
        if self.validateReachability(){
            loadNews()
        }
        else{
            self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
            tblNews.fadeOut(withDuration: 0.25)
        }

        refreshControl.endRefreshing()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
  /*  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "gotoDetail" {
            let destinationVC = segue.destination as! NewsDetailVC
            destinationVC.detailNews = self.selectedNews
        }
    }*/
    
    func loadNews()  {
        self.tblNews.fadeIn(withDuration: 0.25)
        self.activityIndicator?.startAnimating()
        NewsManager.getNews(completion: {results,error in
            self.activityIndicator?.stopAnimating()
            if error == nil{
                if !results!.isEmpty{
                 self.news = results!
                 self.tblNews.reloadData()
                }
            }
            else{
                self.tblNews.fadeOut(withDuration: 0.25)
                self.showErrorMessageGeneral(title: "GeneralErrorTitleKey".localized, icon: "GeneralErrorIconKey".localized, descriptionMessage: "GeneralErrorMessageKey".localized, titleKeyButton: nil, showButton: true)
            }
        })
    }
    override func  reloadData() {
        if self.validateReachability(){
            loadNews()
        }
        else{
            self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
            tblNews.fadeOut(withDuration: 0.25)
        }

    }
    func loadOnboarding(){
        let usrDefaults = UserDefaults.standard
        if !usrDefaults.bool(forKey: "onboardingNews"){
            let onboardingVC = self.storyboard?.instantiateViewController(withIdentifier: "OnBoardingNewsVC") as! OnBoardingVC
            self.present(onboardingVC, animated: true, completion: nil)
        }
    }
}

extension NewsVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedNews = self.news[indexPath.row]
        let detailNewsVC = self.storyboard?.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
        detailNewsVC.detailNews = self.selectedNews
        self.navigationController?.pushViewController(detailNewsVC, animated: true)
    }
}
extension NewsVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return    news.count
    }    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell") as! NewsTVC
        cell.setupCell(news: news[indexPath.row])
        return cell
    }
}
