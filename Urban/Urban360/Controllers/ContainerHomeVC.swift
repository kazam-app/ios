//
//  ContainerHomeVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/12/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

class ContainerHomeVC: UIViewController {
    
    var merchant:MerchantModel!
    var isBluetoothScan = true

    //Manipulating container views
    fileprivate weak var viewController : UIViewController!
    //Keeping track of containerViews
    fileprivate var containerViewObjects = Dictionary<String,UIViewController>()
    /** Specifies which ever container view is on the front */
    open var currentViewController : UIViewController{
        get {
            return self.viewController
        }
    }
    
    fileprivate var segueIdentifier : String!
    /*Identifier For First Container SubView*/
    @IBInspectable internal var firstLinkedSubView : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        if let identifier = firstLinkedSubView{
            segueIdentifierReceivedFromParent(identifier)
        }
    }
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func segueIdentifierReceivedFromParent(_ identifier: String){
        self.segueIdentifier = identifier
        self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
    }
    
    override open func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier{
            //Remove Container View
            if viewController != nil{
                viewController.willMove(toParentViewController: nil)
                viewController.view.removeFromSuperview()
                viewController.removeFromParentViewController()
                viewController = nil
            }
            //Add to dictionary if isn't already there
            if ((self.containerViewObjects[self.segueIdentifier] == nil)){
                viewController = segue.destination
                self.containerViewObjects[self.segueIdentifier] = viewController
                
                // Llamando solo al contenedor de editar registro
                if segue.identifier == "noAccount"{
                    ///let destinacion = segue.destination as! SignUpVC
                    //destinacion.contact = contact
                }
                // Llamando solo al contenedor de editar registro
                if segue.identifier == "punchsCardsHome"{
                    //let destination = segue.destination as! PunchCardHomeVC
                   
                }
                if segue.identifier == "gotoPunchCards"{
                    let destinvationVC = segue.destination as! PunchCardVC
                    destinvationVC.merchant = merchant                    
                }
                
                
            }else{
                for (key, value) in self.containerViewObjects{
                    if key == self.segueIdentifier{
                        viewController = value
                    }
                }
            }
            self.addChildViewController(viewController)
            viewController.view.frame = self.view.bounds
            viewController.view.autoresizingMask = [.flexibleWidth , .flexibleHeight]
            self.view.addSubview(viewController.view)
            viewController.didMove(toParentViewController: self)
        }
    }
}

