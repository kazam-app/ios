//
//  LocationActivateVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/13/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import CoreLocation
import Pulsator
class LocationActivateVC: UIViewController {
    // MARK: - Properties
    var locationManager: CLLocationManager!
    let pulsator = Pulsator()
    var currentUser:UserModel!
    // MARK: - Outlets
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButtonOnNaviationBarRight(title: "nextActionKey".localized)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoNotification"{
            let destinationVC = segue.destination as! NotificationsActivateVC
            destinationVC.currentUser = currentUser
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addPulse()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgLogo.layer.layoutIfNeeded()
        pulsator.position = imgLogo.layer.position
    }
    func addPulse(){
        pulsator.numPulse = 7
        pulsator.radius = 200.0
        pulsator.animationDuration = kMaxDuration
        pulsator.backgroundColor = UIColor.uiCornflowerBlue.cgColor
        imgLogo.layer.superlayer?.insertSublayer(pulsator, below: imgLogo.layer)
        
        pulsator.start()
    }


    @IBAction func enabledLocationAction(_ sender: Any) {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
    }
    override func nextAction() {
        gotoNext()
    }
    func gotoNext(){
        self.performSegue(withIdentifier: "gotoNotification", sender: self)
    }
}
extension LocationActivateVC:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if /*status == .authorizedAlways || status == .authorizedWhenInUse*/ status != .notDetermined {
            /*if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                    print("Permission accepted")
                }
            }*/
            gotoNext()
        }
    }
}
