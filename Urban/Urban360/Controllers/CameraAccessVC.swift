//
//  CameraAccessVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/19/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import AVFoundation
import NVActivityIndicatorView


class CameraAccessVC: UIViewController {
    
    // MARK: - Properties
    var activityIndicator : NVActivityIndicatorView!
    let usrDefaults = UserDefaults.standard
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var currentMerchant:MerchantModel!
    // MARK: - Outlets
    @IBOutlet weak var btnEnabledCamera: UIButton!
    @IBOutlet weak var cameraV: UIView!
    @IBOutlet weak var messageV: UIView!
    @IBOutlet weak var emptyStateV: UIView!
    @IBOutlet weak var noAccessCameraV: UIView!
    
    @IBOutlet weak var lblErrorMessage: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnEnabledCamera.stButtonBorder()
        lblErrorMessage.alpha = 0
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
        if (captureSession?.isRunning == false && usrDefaults.bool(forKey: "isCameraEnabled")) {
            captureSession.startRunning();
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true && usrDefaults.bool(forKey: "isCameraEnabled")) {
            captureSession.stopRunning();
        }
    }
    
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func enableCameraAction(_ sender: Any) {
     // usrDefaults.bool(forKey: "isCameraEnabled")
        usrDefaults.set(true, forKey: "isCameraEnabled")
        usrDefaults.synchronize()
        setup()
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func setup(){
        if  usrDefaults.bool(forKey: "isCameraEnabled") {
            emptyStateV.alpha = 0
            noAccessCameraV.alpha = 0
            cameraV.alpha = 1
            messageV.alpha = 1
            captureSession = AVCaptureSession()
            
            let videoCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
            let videoInput: AVCaptureDeviceInput
            
            do {
                videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                emptyStateV.alpha = 1
                noAccessCameraV.alpha = 1
                cameraV.alpha = 0
                messageV.alpha = 0
                usrDefaults.set(false, forKey: "isCameraEnabled")
                usrDefaults.synchronize()
                self.view.reloadInputViews()
                return
            }
            
            if (captureSession.canAddInput(videoInput)) {
                captureSession.addInput(videoInput)
            } else {
                failed();
                return;
            }
            
            let metadataOutput = AVCaptureMetadataOutput()
            
            if (captureSession.canAddOutput(metadataOutput)) {
                captureSession.addOutput(metadataOutput)
                
                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
            } else {
                failed()
                return
            }
            
            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
            previewLayer.frame = cameraV.layer.bounds;
            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            cameraV.layer.addSublayer(previewLayer);
             self.view.reloadInputViews()
            captureSession.startRunning();
        }
        else{
            cameraV.alpha = 0
            messageV.alpha = 0
        }

    }

    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    func found(code: String) {
        getMerchant(qrCode: code)
        
    }
    func  getMerchant(qrCode:String){
        self.lblErrorMessage.fadeOut(withDuration: 0.25)
        self.activityIndicator.startAnimating()
        MerchantManager.merchantByQR(qrCode: qrCode){ merchant,error in
            self.activityIndicator.stopAnimating()
            if error == nil{
                if merchant != nil{
                    self.usrDefaults.set(MerchantModel.toDictionary(merchant: merchant!), forKey: "merchantBrand")
                    self.usrDefaults.synchronize()
                    self.dismiss(animated: true, completion:nil)
                }
            }
            else{
                self.lblErrorMessage.fadeIn(withDuration: 0.25)
                self.captureSession.startRunning()
            }
        }
    }

}

extension CameraAccessVC:AVCaptureMetadataOutputObjectsDelegate{
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject;
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: readableObject.stringValue);
        }
    }

}
