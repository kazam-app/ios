//
//  MessageDetailVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/13/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import NVActivityIndicatorView




class MessageDetailVC: UIViewController {

    // MARK: - Properties
    var titleDetail:String!
    var activityIndicator : NVActivityIndicatorView!
    var option:Int!
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tvDetail: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblTitle.text = "" /* option != 0 ? self.title : ""*/
        self.activityIndicator = activityIndicator(view: view)
         loadAbout(option: option)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func loadAbout(option:Int) {
        self.activityIndicator.startAnimating()
        switch option {
        case 0:
            AboutManager.getListFAQ(){ listFAQ, error in
                self.activityIndicator?.stopAnimating()
                if error == nil {
                    if !(listFAQ!.isEmpty) {
                        for faq in listFAQ!{
                            self.tvDetail.insertText("\n\(faq)")
                        }
                    }else{
                        self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar actualizar tu contraseña por favor verifica e intenta nuevamente")
                    }
                }
                else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "No se pudo actualizar intenta de nuevo")
                }
            }
            break
        case 1:
            AboutManager.getListTerms(){ listTerms, error in
                self.activityIndicator?.stopAnimating()
                if error == nil {
                    if !(listTerms!.isEmpty) {
                        for faq in listTerms!{
                            self.tvDetail.insertText("\n\n\(faq)")
                        }
                    }else{
                        self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar actualizar tu contraseña por favor verifica e intenta nuevamente")
                    }
                }
                else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "No se pudo actualizar intenta de nuevo")
                }
            }

            break
        case 2:
            AboutManager.getListPolicy(){ listPolicies, error in
                self.activityIndicator?.stopAnimating()
                if error == nil {
                    if !(listPolicies!.isEmpty) {
                        for faq in listPolicies!{
                            self.tvDetail.insertText("\n\n\(faq)")
                        }
                    }else{
                        self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar actualizar tu contraseña por favor verifica e intenta nuevamente")
                    }
                }
                else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "No se pudo actualizar intenta de nuevo")
                }
            }

            break
        default:
            break
        }
    }
}
