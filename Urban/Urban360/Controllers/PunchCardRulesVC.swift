//
//  PunchCardRulesVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 8/9/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

class PunchCardRulesVC: UIViewController {
    
    var punchCard:PunchCardModel!
    
    @IBOutlet weak var lblEndDatePromotion: UILabel!
    @IBOutlet weak var lblRulesPromotion: UILabel!
    
    @IBOutlet weak var txtvConditionsPromotion: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        lblEndDatePromotion.text = punchCard.expiresAt
        lblRulesPromotion.text = punchCard.rules
        txtvConditionsPromotion.text = punchCard.terms
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
