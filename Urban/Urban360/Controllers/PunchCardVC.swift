//
//  PunchCardVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/30/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class PunchCardVC: UIViewController {
    // MARK: - Properties
    var merchant:MerchantModel!
    let usrDefaults = UserDefaults.standard
    var activityIndicator : NVActivityIndicatorView!
    var punchsCards:[PunchCardModel] = []
    var isEmmbed = false
    var isBluetoothScan = true
    @IBOutlet weak var cvPunchsCards: UICollectionView!
    @IBOutlet weak var emptyPunchCard: UIView!
    @IBOutlet weak var lblEmptyPunchCard: UILabel!
    @IBOutlet weak var control: UIPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        if !isEmmbed{
            let merchantDic:[String:AnyObject] = self.usrDefaults.object( forKey: "merchantBrand") as! [String : AnyObject]
            merchant = MerchantModel.toMerchantModel(merchantDic: merchantDic)
        }
       // self.activityIndicator = activityIndicator(view: view)
        //loadPunchsCards()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
        if self.validateReachability(){
              punchsCards = []
            loadPunchsCards()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func loadPunchsCards()  {
        self.activityIndicator.startAnimating()
        PunchsCardsManager.punchsCardsByMerchant(merchantId: merchant.merchantId!){ results, error in
            self.activityIndicator.stopAnimating()
            if error == nil {
                self.punchsCards = results!
                self.cvPunchsCards.isPagingEnabled = true
                self.cvPunchsCards.reloadData()
                self.control.numberOfPages = self.punchsCards.count
                self.emptyPunchCard.fadeOut(withDuration: 0.3)
            }
            else{
                self.emptyPunchCard.fadeIn(withDuration: 0.5)
                self.lblEmptyPunchCard.text = NSLocalizedString("emptyPunchCard", comment: "emptyPunchCard").replacingOccurrences(of: "{MERCHANT}", with: self.merchant.name!)
            }
        }
    }
    
    func configureCollectionViewCell(cell: UICollectionViewCell) {
        cell.contentView.layer.cornerRadius = 7.0
        cell.contentView.layer.borderWidth = 2.0
        cell.contentView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        cell.contentView.layer.masksToBounds = false
        cell.contentView.layer.shadowColor = UIColor.lightGray.cgColor
        cell.contentView.layer.shadowOpacity = 1.0
        cell.contentView.layer.shadowRadius = 4.0
        cell.contentView.layer.shadowOffset = CGSize.init(width: 10, height: 10)
        cell.contentView.layer.shouldRasterize = true
        cell.contentView.layer.shadowPath = UIBezierPath.init(rect: cell.layer.bounds).cgPath
                /*
         [ScrlViewLayer setMasksToBounds:NO ];
         [ScrlViewLayer setShadowColor:[[UIColor lightGrayColor] CGColor]];
         [ScrlViewLayer setShadowOpacity:1.0 ];
         [ScrlViewLayer setShadowRadius:6.0 ];
         [ScrlViewLayer setShadowOffset:CGSizeMake( 0 , 0 )];
         [ScrlViewLayer setShouldRasterize:YES];
         [ScrlViewLayer setCornerRadius:5.0];
         [ScrlViewLayer setBorderColor:[UIColor lightGrayColor].CGColor];
         [ScrlViewLayer setBorderWidth:1.0];
         [ScrlViewLayer setShadowPath:[UIBezierPath bezierPathWithRect:scrollview.bounds].CGPath];
         */
    }
}


extension PunchCardVC:UICollectionViewDelegate{
    
}
extension PunchCardVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return punchsCards.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "punchCardcell", for: indexPath) as! PunchCardCVC
        cell.setupCell(merchant: merchant, punchCard:punchsCards[indexPath.row])
        //configureCollectionViewCell(cell: cell)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let tamani :CGSize = CGSize.init(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        return tamani
    }
    
    @objc(collectionView:layout:insetForSectionAtIndex:)  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        control.currentPage = indexPath.row
    }
    
}
