//
//  InvitationCodeVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Eureka
import NVActivityIndicatorView
class InvitationCodeVC: FormViewController,UITextFieldDelegate {
    // MARK: - Properties
    var currentUser:UserModel!
    var activityIndicator : NVActivityIndicatorView!
    var popupVC:PopupVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createFormInvitationCode()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func endRegisterAction(_ sender: Any) {
        let formData = form.values()
        if let invitationCode:String = formData["invitationCode"] as? String{
            if !invitationCode.isEmpty{
                currentUser.inviteCode = invitationCode
            }
            else{
                currentUser.inviteCode = nil
            }
        }
        createAccount(currentUser: currentUser)
    }
    func createFormInvitationCode() {
        form +++ Section()
            <<< TextRow() {
                $0.tag  = "invitationCode"
                $0.placeholder = "Código de Invitación (Opcional)"
                $0.placeholderColor = .gray
                $0.validationOptions = .validatesOnChangeAfterBlurred
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Code")
                }
    }
    
    func createAccount(currentUser:UserModel){
        self.activityIndicator?.startAnimating()
        AccountManager.createAccount(user: currentUser){ userSession, error, message in
            self.activityIndicator?.stopAnimating()
            if error == nil {
                if userSession != nil{
                    let usrDefaults = UserDefaults.standard
                    usrDefaults.set(UserModel.toDictionary(user: userSession!), forKey: "userSession")
                    usrDefaults.synchronize()
                    if userSession?.merchant != nil{
                        let sb = UIStoryboard(name: "General", bundle: nil)
                        self.popupVC = sb.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
                        self.popupVC.modalPresentationStyle = .overFullScreen
                        self.popupVC.titlePopup = "¡Felicidades!"
                        self.popupVC.detailPopup = "¡Ya tienes tu primer sello en \(userSession!.merchant!.name!)!"
                        self.popupVC.urlLogo = userSession!.merchant!.urlImage!
                        self.popupVC.isSingle = true
                        self.popupVC.popupDelegate = self as PopupDelegate
                        self.present(self.popupVC, animated: true, completion: nil)

                    }
                    else{
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    if message != nil{
                        self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:message!)
                    }else{
                        self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar crear el usuario por favor verifica e intenta nuevamente")
                    }
                }
            }
            else{
                if message != nil{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:message!)
                }else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "No se pudo crear el usuario el correo ya existe por favor verifica e intenta de nuevo")
                }
            }
        }
    }
}
extension InvitationCodeVC:PopupDelegate{
    func confirmSingle() {
            popupVC.dismiss(animated: true, completion:{
               self.dismiss(animated: true, completion: nil)
            })
    }
}
