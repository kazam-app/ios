//
//  EditProfileVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/11/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Eureka
import NVActivityIndicatorView
class EditProfileVC: FormViewController {
    //  MARK: - Properties
    var activityIndicator : NVActivityIndicatorView!
    var currentUser:UserModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        createUserForm()
        // Do any additional setup after loading the view.
        self.addButtonOnNaviationBarRight(title: "Guardar")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func createUserForm(){
        form +++ Section()
            <<< NameRow() {
                $0.tag  = "name"
                $0.value = currentUser.name
                $0.placeholder = "Nombre"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "user")
                    cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.cell.textField.textColor = .black
                        
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        row.placeholderColor = .red
                        row.placeholder = "Nombre requerido"
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.placeholderColor = .gray
                        row.placeholder = "Nombre"
                        row.cell.textField.textColor = .black
                        
                    }
            }
            
            <<< TextRow(){
                $0.tag  = "lastnames"
                $0.value = currentUser.lastNames
                $0.placeholder = "Apellido"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "user")
                    cell.imageView?.backgroundColor = .clear
                    cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.cell.textField.textColor = .black
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        row.placeholderColor = .red
                        row.placeholder = "Apellido requerido"
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.placeholderColor = .gray
                        row.placeholder = "Nombre"
                        row.cell.textField.textColor = .black
                        
                    }
            }
            <<< DateRow(){
                $0.tag  = "birthDay"
                $0.title = "Fecha de Nacimiento"
                let formatter = DateFormatter()
                formatter.dateFormat = "dd'/'MM'/'yyyy"
                $0.dateFormatter = formatter
                $0.value = (currentUser.birthDate != nil) ? formatter.date(from: currentUser.birthDate!) : Date()
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Birthday")
            }
            <<<  ActionSheetRow<String>(){
                $0.tag  = "gender"
                $0.title = "Genero"
                var gender:String = ""
                if currentUser.gender == "0"{
                    gender  = "Mujer"
                }
                else if currentUser.gender == "1"{
                     gender  = "Hombre"
                }
                $0.value = gender
                $0.selectorTitle = "Seleccione Género"
                $0.options = ["Hombre","Mujer"]
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Gender")
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
   
    
    func validateForm(){
        let formData = form.values()
        
        let validateFormErrors = form.validate(includeHidden: true)
        if !validateFormErrors.isEmpty{
            self.displayErrorAlert(title: "generalErrorMessageKey".localized, message: "fieldRequiredKey".localized)
        }
        else{
            let birthDay = formData["birthDay"] as? Date
            currentUser.birthDate = birthDay?.stDateString(format: STDateFormat.date_only_signup)
            currentUser.name = formData["name"] as? String
            currentUser.lastNames = formData["lastnames"] as? String
            currentUser.gender = formData["gender"] as? String
            updateAccount(currentUser: currentUser)
        }
    }
    
    override func nextAction() {
        validateForm()
    }
    
        func updateAccount(currentUser:UserModel)  {
        self.activityIndicator?.startAnimating()
        AccountManager.updateAccount(user: currentUser){ userSession, error in
            self.activityIndicator?.stopAnimating()
            if error == nil {
                if userSession != nil {
                    let usrDefaults = UserDefaults.standard
                    currentUser.name = userSession?.name
                    currentUser.lastNames = userSession?.lastNames
                    currentUser.birthDate = userSession?.birthDate
                    currentUser.gender = userSession?.gender
                    usrDefaults.set(UserModel.toDictionary(user: currentUser), forKey: "userSession")
                    usrDefaults.synchronize()
                    self.displayHandlAlert(title: "generalErrorMessageKey".localized, message: "Se han actualizado correctamente tus datos", completion:{action in self.navigationController?.popToRootViewController(animated: true)})
                    
                }else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar actualizar el usuario por favor verifica e intenta nuevamente")
                }
            }
            else{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "No se pudo actualizar intenta de nuevo")
            }
        }
    }

}

