//
//  ForgotPasswordVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/17/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Eureka
import NVActivityIndicatorView

class ForgotPasswordVC: FormViewController,UITextFieldDelegate {
    var activityIndicator : NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        createFormForgotPassword()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.activityIndicator = activityIndicator(view: view)
    }
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
     */
    
    @IBAction func recoverPassAction(_ sender: Any) {
        let validateFormErrors = form.validate()
        if !validateFormErrors.isEmpty{
            self.displayErrorAlert(title: "generalErrorMessageKey".localized, message: "fieldRequiredKey".localized)
        }
        else{
            self.activityIndicator.startAnimating()
            let formData = form.values()
            let userMail:[String:String] = ["email":(formData["email"] as? String)!]
            recoveryPassword(mail: userMail)
        }
    }
    
    
    func createFormForgotPassword() {
        form +++ Section()
            <<< EmailRow() {
                $0.tag  = "email"
                $0.placeholder = "Correo electrónico"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleEmail())
                $0.validationOptions = .validatesOnChangeAfterBlurred
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Email")
                    cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                        cell.backgroundColor = .white
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        for (index, _) in row.validationErrors.map({ $0.msg }).enumerated() {
                            let labelRow = LabelRow() {
                                $0.title = "Correo requerido"
                                $0.cellStyle = .subtitle
                                $0.cell.backgroundColor = .white
                                $0.cell.height = { 30 }
                                }.cellUpdate({ (cell, row) in
                                    cell.textLabel?.textColor = .red
                                })
                            row.section?.insert(labelRow, at: row.indexPath!.row + index + 1)
                    }
                }
            }
    }
    func recoveryPassword(mail:[String:String]){
        AccountManager.recoveryPassword(email:mail){ results, error in
            self.activityIndicator.stopAnimating()
            if error == nil {
                if results! {
                    
                    self.displayHandlAlert(title: "generalErrorMessageKey".localized, message: "recovereyKey".localized, completion:{ (action) in
                        
                        let _ = self.dismiss(animated: true, completion: nil)
        
                    })
                }else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema verifica que tu correo sea correcto")
                }
            }
            else{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "Ocurrio un problema tratar de recuperar tu contraseña intenta de nuevo")
            }
        }
    }

}
