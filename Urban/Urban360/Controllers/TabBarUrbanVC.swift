//
//  TabBarUrbanVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 8/4/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import UIKit

class TabBarUrbanVC: UITabBarController, UITabBarControllerDelegate {
    
    let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
    
    // View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = UIColor.uiCornflowerBlue
        self.tabBar.unselectedItemTintColor = UIColor.init(red: 102.0/255.0, green: 102.0/255.0, blue: 102.0/255.0, alpha: 0.95)
        self.delegate = self
        self.setupMiddleButton()
    }
    
    // Tab Bar Specific Code
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    // TabBarButton – Setup Middle Button
    func setupMiddleButton() {
        var menuButtonFrame = menuButton.frame
        menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height
        menuButtonFrame.origin.x = self.view.bounds.width / 2 - menuButtonFrame.size.width / 2
        menuButton.frame = menuButtonFrame
        
        //menuButton.backgroundColor = UIColor.uiCornflowerBlue
        menuButton.layer.cornerRadius = menuButtonFrame.height/2
        self.view.addSubview(menuButton)
        
        menuButton.setImage(UIImage(named: "logoInteraction"), for: UIControlState.normal)
        menuButton.addTarget(self, action: #selector(TabBarUrbanVC.menuButtonAction), for: UIControlEvents.touchUpInside)
        
        self.view.layoutIfNeeded()
    }
    
    // Menu Button Touch Action
    func menuButtonAction(sender: UIButton) {
        self.selectedIndex = 1
        // console print to verify the button works
        menuButton.setImage(UIImage(named: "logoInteraction"), for: UIControlState.normal)
        print("Middle Button was just pressed!")
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        switch viewController
        {
        case is NewsVC:
           menuButton.setImage(UIImage(named: "InteractionLogoOff"), for: UIControlState.normal)
        case is ContainerHomeVC:
            menuButton.setImage(UIImage(named: "logoInteraction"), for: UIControlState.normal)
        case is ProfileVC:
            menuButton.setImage(UIImage(named: "InteractionLogoOff"), for: UIControlState.normal)
        default:
            menuButton.setImage(UIImage(named: "InteractionLogoOff"), for: UIControlState.normal)
        }
    }
}
