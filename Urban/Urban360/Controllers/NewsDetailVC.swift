//
//  NewsDetailVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/17/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import SafariServices
class NewsDetailVC: UIViewController{
    //MARK: -properties
    var detailNews:NewModel! = nil
    let imgHeaderNews:UIImageView = UIImageView()
    //MARK: -Outlet
    @IBOutlet weak var newsWV: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = detailNews.category!
        newsWV.scrollView.delegate = self
        setupData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func setupData() {
        imgHeaderNews.backgroundColor = UIColor.clear
        imgHeaderNews.frame = CGRect(x: 0, y: 64, width: self.view.bounds.width, height: 250)
        imgHeaderNews.image = UIImage(named: "NewsEmptyState")
        if detailNews.urlImage! != ""{
            imgHeaderNews.af_setImage(withURL:URL(string:detailNews.urlImage!)!)
        }
        imgHeaderNews.contentMode = .scaleAspectFill
        imgHeaderNews.clipsToBounds = true
        view.insertSubview(imgHeaderNews, belowSubview: newsWV)
        
        
        let bundle = Bundle.main.path(forResource: "news_html", ofType: "txt")
       let templeateHTML = try? String(contentsOfFile: bundle!, encoding: String.Encoding.utf8 )
        newsWV.loadHTMLString(String(format:templeateHTML!, arguments: [detailNews.categoryColor!,detailNews.title!,detailNews.category!,detailNews.date!, detailNews.html!]), baseURL:URL(string: "") )
        let textSize: Int = 100
        self.newsWV.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '\(textSize)%'")
        self.newsWV.backgroundColor = UIColor.clear
        self.newsWV.isOpaque = false
    }
    func openSafari(url:String){
        let svc = SFSafariViewController(url:URL(string: url)!)
        present(svc, animated: true, completion: nil)
    }
}
extension NewsDetailVC:UIWebViewDelegate{
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.url?.absoluteString
        if ( navigationType == UIWebViewNavigationType.linkClicked) {
            if (url?.hasPrefix("http://"))! || (url?.hasPrefix("https://"))!{
                if #available(iOS 9.0, *) {
                    self.openSafari(url: url!)
                }
                 return false
                }
        }
        return true
    }
} 

extension NewsDetailVC:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if (maximumOffset - currentOffset <= 20.0) {
            self.tabBarController?.tabBar.isHidden = true
        }
        else{
            self.tabBarController?.tabBar.isHidden = false

        }
    }
    
    
}
