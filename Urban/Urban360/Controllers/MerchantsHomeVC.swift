//
//  MerchantsHomeVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/20/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation
class MerchantsHomeVC: UIViewController {
    // MARK: - Properties
    var merchants:[MerchantModel] = []
    var merchantsNear:[MerchantLocationModel] = []
    var activityIndicator : NVActivityIndicatorView!
    var locationManager: CLLocationManager!
    var currentMerchant:MerchantModel!
    var currentMerchantNear:MerchantLocationModel!
    var isShowing:Bool = false
    // MARK: - Outlets
    @IBOutlet weak var locationEnableV: UIView!
    @IBOutlet weak var optListMerchants: UISegmentedControl!
    @IBOutlet weak var tblMerchants: UITableView!
    @IBOutlet weak var viewNoMerchants: UIView!
    
    @IBOutlet weak var btnEnabledLocation: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        //self.navigationController!.navigationBar.topItem!.title = ""
        btnEnabledLocation.stButtonBorder()
        // Do any additional setup after loading the view.
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.activityIndicator = activityIndicator(view: view)
        if self.validateReachability(){
            loadAllMerchants()
        }
        else{
            if !isShowing {
                self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
                isShowing = true
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "gotoDetailMerchant" {
            let destinationVC = segue.destination as! MerchantDetailVC
            if optListMerchants.selectedSegmentIndex == 0{
                destinationVC.isNear = false
                destinationVC.currentMerchant = currentMerchant
            }
            else{
                destinationVC.isNear = true
                destinationVC.currentMerchantNear = currentMerchantNear
            }
            
        }
    }
    
    
    @IBAction func changeAction(_ sender: Any) {
        viewNoMerchants.fadeOut(withDuration: 0.3)
        if optListMerchants.selectedSegmentIndex == 0{
             if self.validateReachability(){
                tblMerchants.fadeIn(withDuration: 0.5)
                locationEnableV.fadeOut(withDuration: 0.5)
                loadAllMerchants()
             }else{
                self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: true)
            }
        }
        else{
            if self.validateLocationServices(){
                let locManager = CLLocationManager()
                let currentLocation = locManager.location
                loadMerchantsNear(currentLocation: currentLocation!)
            }
            else{
                tblMerchants.fadeOut(withDuration: 0.5)
                locationEnableV.fadeIn(withDuration: 0.5)
            }
        }
    }
    
    @IBAction func enabledLocationAction(_ sender: Any) {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .denied:
                print("Denied")
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                break
            case .notDetermined:
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.requestAlwaysAuthorization()
                break
            default:
                break
            }
        } else {
            if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    
    
    func loadAllMerchants() {
        self.activityIndicator.startAnimating()
        MerchantManager.getListMerchants(){ results, error in
            self.activityIndicator.stopAnimating()
            if error == nil {
                if results != nil {
                    self.merchants = results!
                    self.tblMerchants.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .fade)
                }else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar obtener las marcas")
                }
            }
            else{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "Ocurrio un problema intenta de nuevo")
            }
        }

    }
    func loadMerchantsNear(currentLocation:CLLocation) {
        
        self.activityIndicator.startAnimating()
        MerchantManager.getListMerchantsNear(location: ["lat":Float(currentLocation.coordinate.latitude),"lng":Float(currentLocation.coordinate.longitude)]){ results, error in
            self.activityIndicator.stopAnimating()
            if error == nil {
                if results != nil {
                    self.merchantsNear = results!
                    self.tblMerchants.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .fade)
                }else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema al intentar obtener las marcas")
                }
            }
            else{
                //self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "No pudimos obtener tiendas cercanas a ti intenta de nuevo")
                self.tblMerchants.reloadData()
                self.viewNoMerchants.fadeIn(withDuration: 0.3)
            }
        }
        
    }
    override func reloadData() {
        isShowing = false
        changeAction(self)
    }
}
extension MerchantsHomeVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if optListMerchants.selectedSegmentIndex == 0{
            currentMerchant = merchants[indexPath.row]
        }
        else{
            currentMerchantNear = merchantsNear[indexPath.row]
        }
        
        self.performSegue(withIdentifier: "gotoDetailMerchant", sender: self)
    }
    
}
extension MerchantsHomeVC:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if optListMerchants.selectedSegmentIndex == 0{
            return 80.0
        }
        else{
            return 100.0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if optListMerchants.selectedSegmentIndex == 0{
            return merchants.count
        }
        else{
            return merchantsNear.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if optListMerchants.selectedSegmentIndex == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "merchantCell") as! MerchantTVC
            cell.setupCell(merchant:merchants[indexPath.row])
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "merchantLocationCell") as! MerchantLocationTVC
            cell.setupCell(merchantNear:merchantsNear[indexPath.row])
            return cell
        }
        
    }
}
extension MerchantsHomeVC:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                    tblMerchants.fadeIn(withDuration: 0.5)
                    locationEnableV.fadeOut(withDuration: 0.5)
                    changeAction(self)
                }
            }
        }
    }
}
