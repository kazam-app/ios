//
//  ChangePasswordVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/11/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Eureka
import NVActivityIndicatorView
class ChangePasswordVC: FormViewController {
    //  MARK: - Properties
    var activityIndicator : NVActivityIndicatorView!
    var currentUser:UserModel!
    
    // MARK: - Outlets
    
    @IBOutlet weak var txtCurrentPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtCurrentPassword.delegate = self
        // Do any additional setup after loading the view.
        createUserForm()
        self.addButtonOnNaviationBarRight(title: "Cambiar")
        txtCurrentPassword.attributedPlaceholder = NSAttributedString(string: txtCurrentPassword.placeholder!, attributes: [NSForegroundColorAttributeName: UIColor.gray])
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func recoveryPassAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Profile", bundle: nil)
        let destinationVC = sb.instantiateViewController(withIdentifier: "ForgotPasswordVC")
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    override func nextAction() {
        validateForm()
    }
    
    func createUserForm(){
        form +++ Section()
            <<< PasswordRow(){
                $0.tag  = "password"
                $0.placeholder = "Contraseña nueva"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleMinLength(minLength: 8))
                }
                .cellSetup { cell, row in
                    //cell.imageView?.image = #imageLiteral(resourceName: "Password")
                    cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.textLabel?.textColor = .red
                    }
            }
            <<< PasswordRow(){
                $0.tag  = "confirmPassword"
                $0.placeholder = "Reptir Contraseña"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleMinLength(minLength: 8))
                //$0.add(rule: RuleEqualsToRow(form: form, tag: "password"))
                }
                .cellSetup { cell, row in
                    ///cell.imageView?.image = #imageLiteral(resourceName: "Password")
                    cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    
                    if !row.isValid {
                        cell.textLabel?.textColor = .red
                    }
        }
        
    }
    
    func validateForm(){
        let formData = form.values()
        
        let validateFormErrors = form.validate(includeHidden: true)
        if !validateFormErrors.isEmpty || (txtCurrentPassword.text?.isEmpty )!{
            self.displayErrorAlert(title: "generalErrorMessageKey".localized, message: "fieldRequiredKey".localized)
        }
        else{
            let password = formData["password"] as! String
            let confirmPassword = formData["confirmPassword"] as! String
            if (password.characters.count) < 8{
                self.displayErrorAlert(title: "errorMessage".localized, message: "La contraseña actual que ingresó es incorrecta, por favor intente de nuevo")
            }else{
                if password == confirmPassword {
                    updatePassword(oldPassword:txtCurrentPassword.text!, newPassword: (formData["password"] as? String)!, confirmPassword: (formData["password"] as? String)!)
                }else{
                    self.displayErrorAlert(title: "errorMessage".localized, message: "La contraseña que ingresó no es igual en ambos campos. Asegúrese de que sea la misma.")
                }
            }
            
        }
    }
    func updatePassword(oldPassword:String, newPassword:String, confirmPassword:String){
        self.activityIndicator?.startAnimating()
        AccountManager.updatePassword(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword){ updated, error in
            self.activityIndicator?.stopAnimating()
            if error == nil {
                if updated! {
                    self.displayHandlAlert(title: "¡Listo!".localized, message: "Se han actualizado correctamente tus datos", completion:{action in self.navigationController?.popToRootViewController(animated: true)})
                    
                }else{
                    self.displayErrorAlert(title: "errorMessage".localized,message:"No pudimos actualizar su información en este momento. Por favor intente de nuevo más tarde")
                }
            }
            else{
                self.displayErrorAlert(title: "errorMessage".localized,message: "La contraseña actual que ingresó es incorrecta, por favor intente de nuevo.")
            }
        }
    }

}

extension ChangePasswordVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtCurrentPassword.resignFirstResponder()
        return true
    }
}
