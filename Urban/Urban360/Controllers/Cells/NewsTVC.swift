//
//  NewsTVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/17/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit


class NewsTVC: UITableViewCell {
    // Mark: - Outlets
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var lblWhen: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DispatchQueue.main.async {
            self.imgBackground.addBlackGradientLayer() 
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(news:NewModel) {
       // print("news URL: \(news.urlImage!)")
        if news.urlImage! != ""{
            self.imgBackground.af_setImage(
                withURL: URL(string: news.urlImage!)!,
                placeholderImage: UIImage(named: "NewsEmptyState"),
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgBackground.image = UIImage(named: "NewsEmptyState")
        }
        self.lblWhen.text = news.date!
        self.lblTitle.text = news.title!
        self.lblCategory.text = news.category!
    }

}
