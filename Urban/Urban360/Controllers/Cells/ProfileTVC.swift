//
//  ProfileTVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/10/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

protocol LogoutDelegate {
    func logout()
}

class ProfileTVC: UITableViewCell {
    
    //MARK: - Properties
    
    
    //MARK: - Outlets
    @IBOutlet weak var lblTitleSetting: UILabel!
    @IBOutlet weak var lblDetailSetting: UILabel!
    @IBOutlet weak var btnLogout: UIButton!
    
    var delegate: LogoutDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func logout(){
        if self.delegate != nil{
            self.delegate.logout()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupCell(user:UserModel?,indexPath:IndexPath) {
        if lblDetailSetting != nil{
            lblDetailSetting.text = ""
        }
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                lblTitleSetting.text = "Nombre"
                lblDetailSetting.text = (user != nil) ? user?.name : ""
                break
            case 1:
                lblTitleSetting.text = "Apellido"
                lblDetailSetting.text = (user != nil) ? user?.lastNames : ""
                break
            case 2:
                lblTitleSetting.text = "Fecha de Nacimiento"
                lblDetailSetting.text = (user != nil) ? user?.birthDate : ""
                break
            case 3:
                lblTitleSetting.text = "Género"
                lblDetailSetting.text = (user != nil) ? (user?.gender == "1") ? "Hombre":"Mujer" : ""
                break
            case 4:
                lblTitleSetting.text = "Correo"
                lblDetailSetting.text = (user != nil) ? user?.email : ""
                break
            case 5:
                lblTitleSetting.text = "Cambio de contraseña"
                
                break
            default:
                break
            }
        break
            
        case 1:
            lblTitleSetting.text = "Permisos Notificaciones"
            break
        case 2:
            switch indexPath.row {
            case 0:
                lblTitleSetting.text = "Preguntas Frecuentes"
                break
            case 1:
                lblTitleSetting.text = "Contáctanos"
                break
            default:
                break
            }
            break
        
        case 3:
            switch indexPath.row {
            case 0:
                lblTitleSetting.text = "Términos y Condiciones"
                break
            case 1:
                lblTitleSetting.text = "Política de Privacidad"
                break
            default:
                break
            }
            break
        case 4:
            btnLogout.alpha = (user != nil) ? 1.0 : 0.0
            break
        default:
            break
        }
    }
}
