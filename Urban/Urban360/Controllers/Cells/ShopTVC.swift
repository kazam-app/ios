//
//  ShopTVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/30/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import AlamofireImage
class ShopTVC: UITableViewCell {
    
    //MARK: - Outlets
    
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblShopCluster: UILabel!
    @IBOutlet weak var lblShopDistance: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(shop:MerchantLocationModel){
       // let imageFilter = CircleFilter()
        /*if shop.urlImage! != ""{
            self.imgLogoMerchant.af_setImage(
                withURL: URL(string: shop.urlImage!)!,
                placeholderImage: UIImage(named: "MerchantLogoPlaceHolder"),
                filter: imageFilter,
                imageTransition: .crossDissolve(0.2)
                
            )
        }
        else{
            self.imgLogoMerchant.image = UIImage(named: "MerchantLogoPlaceHolder")
        }*/
        lblShopName.text = "\( shop.merchantName!) - \(shop.name!)"
        lblShopCluster.text = shop.shopCluster
        lblShopDistance.text = shop.merchantDistance
    }

}
