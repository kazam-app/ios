//
//  ToolsPunchsCardsTVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/21/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
@objc protocol ToolsCellDelegate {
    @objc optional func showReedem()
}

class ToolsPunchsCardsTVC: UITableViewCell,PopupDelegate,PunchCardDelegate {

    // MARK:: - Properties
    var punchCard:PunchCardModel!
    var merchant:MerchantModel!
    var isComplet = false
    var popupVC:PopupVC!
    var punchVC:PunchInitialVC!
    var toolsCellDelegate:ToolsCellDelegate!
    let sb = UIStoryboard(name: "General", bundle: nil)
    let sbPunch = UIStoryboard(name: "PunchsCards", bundle: nil)
    var reedemPunchCard:PunchCardModel!
    
    // MARK: - Outlets
    @IBOutlet weak var btnPunchCard: UIButton!
    @IBOutlet weak var btnReedem: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func punchAction(_ sender: Any) {
        if isComplet{
            self.popupVC.titlePopup = "Tu Tarjeta es completa"
            self.popupVC.detailPopup = "¡Felicidades ya puedes canjear tu tarjeta!"
            self.popupVC.isSingle = true
            let vc = super.viewController()
            vc?.present(self.popupVC, animated: true, completion: nil)
        }
        else{
            punchVC = sbPunch.instantiateViewController(withIdentifier: "PunchInitialVC") as! PunchInitialVC
            punchVC.currentPunchCard = punchCard
            punchVC.currentMerchant = merchant
            punchVC.punchCardDelegate = self as PunchCardDelegate
            let vc = super.viewController()
            vc?.present(punchVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func reedemAction(_ sender: Any) {
        if isComplet{
            punchVC = sbPunch.instantiateViewController(withIdentifier: "PunchInitialVC") as! PunchInitialVC
            punchVC.currentPunchCard = punchCard
            punchVC.currentMerchant = merchant
            punchVC.isReedem = true
            punchVC.punchCardDelegate = self as PunchCardDelegate
            let vc = super.viewController()
            vc?.present(punchVC, animated: true, completion: nil)
        }
        else{
            self.popupVC.titlePopup = "Completa tu Tarjeta"
            self.popupVC.detailPopup = "Asegúrate de tener todos los sellos necesarios antes de canjear tu premio."
            self.popupVC.isSingle = true
            let vc = super.viewController()
            vc?.present(self.popupVC, animated: true, completion: nil)
        }
    }
    func setupCell(merchant:MerchantModel,punchCard:PunchCardModel){
        self.merchant = merchant
        self.punchCard = punchCard
        isComplet = false
        btnPunchCard.backgroundColor = UIColor.uiTealish
        btnReedem.backgroundColor = UIColor.uigray
        if self.punchCard.punchCount == self.punchCard.punchLimit {
            btnPunchCard.backgroundColor = UIColor.uigray
            btnReedem.backgroundColor = UIColor.uiTealish
            isComplet = true
        }
        self.popupVC = sb.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
        self.popupVC.modalPresentationStyle = .overFullScreen
        self.popupVC.popupDelegate = self as PopupDelegate
    }
    
    func confirmSingle() {
     self.popupVC.dismiss(animated: true, completion: nil)
    }
    func reedem() {
        print(punchVC.reedemPunchCard.reedemCode!)
        reedemPunchCard = punchVC.reedemPunchCard!
        self.toolsCellDelegate.showReedem!()
    }
}
