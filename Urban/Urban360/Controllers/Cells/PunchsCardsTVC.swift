//
//  PunchsCardsTVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/21/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
class PunchsCardsTVC: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet var punchs: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(punchCard:PunchCardModel){
        for (index,imgPunch) in punchs.enumerated(){
            imgPunch.alpha = 0
            if punchCard.punchCount! > 0 && index < (punchCard.punchCount!){
                imgPunch.image = #imageLiteral(resourceName: "punchCardFull")
                imgPunch.alpha = 1
            }
            else{
                if  index  <= (punchCard.punchLimit! - 1){
                    imgPunch.image = #imageLiteral(resourceName: "punchCardEmpty")
                    imgPunch.alpha = 1
                }
            }
        }
    }


}
