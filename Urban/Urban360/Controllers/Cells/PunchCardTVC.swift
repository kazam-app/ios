//
//  PunchCardTVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/23/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import AlamofireImage
class PunchCardTVC: UITableViewCell {
    // MARK: - Outlets
    
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var imgMerchant: UIImageView!
    @IBOutlet weak var lblPrizePunchcard: UILabel!
    @IBOutlet weak var punchsContainerV: UIView!
    @IBOutlet var imgPunchs: [UIImageView]!
    
    @IBOutlet weak var btnRedeemPrize: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnRedeemPrize.roundButton()
        btnRedeemPrize.alpha = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(punchCard:PunchCardListModel, isCompleted:Bool){
        lblMerchantName.text = punchCard.merchantName
        lblPrizePunchcard.text = "Premio: \(punchCard.prize!)"
        btnRedeemPrize.alpha = isCompleted ? 1:0
        let imageFilter = CircleFilter()
        if punchCard.urlImage! != ""{
            self.imgMerchant.af_setImage(
                withURL: URL(string: punchCard.urlImage!)!,
                placeholderImage: UIImage(named: "MerchantLogoPlaceHolder"),
                filter: imageFilter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgMerchant.image = UIImage(named: "MerchantLogoPlaceHolder")
        }
        for (index,imgPunch) in imgPunchs.enumerated(){
            imgPunch.alpha = 0
            if punchCard.punchCount! > 0 && index < (punchCard.punchCount!){
                imgPunch.image = #imageLiteral(resourceName: "punchCardFull")
                imgPunch.alpha = 1
            }
            else{
                if  index  <= (punchCard.punchLimit! - 1){
                    imgPunch.image = #imageLiteral(resourceName: "punchCardEmpty")
                    imgPunch.alpha = 1
                }
            }
        }
    }

}
