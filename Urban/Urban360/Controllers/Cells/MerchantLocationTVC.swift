//
//  MerchantTVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/21/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import AlamofireImage
class MerchantLocationTVC: UITableViewCell {
    
    //MARk: -Outlets
    @IBOutlet weak var imgLogoMerchant: UIImageView!
    @IBOutlet weak var lblNameMerchant: UILabel!
    @IBOutlet weak var lblLocationMerchant: UILabel!
    @IBOutlet weak var lblCategoryMerchant: UILabel!
    @IBOutlet weak var lblRatingPriceMerchant: UILabel!
    @IBOutlet weak var lblDistanceMerchant: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func setupCell(merchantNear:MerchantLocationModel) {
        // print("news URL: \(news.urlImage!)")
        let imageFilter = CircleFilter()
        if merchantNear.urlImage! != ""{
            self.imgLogoMerchant.af_setImage(
                withURL: URL(string: merchantNear.urlImage!)!,
                placeholderImage: UIImage(named: "MerchantLogoPlaceHolder"),
                filter: imageFilter,
                imageTransition: .crossDissolve(0.2)
                
            )
        }
        else{
            self.imgLogoMerchant.image = UIImage(named: "MerchantLogoPlaceHolder")
        }
       
        lblNameMerchant.text = "\( merchantNear.merchantName!) - \(merchantNear.name!)"
        lblLocationMerchant.text = merchantNear.shopCluster
        lblCategoryMerchant.text = merchantNear.category
        lblRatingPriceMerchant.attributedText = "$$$$$".rateText(rate: merchantNear.merchantBudgetRaiting ?? 0, colorHighlight: UIColor.gray)
        lblDistanceMerchant.text = merchantNear.merchantDistance
    }
}
