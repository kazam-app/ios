 //
//  PunchCardCVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/24/17.
//  Copyright © 2017 Urban360. All rights reserved.
//
import UIKit
class PunchCardCVC: UICollectionViewCell, UITableViewDataSource, UITableViewDelegate,ToolsCellDelegate {
    // MARK:: - Properties
    var punchCard:PunchCardModel!
    var merchant:MerchantModel!
    var loaderPunchCard = 0
    var isReedem = false
    // MARK: - Outlets
    @IBOutlet weak var punchCardtbl: UITableView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        punchCardtbl.delegate = self
        punchCardtbl.dataSource = self
        /*self.contentView.layer.cornerRadius = 10.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath*/
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if indexPath.row == 0{
            return 56
        }
        else if indexPath.row == 1{
            if punchCard.punchLimit! < 6 && !isReedem{
                return 40
            }
            if punchCard.punchLimit! < 6 && isReedem{
                return 130
            }
            else if punchCard.punchLimit! < 11{
                return 80
            }
            else{
                return 130
            }
        }
        else if indexPath.row == 3{
            return 48
        }
        else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loaderPunchCard
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "termsPunchsCardsCell") as! TermsTVC
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "titlePunchsCardsCell") as! TitlePunchCardTVC
            cell.lblGift.text = punchCard.prize
            return cell
        }
        else if indexPath.row  == 1 && !isReedem{
            let cell = tableView.dequeueReusableCell(withIdentifier: "punchsCardsTVC") as! PunchsCardsTVC
            cell.setupCell(punchCard: punchCard)
            return cell
        }
        else if indexPath.row == 2{
            cell.lblTerm.text = "Vencimiento y Condiciones"
        }
        else if indexPath.row == 3 && !isReedem{
            let cell = tableView.dequeueReusableCell(withIdentifier: "toolsPunchsCardsCell") as! ToolsPunchsCardsTVC
            cell.setupCell(merchant: merchant, punchCard: punchCard)
            cell.toolsCellDelegate = self
            return cell
        }
        else if indexPath.row  == 1 && isReedem{
            let cell = tableView.dequeueReusableCell(withIdentifier: "reedemCell") as! ReedemTVC
            isReedem = false
            cell.setupCell(punchCard: punchCard)
            return cell
        }

        return cell
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "PunchsCards", bundle: nil)
        if indexPath.row == 2{
            let termsNC = sb.instantiateViewController(withIdentifier: "PunchCardRulesVC") as! UINavigationController
            let termsVC = termsNC.viewControllers[0] as! PunchCardRulesVC
            termsVC.punchCard = punchCard
            let vc = super.viewController()
            vc?.present(termsNC, animated: true, completion: nil)
        }
    }
    func setupCell(merchant:MerchantModel,punchCard:PunchCardModel){
        self.merchant = merchant
        self.punchCard = punchCard
        loaderPunchCard = 4
        punchCardtbl.reloadData()
    }
    
    func showReedem() {
        let index:IndexPath = IndexPath(row: 3, section: 0)
        let cell =  punchCardtbl.cellForRow(at:index) as! ToolsPunchsCardsTVC
        punchCard = cell.reedemPunchCard
         loaderPunchCard = 3
        isReedem = true
        punchCardtbl.reloadData()
    }
}
