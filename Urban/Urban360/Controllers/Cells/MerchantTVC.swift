//
//  MerchantTVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/21/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import AlamofireImage
class MerchantTVC: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var imgMerchantLogo: UIImageView!
    @IBOutlet weak var lblNameMerchant: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblRaitngPrice: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupCell(merchant:MerchantModel) {
        // print("news URL: \(news.urlImage!)")
        let imageFilter = CircleFilter()
        if merchant.urlImage! != ""{
            self.imgMerchantLogo.af_setImage(
                withURL: URL(string: merchant.urlImage!)!,
                placeholderImage: UIImage(named: "MerchantLogoPlaceHolder"),
                filter: imageFilter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgMerchantLogo.image = UIImage(named: "MerchantLogoPlaceHolder")
        }
        lblNameMerchant.text = merchant.name
        lblCategory.text = merchant.category
        lblRaitngPrice.attributedText = "$$$$$".rateText(rate: merchant.budgetRating ?? 0,colorHighlight: UIColor.gray)
    }

}
