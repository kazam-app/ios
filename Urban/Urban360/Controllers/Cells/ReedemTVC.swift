//
//  ReedemTVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/26/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

class ReedemTVC: UITableViewCell {
    
    @IBOutlet weak var lblReedemCode: UILabel!
    
    @IBOutlet weak var lblTimer: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupCell(punchCard:PunchCardModel){
        lblReedemCode.text = punchCard.reedemCode
    }
}
