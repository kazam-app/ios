//
//  LoginVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Eureka
import NVActivityIndicatorView
class LoginVC: FormViewController,UITextFieldDelegate {
    
    var activityIndicator : NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        createLoginForm()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.activityIndicator = activityIndicator(view: view)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func loginAction(_ sender: Any) {
        let validateFormErrors = form.validate()
        if !validateFormErrors.isEmpty{
            self.displayErrorAlert(title: "generalErrorMessageKey".localized, message: "fieldRequiredKey".localized)
        }
        else{
            self.activityIndicator.startAnimating()
            let formData = form.values()
            let userLogin:UserModel = UserModel()
            userLogin.email = formData["email"] as? String
            userLogin.pwd = formData["password"] as? String
            login(userLogin: userLogin)
        }
        
    }
    @IBAction func loginFbAction(_ sender: Any) {
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile","email"], from: self, handler: { (result, error) in
            if error != nil{
                print("Custom FB Login failed:", error!)
                return
            }
            self.getFBUserData()
        })

    }
    
    func createLoginForm() {
        form +++ Section()
            <<< EmailRow() {
                $0.tag  = "email"
                $0.placeholder = "Correo electrónico"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleEmail())
                $0.validationOptions = .validatesOnChangeAfterBlurred
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Email")
                    cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                        cell.backgroundColor = .white
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        for (index, _) in row.validationErrors.map({ $0.msg }).enumerated() {
                            let labelRow = LabelRow() {
                                $0.title = "Correo requerido"
                                $0.cellStyle = .subtitle
                                $0.cell.backgroundColor = .white
                                $0.cell.height = { 30 }
                                }.cellUpdate({ (cell, row) in
                                    cell.textLabel?.textColor = .red
                                })
                            row.section?.insert(labelRow, at: row.indexPath!.row + index + 1)
                        }
                    }
            }
            <<< PasswordRow(){
                $0.tag  = "password"
                $0.placeholder = "Contraseña"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleMinLength(minLength: 8))
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Password")
                    cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.textLabel?.textColor = .red
                    }
        }
    }
    func getFBUserData(){
        if FBSDKAccessToken.current() != nil {
            let parameters = ["fields": "id, name, first_name, last_name, picture.type(large), email"]
            FBSDKGraphRequest(graphPath: "me", parameters: parameters ).start(completionHandler: { (conection, result, error) in
                if error == nil{
                    let results = result as! NSDictionary
                    let userLogin =  UserModel()
                    userLogin.fbId = results.value(forKey: "id") as? String
                    userLogin.fbToken = FBSDKAccessToken.current().tokenString
                    userLogin.fbExpiresAt = FBSDKAccessToken.current().expirationDate.stDateString(format: STDateFormat.date_only_signup)
                    self.login(userLogin: userLogin)
                }
            })
        }
    }

    func login(userLogin:UserModel){
        self.activityIndicator.startAnimating()
        AccountManager.login(user: userLogin){ userSession, error in
            self.activityIndicator.stopAnimating()
            if error == nil {
                if userSession != nil {
                    let usrDefaults = UserDefaults.standard
                    usrDefaults.set(UserModel.toDictionary(user: userSession!), forKey: "userSession")
                    usrDefaults.synchronize()
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"Ocurrio un problema tratar de iniciar sesión")
                }
            }
            else{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized,message: "Ocurrio un problema tratar de iniciar sesión intenta de nuevo")
            }
        }
    }
}
