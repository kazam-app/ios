//
//  RegisterUserVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/13/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Eureka
import NVActivityIndicatorView
class RegisterUserVC: FormViewController,UITextFieldDelegate {
    var currentUser:UserModel!
    var isSocialSignUp:Bool = false
     var activityIndicator : NVActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
             // Do any additional setup after loading the view.
        createUserForm()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
    }
    
    func createUserForm(){
        form +++ Section()
            <<< EmailRow() {
                $0.tag  = "email"
                $0.placeholder = "Correo electrónico"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleEmail())
                $0.validationOptions = .validatesOnChangeAfterBlurred
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Email")
                    //cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.cell.textField.textColor = .black
                        
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        row.placeholderColor = .red
                        row.placeholder = "Correo requerido"
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.placeholderColor = .gray
                        row.placeholder = "Correo electrónico"
                        row.cell.textField.textColor = .black
                        
                    }
            }
             <<< PasswordRow(){
             $0.tag  = "password"
             $0.hidden = isSocialSignUp ? true: false
             $0.placeholder = "Contraseña"
             $0.placeholderColor = .gray
             $0.add(rule: RuleRequired())
             $0.add(rule: RuleMinLength(minLength: 8))
             }
             .cellSetup { cell, row in
                cell.imageView?.image = #imageLiteral(resourceName: "Password")
                cell.row.updateCell()
             }
             .cellUpdate { cell, row in
             if !row.isValid {
                cell.textLabel?.textColor = .red
                }
             }
             <<< PasswordRow(){
             $0.tag  = "confirmPassword"
             $0.hidden = isSocialSignUp ? true: false
             $0.placeholder = "Confirmar Contraseña"
             $0.placeholderColor = .gray
             $0.add(rule: RuleRequired())
             $0.add(rule: RuleMinLength(minLength: 8))
             }
             .cellSetup { cell, row in
                cell.imageView?.image = #imageLiteral(resourceName: "Password")
                cell.row.updateCell()
             }
             .cellUpdate { cell, row in
    
             if !row.isValid {
                    cell.textLabel?.textColor = .red
                }
             }
            
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "gotoLastRegister"{
            let destinationVC = segue.destination as! RegisterUserLastVC
            destinationVC.currentUser = currentUser
        }
    }
    
    func validateForm(){
        let formData = form.values()
       
            let validateFormErrors = form.validate(includeHidden: true)
            if !validateFormErrors.isEmpty{
                self.displayErrorAlert(title: "generalErrorMessageKey".localized, message: "fieldRequiredKey".localized)
            }
            else{
                let password = formData["password"] as! String
                let confirmPassword = formData["confirmPassword"] as! String
                if (password.characters.count) < 8{
                    self.displayErrorAlert(title: "generalErrorMessageKey".localized, message: "La contraseña tiene que tener mas de 8 caracteres")
                }else{
                    if password == confirmPassword {
                        print("password confirmado")
                        currentUser = UserModel()
                        currentUser.email = formData["email"] as? String
                        currentUser.pwd = formData["password"] as? String
                        currentUser.confirmPwd =  formData["confirmPassword"] as? String
                        self.performSegue(withIdentifier: "gotoLastRegister", sender: self)
                    }else{
                        self.displayErrorAlert(title: "generalErrorMessageKey".localized, message: "La contraseña es incorrecta")
                    }
                }

            }
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        validateForm()
    }
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
