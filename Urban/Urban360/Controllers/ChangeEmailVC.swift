//
//  ChangeEmailVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/11/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Eureka
import NVActivityIndicatorView
class ChangeEmailVC: FormViewController {
    //  MARK: - Properties
    var activityIndicator : NVActivityIndicatorView!
    var currentUser:UserModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        createUserForm()
        self.addButtonOnNaviationBarRight(title: "Cambiar")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator = activityIndicator(view: view)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSAhora elige una nueva.egue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func createUserForm(){
        form +++ Section()
            <<< EmailRow() {
                $0.tag  = "email"
                $0.placeholder = "Nuevo correo"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleEmail())
                $0.validationOptions = .validatesOnChangeAfterBlurred
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Email")
                    //cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.cell.textField.textColor = .black
                        
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        row.placeholderColor = .red
                        row.placeholder = "Correo requerido"
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.placeholderColor = .gray
                        row.placeholder = "Correo electrónico"
                        row.cell.textField.textColor = .black
                        
                    }
            }
        
            <<< PasswordRow(){
                $0.tag  = "password"
                $0.placeholder = "Contraseña"
                $0.placeholderColor = .gray
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleMinLength(minLength: 8))
                }
                .cellSetup { cell, row in
                    cell.imageView?.image = #imageLiteral(resourceName: "Password")
                    cell.row.updateCell()
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.textLabel?.textColor = .red
                    }
                
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        row.placeholderColor = .red
                        row.placeholder = "Contraseña requerida"
                        row.cell.textField.textColor = .red
                    }
                    else{
                        row.placeholderColor = .gray
                        row.placeholder = "Contraseña"
                        row.cell.textField.textColor = .black
                        
                    }
            }
    }
    func validateForm(){
        let formData = form.values()
        
        let validationEmail = formData["email"]
        let validationPass = formData["password"]
        
        if !(validationEmail is String) || !(validationEmail as! String).isValidEmail(){
            self.displayErrorAlert(title: "errorMessage".localized, message: "errorEmail".localized)
        }else if !(validationPass is String) || (validationPass as! String).characters.count < 8{
            self.displayErrorAlert(title: "errorMessage".localized, message: "errorPass".localized)
        }else{
            let mail = formData["email"] as? String
            let password = formData["password"] as! String
            updateMail(mail: mail!, password: password)
        }
    }


    override func nextAction() {
        validateForm()
    }
    func updateMail(mail: String, password: String){
        self.activityIndicator?.startAnimating()
        AccountManager.updateMail(mail:mail, password: password){ updated, error, message in
            self.activityIndicator?.stopAnimating()
            if error == nil {         
                if updated! {
                    /*let usrDefaults = UserDefaults.standard
                    self.currentUser.email = mail
                    usrDefaults.set(UserModel.toDictionary(user: self.currentUser), forKey: "userSession")
                    usrDefaults.synchronize()*/
                    self.displayHandlAlert(title: "¡Listo!".localized, message: "Se ha actualizado correctamente tu correo.", completion:{action in self.navigationController?.popToRootViewController(animated: true)})
                    
                }else{
                    
                    if message == "email_exists"{
                        self.displayErrorAlert(title: "errorMessage".localized,message:"errorEmail".localized)
                    }else if message == "incorrect_password"{
                        self.displayErrorAlert(title: "errorMessage".localized,message:"errorPass".localized)
                    }else{
                    self.displayErrorAlert(title: "errorMessage".localized,message:"No pudimos actualizar su correo en este momento. Por favor intente de nuevo más tarde.")
                    }
                }
            }
            else{
                 self.displayErrorAlert(title: "errorMessage".localized,message:"No pudimos actualizar su correo en este momento. Por favor intente de nuevo más tarde.")
            }
        }
    }

}
