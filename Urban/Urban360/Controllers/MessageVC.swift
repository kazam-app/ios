//
//  MessageVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/26/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

class MessageVC: UIViewController {
    //MARK: -Properties
    var titleMessage: String = ""
    var descriptionMessage: String = ""
    var icon:String = ""
    
    
    //MARK: - Outlets
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var btnRetry: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.text = titleMessage
        self.lblDescription.text = descriptionMessage
        self.imgIcon.image = UIImage(named: icon)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
