//
//  InitialVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/17/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import BWWalkthrough
import CoreBluetooth
import AlamofireImage
class InitialVC: UIViewController {
    
    enum ConnectivityType {
        case network
        case bluetooth
        case location
    }
    
    // Mark: - Properties
    var container : ContainerHomeVC!
    let stb = UIStoryboard(name: "MainOnboarding", bundle: nil)
    var walkthrough:BWWalkthroughViewController!
    var manager:CBCentralManager!
    var peripheral:CBPeripheral!
    var isShowing:Bool = false
    var isBTEnabled:Bool = true
    var userDic:[String : AnyObject]!
    var validation:ValidationBO!
    var merchantBrand:MerchantModel!
    let usrDefaults = UserDefaults.standard
    var cardIsHidden = false
    @IBOutlet weak var brandHeightConstraint: NSLayoutConstraint!
    //Mark: Otulets
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var brandV: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblCategoryBrand: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var viewErrorMessage: UIView!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var lblMemberSince: UILabel!
    @IBOutlet weak var lblKazam: UILabel!
    
    
    var alertViewController :ConnectivityViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.topItem!.title = ""
        // Do any additional setup after loading the view.
       
        brandV.layer.cornerRadius = 10.0
        animateBrand(hide: true)
        lblUserName.text = "Urban360"
        
        let gesture = UISwipeGestureRecognizer.init(target: self, action: #selector(slide(gesture:)))
        gesture.direction = .up
        brandV.addGestureRecognizer(gesture)
        btnInfo.layer.cornerRadius = btnInfo.frame.width / 2
        btnInfo.layer.borderWidth = 1
        btnInfo.layer.borderColor = UIColor.white.cgColor
        
        blurShadow(view: lblUserName)
        blurShadow(view: lblKazam)
        blurShadow(view: lblMemberSince)
        brandV.layer.shadowOffset = CGSize(width: 0, height: 2)
        brandV.layer.shadowOpacity = 0.3
        brandV.layer.shadowRadius = 4
        //self.performSegue(withIdentifier: "goToUpdate", sender: self)
    }
    
    func blurShadow(view: UIView) {
        view.layer.shadowOffset = CGSize(width: 1, height: 3)
        view.layer.shadowOpacity = 0.6
        view.layer.shadowRadius = 4
    }
    
    func slide(gesture: UISwipeGestureRecognizer) {
        removeCurrentCard()
    }
    
    func animateBrand(hide: Bool) {
        
        if cardIsHidden == hide {
            return
        }
        
        self.brandHeightConstraint.constant = hide ?  self.view.bounds.height * 0.22 : self.view.bounds.height * 0.34
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseIn, animations: {
            self.brandV.layoutIfNeeded()
            self.brandV.updateConstraints()
            self.view.layoutIfNeeded()
            self.view.updateConstraints()
        }) { (finish) in
            if hide{
                self.brandV.alpha = 0
            }else{
                self.brandV.alpha = 1
            }
        }
        cardIsHidden = hide
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(checkConnectivity), name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOnName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkConnectivity), name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOffName), object: nil)
        self.navigationController?.isNavigationBarHidden = true
        tabBarController?.tabBar.isHidden = false
        self.hideErrorMessge()
        if let  dicUser = usrDefaults.object(forKey: "userSession"){
            userDic = dicUser as! [String : AnyObject]
            if !usrDefaults.bool(forKey: "onboardingInteraction"){
                showOnboardingInteraction()
            }
            
        }
        else{
            userDic = nil
            lblMemberSince.text = ""
            self.hideErrorMessge()

        }
        if !usrDefaults.bool(forKey: "onboardingGeneral"){
            showWalkthrough()
        }
        

        if userDic != nil{
           validation = ValidationBO.sharedInstance
            let userSession = UserModel.toUserModel(userDic: userDic)
            lblUserName.text = "\(userSession.name!) \(userSession.lastNames!)"
            lblMemberSince.text = "Miembro desde : " + Date().dateFromISOString(string: userSession.createdAt!).stDateString(format: .date_only_m_h).capitalized
            if let merchantDic:[String : AnyObject] =  usrDefaults.object(forKey: "merchantBrand") as? [String : AnyObject]{
                merchantBrand = MerchantModel.toMerchantModel(merchantDic: merchantDic)
                print("Showing brand")
                setupBrand()
                animateBrand(hide: false)
                
            }else{
                container.segueIdentifierReceivedFromParent("HomePunchsCards")
                let currentVC =  container.currentViewController as! PunchCardHomeVC
                currentVC.punchCardHomeDelegate = self
            }
            
            if isShowing{
                self.hideErrorMessge()
                isShowing = false
            }
            if userDic != nil{
                /*if self.validateReachability(){
                    if !self.validateKontaktLocationServices(){
                        self.showErrorMessageGeneral(title:"LocationTitleKey".localized , icon: "No Location", descriptionMessage: "LocationMessageKey".localized, titleKeyButton: "btnLocationEnableKey", showButton: true)
                        isShowing = true
                    }
                   else {
                        manager = CBCentralManager(delegate: self, queue: nil)
                    }
                }
                else{
                    self.showErrorMessageGeneral(title: "noInterneTitleKey".localized, icon: "noInternetIconKey".localized, descriptionMessage: "noInternetMessageKey".localized, titleKeyButton:nil, showButton: false)
                    isShowing = true
                }*/
                manager = CBCentralManager(delegate: self, queue: nil)
                checkConnectivity()
            }
        }
        else{
            lblUserName.text = "Tu Tarjeta"
            container.segueIdentifierReceivedFromParent("noAccount")
            removeCurrentCard()
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            // Your code with delay
            NotificationCenter.default.addObserver(self, selector: #selector(self.checkConnectivity), name: NSNotification.Name(rawValue: AppDelegate.notificationApplicationDidBecomeActive), object: nil)
        }
        
        if UpdateManager.isChangeVersión(){
            AccountManager.updateApp { (update) in
                if update ?? false{
                    self.performSegue(withIdentifier: "goToUpdate", sender: self)
                }
            }
        }
        
    }
    
    func checkConnectivity(){
        if userDic != nil{
            DispatchQueue.main.async {
                //First Network
                if !self.validateReachability() && self.merchantBrand == nil{
                    self.isShowing = true
                    self.showErrorConnectivity(type: .network, inView: self.viewErrorMessage)
                    return
                }
                    
                    //Second Location
                else if !self.validateLocationServices() && self.merchantBrand == nil{
                    //show error location
                    self.isShowing = true
                    self.showErrorConnectivity(type: .location, inView: self.viewErrorMessage)
                    return
                }
                    //Last Bluetooth
                else if !self.isBTEnabled && self.merchantBrand == nil{
                    //show error bluetooth
                    self.isShowing = true
                    self.showErrorConnectivity(type: .bluetooth, inView: self.viewErrorMessage)
                    return
                }
                
                //If all status ok, remove error view
                self.hideErrorConnectivity(inView: self.viewErrorMessage)
                self.isShowing = false
                self.brandV.fadeIn(withDuration: 0.2)
            }
        }
    }
    
    func showErrorConnectivity(type: ConnectivityType, inView: UIView){
        self.hideErrorConnectivity(inView: self.viewErrorMessage)
        self.brandV.fadeOut(withDuration: 0.2)
        let storyboard = UIStoryboard(name: "General", bundle: nil)
        switch type {
        case .network:
            alertViewController = storyboard.instantiateViewController(withIdentifier: "ConnectivityViewControllerNetwork") as? ConnectivityViewController
            break
        case .location:
            alertViewController = storyboard.instantiateViewController(withIdentifier: "ConnectivityViewControllerLocation") as? ConnectivityViewController
            alertViewController?.delegate = self as LocationActivateDelegate
            break
        case .bluetooth:
            alertViewController = storyboard.instantiateViewController(withIdentifier: "ConnectivityViewControllerBluetooth") as? ConnectivityViewController
            break
        }
        
        UIView.animate(withDuration: 0.5, animations: {inView.alpha = 1.0},
                       completion: {(value: Bool) in
                        // Add Child View Controller
                        self.addChildViewController(self.alertViewController!)
                        // Add Child View as Subview
                        self.viewErrorMessage.addSubview((self.alertViewController?.view)!)
                        // Configure Child View
                        self.alertViewController?.view.frame = (self.viewErrorMessage?.bounds)!
                        self.alertViewController?.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                        // Notify Child View Controller
                        self.alertViewController?.didMove(toParentViewController: self)
        })
        
    }
    
    public func hideErrorConnectivity(inView: UIView){
        UIView.animate(withDuration: 0.2, animations: {inView.alpha = 0.0},
                       completion: {(value: Bool) in
                        if self.self.alertViewController != nil{
                            // Notify Child View Controller
                            self.alertViewController?.willMove(toParentViewController: nil)
                            
                            // Remove Child View From Superview
                            self.alertViewController?.view.removeFromSuperview()
                            
                            // Notify Child View Controller
                            self.alertViewController?.removeFromParentViewController()
                        }
                        self.reloadData()
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //animateBrand(hide: true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOnName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOffName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppDelegate.notificationApplicationDidBecomeActive), object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func showWalkthrough(){
        
        // Get view controllers and build the walkthrough
        let stb = UIStoryboard(name: "MainOnboarding", bundle: nil)
        walkthrough = stb.instantiateViewController(withIdentifier: "walk") as! BWWalkthroughViewController
        /*let img:UIImage = UIImage(named: "Splash-Onboarding Background")!
        walkthrough.scrollview.backgroundColor = UIColor(patternImage: img)*/
        let page_one = stb.instantiateViewController(withIdentifier: "walk1")
        let page_two = stb.instantiateViewController(withIdentifier: "walk2")
        let page_three = stb.instantiateViewController(withIdentifier: "walk3")
        let page_fourth = stb.instantiateViewController(withIdentifier: "walk4")
        
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.add(viewController:page_one)
        walkthrough.add(viewController:page_two)
        walkthrough.add(viewController:page_three)
        walkthrough.add(viewController:page_fourth)
        
        
        self.present(walkthrough, animated: true, completion: nil)
    }
    func  showOnboardingInteraction()  {
        let stb = UIStoryboard(name: "MainOnboarding", bundle: nil)
        let interationOnboardingVC = stb.instantiateViewController(withIdentifier: "InteractionMain")
        self.present(interationOnboardingVC, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "container"{
            self.container = segue.destination as! ContainerHomeVC
        }
        else if  segue.identifier == "HomePunchsCards"{
            self.container = segue.destination as! ContainerHomeVC
            
        }
        else if  segue.identifier == "gotoPunchCards"{
            self.container = segue.destination as! ContainerHomeVC
            container.merchant = merchantBrand
        }
    }
    
    
    @IBAction func MerchantsAction(_ sender: Any) {
        let stb = UIStoryboard(name: "Merchants", bundle: nil)
        let giftsVC = stb.instantiateViewController(withIdentifier:"MerchantsHomeVC")
        self.navigationController?.pushViewController(giftsVC, animated: true)

    }
    
    @IBAction func gits(_ sender: Any) {
        let stb = UIStoryboard(name: "RewardsGifts", bundle: nil)
        let giftsVC = stb.instantiateViewController(withIdentifier:"RewardsGiftsHomeVC")
        self.navigationController?.pushViewController(giftsVC, animated: true)
    }
    
    override func reloadData() {
        if userDic != nil{
            if !validation.isLocationEnabled{
                
            }
            //validation.requestLocationAlways()
        }
    }
    
    @IBAction func closeCard(_ sender: Any) {
        removeCurrentCard()
    }
    
    func setupBrand(){
        //Flujo Anterior
        //self.container.segueIdentifierReceivedFromParent("gotoPunchCards")
        self.container.segueIdentifierReceivedFromParent("gotoNewPunchCards")
        let imageFilter = CircleFilter()
        if merchantBrand.urlImage! != ""{
            self.imgLogo.af_setImage(
                withURL: URL(string: merchantBrand.urlImage!)!,
                placeholderImage: UIImage(named: "MerchantLogoPlaceHolder"),
                filter: imageFilter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgLogo.image = UIImage(named: "MerchantLogoPlaceHolder")
        }
        lblBrand.text = merchantBrand.name
        lblCategoryBrand.text = merchantBrand.category
        if let color:String = merchantBrand.color {
            brandV.backgroundColor = UIColor(hexString: color)
            
        }
        lblRating.attributedText = "$$$$$".rateText(rate: merchantBrand.budgetRating ?? 0, colorHighlight: UIColor.white)
    }
    
    func removeCurrentCard() {
        usrDefaults.removeObject(forKey: "merchantBrand")
        usrDefaults.synchronize()
        merchantBrand = nil
        animateBrand(hide: true)
        resetContent()
        checkConnectivity()
    }
    
    func resetContent(){
        if merchantBrand == nil && usrDefaults.object(forKey: "userSession") != nil{
            self.container.segueIdentifierReceivedFromParent("HomePunchsCards")
        }
    }
}
extension InitialVC:BWWalkthroughViewControllerDelegate{
    // MARK: - Walkthrough delegate -
    func walkthroughPageDidChange(_ pageNumber: Int) {
        //print("Current Page \(pageNumber)")
        if pageNumber == 3 {
            walkthrough.closeButton?.alpha = 1
        }
        else{
            walkthrough.closeButton?.alpha = 0
        }
    }
    
    func walkthroughCloseButtonPressed() {
        usrDefaults.set(true, forKey: "onboardingGeneral")
        usrDefaults.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
}
extension InitialVC:CBCentralManagerDelegate,CBPeripheralDelegate{
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == CBManagerState.poweredOn {
            central.scanForPeripherals(withServices: nil, options: nil)
            isBTEnabled = true
        } else {
            print("Bluetooth not available.")
            isBTEnabled = false
        }
        checkConnectivity()
    }
}

extension InitialVC:PunchCardHomeDelegate{
    func beaconFound() {
        if let merchantDic:[String : AnyObject] =  usrDefaults.object(forKey: "merchantBrand") as? [String : AnyObject]{
            merchantBrand = MerchantModel.toMerchantModel(merchantDic: merchantDic)
            print("Showing brand")
            setupBrand()
            animateBrand(hide: false)
            checkConnectivity()
        }
    }
}

extension InitialVC : LocationActivateDelegate{
    func activateLocationSuccess() {
        checkConnectivity()
        resetContent()
    }
}

