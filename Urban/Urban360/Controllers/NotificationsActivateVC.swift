//
//  NotificationsActivateVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import OneSignal
import Pulsator
class NotificationsActivateVC: UIViewController {
    
    // MARK: - Properties
    var currentUser:UserModel!
    let pulsator = Pulsator()
    
    // MARK: - Outlets
    @IBOutlet weak var imgLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         addButtonOnNaviationBarRight(title: "nextActionKey".localized)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addPulse()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgLogo.layer.layoutIfNeeded()
        pulsator.position = imgLogo.layer.position
    }
    func addPulse(){
        pulsator.numPulse = 7
        pulsator.radius = 200.0
        pulsator.animationDuration = kMaxDuration
        pulsator.backgroundColor = UIColor.uiCornflowerBlue.cgColor
        imgLogo.layer.superlayer?.insertSublayer(pulsator, below: imgLogo.layer)
        
        pulsator.start()
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoEndRegister"{
            let destinationVC = segue.destination as! InvitationCodeVC
            destinationVC.currentUser = currentUser
        }

    }
    

    @IBAction func EneabledNotificationsAction(_ sender: Any) {
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            if accepted {
                print("User accepted notifications: \(accepted)")
            }
            self.gotoNext()
            })
    }
    override func nextAction() {
        gotoNext()
    }
    func gotoNext(){
        self.performSegue(withIdentifier: "gotoEndRegister", sender: self)
    }
}
