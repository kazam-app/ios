//
//  ProfileVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/30/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import AlamofireImage
import NVActivityIndicatorView
class ProfileVC: UIViewController {
    
    //MARK: - Properties
    var activityIndicator : NVActivityIndicatorView!
    let usrDefaults = UserDefaults.standard
    var userDic:[String:AnyObject]! = [:]
    var popupVC:PopupVC!
    //MARK: -Outlets
    
    @IBOutlet weak var tblSettings: UITableView!
    @IBOutlet weak var lblCopyRight: UILabel!
    //@IBOutlet weak var btnCloseSession: UIButton!
    //@IBOutlet weak var bottomSpace: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.activityIndicator = activityIndicator(view: view)
        checkUser()
    }
    
    func checkUser() {
        updateView()
        if usrDefaults.dictionary(forKey: "userSession") != nil{
            getAccount()
        }
    }
    
    func updateView() {
        if let  userDicTmp = usrDefaults.dictionary(forKey: "userSession"){
            userDic = userDicTmp as [String : AnyObject]
            //btnCloseSession.alpha = 1
            //bottomSpace.constant = 117
            self.tblSettings.reloadData()
            self.view.layoutIfNeeded()
        }
        else{
            //btnCloseSession.alpha = 0
            //bottomSpace.constant = 44
            self.view.layoutIfNeeded()
        }
    }
    
    func getAccount() {
        AccountManager.getAccount { (userModel, error) in
            if error == nil{
                if userModel != nil{
                    if userModel?.email != nil{
                        self.userDic["email"] = userModel?.email as AnyObject
                    }
                    self.usrDefaults.set(self.userDic, forKey: "userSession")
                    self.usrDefaults.synchronize()
                }
            }
            self.updateView()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        switch segue.identifier! {
        case "gotoEditProfile":
            let destinationVC = segue.destination as! EditProfileVC
            destinationVC.currentUser = UserModel.toUserModel(userDic: userDic)
            break
        case "gotoChangeEmail":
            let destinationVC = segue.destination as! ChangeEmailVC
            destinationVC.currentUser = UserModel.toUserModel(userDic: userDic)
            break
        case "gotoChangePassword":
            break
        default:
            break
        }
    }
    
    
    @IBAction func closeSessionAction(_ sender: Any) {
        self.displayConfirmHandlAlert(title: "Confirmación", message: "¿Realmente desea cerrar su sesión?", completion: {(action) in
           self.closeSession()
          })
    }
    
    func closeSession() {
        self.activityIndicator.startAnimating()
        AccountManager.closeSession(){ succes, error, statusCode in
            self.activityIndicator.stopAnimating()
            if error == nil || statusCode == 401{
                self.usrDefaults.removeObject(forKey: "userSession")
                self.userDic =  [:]
                UIView.animate(withDuration: 0.5, animations: {
                    self.tblSettings.reloadData()
                    //self.btnCloseSession.alpha = 0
                    //self.bottomSpace.constant = 44
                    self.view.layoutIfNeeded()
                })

            }
            else{
                self.displayOKAlert(title: "generalErrorMessageKey".localized, message: "tuvimos un problema al intentar cerrar tu sesión intenta mas tarde")
            }
        }
    }
}
extension ProfileVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "General", bundle: nil)
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        if !userDic.isEmpty {
            
            switch indexPath.section {
            case 0:
                if( indexPath.row == 0 || indexPath.row == 1||indexPath.row == 2||indexPath.row == 3 ){
                    self.performSegue(withIdentifier: "gotoEditProfile", sender: self)
                }
                else if (indexPath.row == 4){
                    self.performSegue(withIdentifier: "gotoChangeEmail", sender: self)
                }
                else if (indexPath.row == 5){
                    self.performSegue(withIdentifier: "gotoChangePassword", sender: self)
                }
                break
            case 1:
                self.performSegue(withIdentifier: "gotoNotifications", sender: self)
                break
            case 2:
                
                let container:UINavigationController = sb.instantiateViewController(withIdentifier: "ContainerMessageNC") as! UINavigationController
                let destinationVC = container.viewControllers[0] as! MessageDetailVC
                switch indexPath.row {
                case 0:
                    destinationVC.title = "Preguntas Frecuentes"
                    destinationVC.option = 0
                    self.present(container, animated: true, completion: nil)
                    break
                case 1:
                    self.performSegue(withIdentifier: "goToContact", sender: self)
                    break
                    
                default:
                    break
                }
                break
            case 3:
                
                let container:UINavigationController = sb.instantiateViewController(withIdentifier: "ContainerMessageNC") as! UINavigationController
                let destinationVC = container.viewControllers[0] as! MessageDetailVC
                switch indexPath.row {
                case 0:
                    destinationVC.title = "Términos y Condiciones"
                    destinationVC.option = 1
                    self.present(container, animated: true, completion: nil)
                    break
                case 1:
                    destinationVC.title = "Política de Privacidad"
                    destinationVC.option = 2
                    self.present(container, animated: true, completion: nil)
                    break
                default:
                    break
                }
                break
                
            default:
                break
                
            }
        }
        else if indexPath.section < 2{
            self.popupVC = sb.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
            popupVC.modalPresentationStyle = .overFullScreen
            popupVC.titlePopup = "Regístrate y comienza a ganar"
            popupVC.detailPopup = "Regístrate para comenzar a ganar sellos y premios de tus marcas favoritas."
            popupVC.isSingle = false
            popupVC.popupDelegate = self
            
            
            self.present(popupVC, animated: true, completion: nil)
            
        }
        else if indexPath.section >= 2 {
            
            switch indexPath.section {
            case 2:
                
                let container:UINavigationController = sb.instantiateViewController(withIdentifier: "ContainerMessageNC") as! UINavigationController
                let destinationVC = container.viewControllers[0] as! MessageDetailVC
                switch indexPath.row {
                case 0:
                    destinationVC.title = "Preguntas Frecuentes"
                    destinationVC.option = 0
                    self.present(container, animated: true, completion: nil)
                    break
                    
                default:
                    break
                }
                break
            case 3:
                
                let container:UINavigationController = sb.instantiateViewController(withIdentifier: "ContainerMessageNC") as! UINavigationController
                let destinationVC = container.viewControllers[0] as! MessageDetailVC
                switch indexPath.row {
                case 0:
                    destinationVC.title = "Términos y Condiciones"
                    destinationVC.option = 1
                    self.present(container, animated: true, completion: nil)
                    break
                case 1:
                    destinationVC.title = "Política de Privacidad"
                    destinationVC.option = 2
                    self.present(container, animated: true, completion: nil)
                    break
                default:
                    break
                }
                break
            default:
                break
            }
        }
    }
}
extension ProfileVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 6
        case 1:
            return 1
        case 2:
            return 2
        case 3:
             return 2
        case 4:
            return 1
        default:
           return 0
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "MI CUENTA"
        case 1:
            return "NOTIFICACIONES"
        case 2:
            return "AYUDA"
        case 3:
            return "LEGAL"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 4 {
            return userDic.isEmpty ? 74 : 118
        }else{
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let container:UIView = UIView()
        container.backgroundColor = UIColor.uiSilverBG
        container.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44)
        let label : UILabel = UILabel()
        label.frame = CGRect(x: 8, y: 5, width: self.view.bounds.size.width-10, height: 20)
        label.font = label.font.withSize(13)
        label.textColor = UIColor.uiSilver
        container.addSubview(label)
        switch section {
        case 0:
            label.text = "MI CUENTA"
            break
        case 1:
            label.text = "NOTIFICACIONES"
            break
        case 2:
            label.text = "AYUDA"
            break
        case 3:
            label.text = "LEGAL"
            break
        case 4:
            label.text = ""
            break
        default:
            break
        }
        
        return container
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: indexPath.section == 4 ? "cell_log_out" :"settingsCell") as! ProfileTVC
        cell.delegate = self
        if userDic.isEmpty {
            cell.setupCell(user:nil , indexPath: indexPath)
        }
        else{
            let currentUser = UserModel.toUserModel(userDic: userDic)
            cell.setupCell(user: currentUser, indexPath: indexPath)
        }
        
        //tableView.separatorStyle = indexPath.section == 4 ? .none : .default
        
        if indexPath.section == 4{
            tableView.separatorStyle = .none
        }else{
            tableView.separatorStyle = .singleLine
        }
        
        return cell
    }
    func gotoRegister(){
        self.popupVC.cancelAction(self)
        
    }
}
extension ProfileVC:PopupDelegate{
    func close() {
        popupVC.dismiss(animated: true, completion:{
            self.tabBarController?.selectedIndex = 1
        })
        
    }
}

extension ProfileVC: LogoutDelegate{
    func logout(){
        closeSessionAction(UIButton())
    }
}
