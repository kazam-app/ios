//
//  PopupVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/13/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import AlamofireImage
@objc protocol PopupDelegate {
    @objc optional func close()
    @objc optional func confirm()
    @objc optional func confirmSingle()
}

class PopupVC: UIViewController {
    
    //MARK:  - Properties
    var titlePopup:String = ""
    var detailPopup:String = ""
    var urlLogo:String = ""
    var isSingle:Bool = true
    var popupDelegate:PopupDelegate!
    
    
    // MARK: - Outlets

    @IBOutlet weak var popupV: UIView!
    @IBOutlet weak var imgBrand: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    
    @IBOutlet weak var btnOK: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        popupV.layer.cornerRadius = 10
        popupV.layer.masksToBounds = true
        view.isOpaque = false
        
        lblTitle.text = titlePopup
        lblDetail.text = detailPopup
        if isSingle{
            btnCancel.alpha = 0
            btnConfirm.alpha = 0
            if urlLogo != ""{
                let imageFilter = CircleFilter()
                self.imgBrand.af_setImage(
                    withURL: URL(string: urlLogo)!,
                    placeholderImage: UIImage(named: "logoInteraction"),
                    filter:imageFilter,
                    imageTransition: .crossDissolve(0.2)
                )
            }
        }
        else{
            btnOK.alpha = 0
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmAction(_ sender: Any) {
         self.popupDelegate.close!()

    }
  
    @IBAction func confirmOkAction(_ sender: Any) {
        self.popupDelegate.confirmSingle!()
    }
}
