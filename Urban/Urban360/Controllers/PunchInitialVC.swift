//
//  PunchInitialVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/24/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import AlamofireImage
import Pulsator
import CoreLocation
import AVFoundation
import KontaktSDK
import CoreBluetooth

@objc protocol PunchCardDelegate {
    @objc optional func reedem()
}

@objc protocol PunchCardDelegateUpdate {
    @objc optional func updateView()
}

class PunchInitialVC: BaseViewController {
    
    // MARK: - Properties
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!    
    var currentMerchant:MerchantModel!
    var currentPunchCard:PunchCardModel!
    let usrDefaults = UserDefaults.standard
    var activityIndicator : NVActivityIndicatorView!
    var  isReedem = false
    var isBluetoothScan = true
    var reedemPunchCard:PunchCardModel!
    var punchCardDelegate:PunchCardDelegate!
    var delegateUpdate: PunchCardDelegateUpdate!
    let pulsator = Pulsator()
    //let locationManager = CLLocationManager()
    var beaconManager: KTKBeaconManager!
    //var beaconRegion:CLBeaconRegion!
    var beaconRegion: KTKBeaconRegion!
    var alertViewController :ConnectivityViewController?
    var manager:CBCentralManager!
    var isBTEnabled:Bool = true
    
    var popupVC:PopupVC!
    let sb = UIStoryboard(name: "General", bundle: nil)
    // MARK: - Outlets
    
    @IBOutlet weak var headerV: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var cameraV: UIView!
    @IBOutlet weak var emptyStateV: UIView!
    @IBOutlet weak var noAccessCameraV: UIView!
    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var messageV: UIView!
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var bluetoothV: UIView!
    
    @IBOutlet weak var imgBluetoothLogo: UIImageView!

    @IBOutlet weak var optScan: UISegmentedControl!
    @IBOutlet weak var viewErrorMessage: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblErrorMessage.alpha = 0
        // Do any additional setup after loading the view.
        lblMerchantName.text = currentMerchant.name
        lblCategory.text = currentMerchant.category
        lblRating.attributedText = "$$$$$".rateText(rate: currentMerchant.budgetRating ?? 0 , colorHighlight: UIColor.white)
        if let color:String = currentMerchant.color {
            headerV.backgroundColor = UIColor(hexString: color)
            navigationController?.navigationBar.barTintColor = UIColor(hexString: color)
        }
        
        let imageFilter = CircleFilter()
        if currentMerchant.urlImage! != ""{
            self.imgLogo.af_setImage(
                withURL: URL(string: currentMerchant.urlImage!)!,
                placeholderImage: UIImage(named: "MerchantLogoDetailPH"),
                filter: imageFilter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgLogo.image = UIImage(named: "MerchantLogoDetailPH")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager = CBCentralManager(delegate: self, queue: nil)
        self.suscribeToReachability()
        self.activityIndicator = activityIndicator(view: view)
        if isBluetoothScan{
            lblMessage.text = "Acerca tu celular al sello digital en la caja para recibir tu sello."
            bluetoothV.alpha = 1
            cameraV.alpha = 0
            addPulse()
            if self.validateLocationServices(){
                //locationManager.delegate = self
                beaconManager = KTKBeaconManager(delegate: self)
                setupBeacon()
            }

        }
        else{
            bluetoothV.alpha = 0
            cameraV.alpha = 1
            setupCamera()
            if (captureSession?.isRunning == false && usrDefaults.bool(forKey: "isCameraEnabled")) {
                captureSession.startRunning();
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkConnectivity), name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOnName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkConnectivity), name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOffName), object: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            // Your code with delay
            NotificationCenter.default.addObserver(self, selector: #selector(self.checkConnectivity), name: NSNotification.Name(rawValue: AppDelegate.notificationApplicationDidBecomeActive), object: nil)
        }
     }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        manager.stopScan()
        if (captureSession?.isRunning == true && usrDefaults.bool(forKey: "isCameraEnabled")) {
            captureSession.stopRunning();
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOnName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOffName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppDelegate.notificationApplicationDidBecomeActive), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgBluetoothLogo.layer.layoutIfNeeded()
        pulsator.position = imgBluetoothLogo.layer.position
    }
    
    func checkConnectivity(){
        if !self.validateReachability(){
            self.showErrorConnectivity(type: .network, inView: self.viewErrorMessage)
            return
        }
            
            //Second Location
        else if !self.validateLocationServices() && self.optScan.selectedSegmentIndex == 0{
            //show error location
            self.showErrorConnectivity(type: .location, inView: self.viewErrorMessage)
            return
        }
            //Last Bluetooth
        else if !self.isBTEnabled && self.optScan.selectedSegmentIndex == 0{
            //show error bluetooth
            self.showErrorConnectivity(type: .bluetooth, inView: self.viewErrorMessage)
            return
        }
        
        //If all status ok, remove error view
        self.hideErrorConnectivity(inView: self.viewErrorMessage)
    }
    
    func showErrorConnectivity(type: InitialVC.ConnectivityType, inView: UIView){
        self.hideErrorConnectivity(inView: self.viewErrorMessage)
        let storyboard = UIStoryboard(name: "General", bundle: nil)
        switch type {
        case .network:
            alertViewController = storyboard.instantiateViewController(withIdentifier: "ConnectivityViewControllerNetworkValidation") as? ConnectivityViewController
            break
        case .location:
            alertViewController = storyboard.instantiateViewController(withIdentifier: "ConnectivityViewControllerLocationValidation") as? ConnectivityViewController
            break
        case .bluetooth:
            alertViewController = storyboard.instantiateViewController(withIdentifier: "ConnectivityViewControllerBluetoothValidation") as? ConnectivityViewController
            break
        }
        
        UIView.animate(withDuration: 0.5, animations: {inView.alpha = 1.0},
                       completion: {(value: Bool) in
                        // Add Child View Controller
                        self.addChildViewController(self.alertViewController!)
                        // Add Child View as Subview
                        self.viewErrorMessage.addSubview((self.alertViewController?.view)!)
                        // Configure Child View
                        self.alertViewController?.view.frame = (self.viewErrorMessage?.bounds)!
                        self.alertViewController?.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                        // Notify Child View Controller
                        self.alertViewController?.didMove(toParentViewController: self)
        })
        
    }
    
    public func hideErrorConnectivity(inView: UIView){
        UIView.animate(withDuration: 0.2, animations: {inView.alpha = 0.0},
                       completion: {(value: Bool) in
                        if self.self.alertViewController != nil{
                            // Notify Child View Controller
                            self.alertViewController?.willMove(toParentViewController: nil)
                            
                            // Remove Child View From Superview
                            self.alertViewController?.view.removeFromSuperview()
                            
                            // Notify Child View Controller
                            self.alertViewController?.removeFromParentViewController()
                        }
                        self.reloadData()
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func enableCameraAction(_ sender: Any) {
        // usrDefaults.bool(forKey: "isCameraEnabled")
        usrDefaults.set(true, forKey: "isCameraEnabled")
        usrDefaults.synchronize()
        setupCamera()
        
    }
    @IBAction func changeScanAction(_ sender: Any) {
        if optScan.selectedSegmentIndex == 0{
            lblMessage.text = "Acerca tu celular al sello digital en la caja para recibir tu sello."
            bluetoothV.alpha = 1
            cameraV.alpha = 0
            isBluetoothScan = true
            addPulse()
            if self.validateLocationServices(){
                //locationManager.delegate = self
                beaconManager = KTKBeaconManager(delegate: self)
                setupBeacon()
            }
            if (captureSession?.isRunning == true && usrDefaults.bool(forKey: "isCameraEnabled")) {
                captureSession.stopRunning();
            }

        }
        else{
            bluetoothV.alpha = 0
            cameraV.alpha = 1
            isBluetoothScan = false
            setupCamera()
            if (captureSession?.isRunning == false && usrDefaults.bool(forKey: "isCameraEnabled")) {
                captureSession.startRunning();
            }

        }
        
        checkConnectivity()
    }
    
    
    func setupBeacon(){
        if let dicBeacon:[String:AnyObject] = self.usrDefaults.object(forKey: "currentBeacon") as? [String : AnyObject]{
            let currentBeacon:BeaconModel = BeaconModel.toBeaconModel(beaconDic: dicBeacon)
            if let uuid = UUID(uuidString: currentBeacon.uid!) {
                /*let beaconReg = CLBeaconRegion(
                    proximityUUID: uuid,
                    identifier: "iBeacon")*/
                //beaconRegion = beaconReg
                //locationManager.startMonitoring(for: beaconRegion)
                //locationManager.startRangingBeacons(in: beaconRegion)
                beaconRegion = KTKBeaconRegion(proximityUUID: uuid, identifier: "iBeacon")
                beaconManager.startMonitoring(for: beaconRegion)
                beaconManager.startRangingBeacons(in: beaconRegion)
            }
        }
    }
    
    func addPulse(){
        pulsator.numPulse = 5
        pulsator.radius = 150.0
        pulsator.animationDuration = Double(0.5) * kMaxDuration
        pulsator.backgroundColor = UIColor.uiCornflowerBlue.cgColor
        imgBluetoothLogo.layer.superlayer?.insertSublayer(pulsator, below: imgBluetoothLogo.layer)
        pulsator.start()
    }

    
    func setupCamera(){
        lblMessage.text = "Escanea el código que te mostrará el cajero para recibir tu sello."
        if  usrDefaults.bool(forKey: "isCameraEnabled") {
            emptyStateV.alpha = 0
            noAccessCameraV.alpha = 0
            cameraV.alpha = 1
            messageV.alpha = 1
            cameraV.alpha = 1
            captureSession = AVCaptureSession()
            
            let videoCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
            let videoInput: AVCaptureDeviceInput
            
            do {
                videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                
                emptyStateV.alpha = 1
                noAccessCameraV.alpha = 1
                cameraV.alpha = 0
                messageV.alpha = 0
                cameraV.alpha = 0
                
                usrDefaults.set(false, forKey: "isCameraEnabled")
                usrDefaults.synchronize()
                self.view.reloadInputViews()
                return
            }
            
            if (captureSession.canAddInput(videoInput)) {
                captureSession.addInput(videoInput)
            } else {
                failed();
                return;
            }
            
            let metadataOutput = AVCaptureMetadataOutput()
            
            if (captureSession.canAddOutput(metadataOutput)) {
                captureSession.addOutput(metadataOutput)
                
                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
            } else {
                failed()
                return
            }
            
            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
            previewLayer.frame = cameraV.layer.bounds;
            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            cameraV.layer.addSublayer(previewLayer);
            self.view.reloadInputViews()
            captureSession.startRunning();
        }
        else{
            cameraV.alpha = 0
            messageV.alpha = 0
        }
        
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    func found(code: String) {
        if isReedem{
            //reedemPunchCard(qrCode: code)
            reedemPunchCard(qrCode: code, beaconId: nil, beaconMajor: nil, beaconMinor: nil)
        }
        else{
             punchCard(qrCode: code, beaconId: nil, beaconMajor: nil, beaconMinor: nil)
        }
       
    }
    
    func  punchCard(qrCode:String?,beaconId:String?,beaconMajor:String?,beaconMinor:String?){
        self.lblErrorMessage.fadeOut(withDuration: 0.25)
        self.activityIndicator.startAnimating()
        PunchsCardsManager.punchCard(merchantId: currentMerchant.merchantId!, QRToken: qrCode, beaconId: beaconId, beaconMajor: beaconMajor, beaconMinor: beaconMinor, punchCard: currentPunchCard){ punchCard,error,responseCode in
            self.activityIndicator.stopAnimating()
            if error == nil{
                if punchCard != nil{
                    if self.delegateUpdate != nil{
                        self.delegateUpdate.updateView!()
                    }
                    self.dismiss(animated: true, completion:nil)
                }
            }
            else{
                if responseCode == 429{
                    self.popupVC = self.sb.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
                    self.popupVC.titlePopup = "¡Oops! Llegaste al Límite"
                    self.popupVC.detailPopup = "Has llegado al límite de sellos que puedes obtener de esta marca hoy, vuelve mañana."
                    self.popupVC.modalPresentationStyle = .overFullScreen
                    self.popupVC.popupDelegate = self as PopupDelegate
                    self.present(self.popupVC, animated: true, completion: nil)
                }else{
                    self.lblErrorMessage.fadeIn(withDuration: 0.25)
                    if self.isBluetoothScan{
                        self.beaconManager.stopMonitoring(for: self.beaconRegion)
                        self.beaconManager.stopRangingBeacons(in: self.beaconRegion)
                    }
                    else{
                        self.captureSession.startRunning()
                    }
                }
            }
        }
    }
    
    func  reedemPunchCard(qrCode:String?,beaconId:String?,beaconMajor:String?,beaconMinor:String?){
        self.lblErrorMessage.fadeOut(withDuration: 0.25)
        self.activityIndicator.startAnimating()
        PunchsCardsManager.reedemPunchCard(merchantId: currentMerchant.merchantId!, QRToken: qrCode, beaconId: beaconId, beaconMajor: beaconMajor, beaconMinor: beaconMinor, punchCard: currentPunchCard){ punchCard,error,responseCode in
            self.activityIndicator.stopAnimating()
            if error == nil{
                if punchCard != nil{
                    self.reedemPunchCard = self.currentPunchCard
                    self.reedemPunchCard.reedemCode = punchCard!.reedemCode!
                    /*
                     lblEndDatePromotion.text = punchCard.expiresAt
                     lblRulesPromotion.text = punchCard.rules
                     txtvConditionsPromotion.text = punchCard.terms
                     */
                    let reedemLocal = [ReedemManager.KEY_REEDEM_TIME : Date().timeIntervalSince1970 , ReedemManager.KEY_REEDEM_CODE : punchCard?.reedemCode ?? "", ReedemManager.KEY_REEDEM_TITLE : self.reedemPunchCard.prize ?? "", ReedemManager.KEY_REEDEM_END_DATE_PROMOTION : self.reedemPunchCard.expiresAt ?? "", ReedemManager.KEY_REEDEM_RULES_PROMOTION : self.reedemPunchCard.rules ?? "", ReedemManager.KEY_REEDEM_CONDITIONS_PROMOTION : self.reedemPunchCard.terms ?? "" ,ReedemManager.KEY_REEDEM_MERCHANT_ID : self.currentMerchant.merchantId ?? ""] as [String : Any]
                    ReedemManager.addReedemItem(dic: reedemLocal)
                        self.dismiss(animated: true, completion:{
                            if self.delegateUpdate != nil{
                                self.delegateUpdate.updateView!()
                            }
                            self.punchCardDelegate.reedem!()
                        })
                }
            }
            else{
                self.lblErrorMessage.fadeIn(withDuration: 0.25)
                 if self.isBluetoothScan{
                    self.beaconManager.stopMonitoring(for: self.beaconRegion)
                    self.beaconManager.stopRangingBeacons(in: self.beaconRegion)
                }
                else{
                    self.captureSession.startRunning()
                }
                

            }
        }
    }
    
    //MARK BaseViewController methods
    
    override func notInternetConnection() {
        self.viewErrorMessage.fadeIn(withDuration: 0.5)
    }
    
    override func connectedToInternet() {
        self.viewErrorMessage.fadeOut(withDuration: 0.3)
    }

}
extension PunchInitialVC:AVCaptureMetadataOutputObjectsDelegate{
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject;
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: readableObject.stringValue);
        }
    }
    
}

/*extension PunchInitialVC:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if let beaconRegion = region as? CLBeaconRegion {
            print("DID ENTER REGION: uuid: \(beaconRegion.proximityUUID.uuidString)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if let beaconRegion = region as? CLBeaconRegion {
            print("DID EXIT REGION: uuid: \(beaconRegion.proximityUUID.uuidString)")
        }
    }
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        for beacon in beacons {
             print("proximidad del beacon: \(beacon.accuracy)")
            var beaconProximity: String;
            switch (beacon.proximity) {
            case CLProximity.unknown:    beaconProximity = "Unknown";
            case CLProximity.far:        beaconProximity = "Far";
            case CLProximity.near:       beaconProximity = "Near";
            case CLProximity.immediate:
                beaconProximity = "Immediate";
                
                if beacon.accuracy < 0.2{
                    locationManager.stopMonitoring(for: beaconRegion)
                    locationManager.stopRangingBeacons(in: beaconRegion)
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    if isReedem{
                        //reedemPunchCard(qrCode: code)
                        reedemPunchCard(qrCode: nil, beaconId: beacon.proximityUUID.uuidString.lowercased(), beaconMajor: beacon.major.stringValue, beaconMinor: beacon.minor.stringValue)
                    }
                    else{
                        punchCard(qrCode: nil, beaconId: beacon.proximityUUID.uuidString.lowercased(), beaconMajor: beacon.major.stringValue, beaconMinor: beacon.minor.stringValue)
                    }
                }
                

                break
            }
            print("BEACON RANGED: uuid: \(beacon.proximityUUID.uuidString) major: \(beacon.major)  minor: \(beacon.minor) proximity: \(beaconProximity)")
        }
    }
}*/

extension PunchInitialVC: KTKBeaconManagerDelegate{
    
    func beaconManager(_ manager: KTKBeaconManager, didEnter region: KTKBeaconRegion) {
        // Decide what to do when a user enters a range of your region; usually used
        // for triggering a local notification and/or starting a beacon ranging
        print("DID ENTER REGION: uuid: \(region.proximityUUID.uuidString)")
    }
    
    func beaconManager(_ manager: KTKBeaconManager, didExitRegion region: KTKBeaconRegion) {
        // Decide what to do when a user exits a range of your region; usually used
        // for triggering a local notification and stoping a beacon ranging
        print("DID EXIT REGION: uuid: \(region.proximityUUID.uuidString)")
    }
    
    func beaconManager(_ manager: KTKBeaconManager, didRangeBeacons beacons: [CLBeacon], in region: KTKBeaconRegion) {
        for beacon in beacons {
            print("proximidad del beacon: \(beacon.accuracy)")
            var beaconProximity: String;
            switch (beacon.proximity) {
            case CLProximity.unknown:    beaconProximity = "Unknown";
            case CLProximity.far:        beaconProximity = "Far";
            case CLProximity.near:       beaconProximity = "Near";
            case CLProximity.immediate:
                beaconProximity = "Immediate";
                
                if beacon.accuracy < 0.1{
                    beaconManager.stopMonitoring(for: beaconRegion)
                    beaconManager.stopRangingBeacons(in: beaconRegion)
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    if isReedem{
                        //reedemPunchCard(qrCode: code)
                        reedemPunchCard(qrCode: nil, beaconId: beacon.proximityUUID.uuidString.lowercased(), beaconMajor: beacon.major.stringValue, beaconMinor: beacon.minor.stringValue)
                    }
                    else{
                        punchCard(qrCode: nil, beaconId: beacon.proximityUUID.uuidString.lowercased(), beaconMajor: beacon.major.stringValue, beaconMinor: beacon.minor.stringValue)
                    }
                }
                
                
                break
            }
            print("BEACON RANGED: uuid: \(beacon.proximityUUID.uuidString) major: \(beacon.major)  minor: \(beacon.minor) proximity: \(beaconProximity)")
        }
    }
}

extension PunchInitialVC:CBCentralManagerDelegate,CBPeripheralDelegate{
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == CBManagerState.poweredOn {
            central.scanForPeripherals(withServices: nil, options: nil)
            isBTEnabled = true
        } else {
            print("Bluetooth not available.")
            isBTEnabled = false
        }
        checkConnectivity()
    }
}

extension PunchInitialVC : PopupDelegate{
    func confirmSingle() {
        self.popupVC.dismiss(animated: true) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
