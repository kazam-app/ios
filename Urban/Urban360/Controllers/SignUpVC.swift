//
//  SignUpVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/12/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class SignUpVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func signUpMailAction(_ sender: Any) {
        SignupBO.signupMail(controller: self)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let navController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")
        self.present(navController!, animated: true, completion: nil)
    }
    
    @IBAction func signupFBAction(_ sender: Any) {
        Tracker.trackEvent(event: Tracker.EVENT_SIGNUP_FB_START, parameters: nil)
        SignupBO.singupFB(controller: self)
    }
    
    //MARK Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "performTerms"{
            let container:UINavigationController = segue.destination as! UINavigationController
            let destinationVC = container.viewControllers[0] as! MessageDetailVC
            destinationVC.title = "Términos y Condiciones"
            destinationVC.option = 1

        }else if segue.identifier == "performPolicy"{
            let container:UINavigationController = segue.destination as! UINavigationController
            let destinationVC = container.viewControllers[0] as! MessageDetailVC
            destinationVC.title = "Política de Privacidad"
            destinationVC.option = 2
        }
    }
    
    /*
     let container:UINavigationController = sb.instantiateViewController(withIdentifier: "ContainerMessageNC") as! UINavigationController
     let destinationVC = container.viewControllers[0] as! MessageDetailVC
     switch indexPath.row {
     case 0:
     destinationVC.title = "Términos y Condiciones"
     destinationVC.option = 1
     self.present(container, animated: true, completion: nil)
     break
     case 1:
     destinationVC.title = "Política de Privacidad"
     destinationVC.option = 2
     self.present(container, animated: true, completion: nil)
     break
     default:
     break
     }

     */
  
}
