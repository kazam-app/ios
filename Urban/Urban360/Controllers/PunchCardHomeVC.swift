//
//  PunchCardHomeVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/12/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Pulsator
import CoreLocation
import AVFoundation
import KontaktSDK
import ProximitySDK

let kMaxRadius: CGFloat = 200
let kMaxDuration: TimeInterval = 10
import NVActivityIndicatorView

@objc protocol PunchCardHomeDelegate {
    @objc optional func beaconFound()
}
class PunchCardHomeVC: UIViewController {

    // MARK: - Properties
    //let locationManager = CLLocationManager()
    var beaconManager: KTKBeaconManager!
    var proximityManager: KTKProximityManager!
    var beaconRegion:CLBeaconRegion!
    var beaconRegions:[BeaconModel] = []
    let pulsator = Pulsator()
    let usrDefaults = UserDefaults.standard
    var activityIndicator : NVActivityIndicatorView!
    var punchCardHomeDelegate:PunchCardHomeDelegate!
    //MARK: - Outlets
    var isGettingMerchant = false
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnScanCode: UIButton!
    
    
       override func viewDidLoad() {
        super.viewDidLoad()
        btnScanCode.roundButton()
        
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addPulse()
        self.activityIndicator = activityIndicator(view: view)
        beaconManager = KTKBeaconManager(delegate: self)
        proximityManager = KTKProximityManager.init(delegate: self)
        if self.validateLocationServices(){
            //locationManager.delegate = self
            getListBeaconRegions()
        }
         //getMerchant(beaconId: "f7826da6-4fa2-4e98-8024-bc5b71e0893e", beaconMajor: "54390", beaconMinor: "23622")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            // Your code with delay
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadBeaconMonitoring), name: NSNotification.Name(rawValue: AppDelegate.notificationApplicationDidBecomeActive), object: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppDelegate.notificationApplicationDidBecomeActive), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgLogo.layer.layoutIfNeeded()
        pulsator.position = imgLogo.layer.position
    }
    
    func reloadBeaconMonitoring(){
        if beaconRegions.count == 0{
           getListBeaconRegions()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func openCameraAction(_ sender: Any) {
        let cameraVC  = self.storyboard?.instantiateViewController(withIdentifier: "CameraAccessVC") as! CameraAccessVC
        self.present(cameraVC, animated: true, completion: nil)
    }
    
    
    func addPulse(){
        pulsator.numPulse = 5
        pulsator.radius = 90.0
        pulsator.animationDuration = Double(0.5) * kMaxDuration
        pulsator.backgroundColor = UIColor.uiCornflowerBlue.cgColor
        imgLogo.layer.superlayer?.insertSublayer(pulsator, below: imgLogo.layer)
        pulsator.start()
    }
    
    
    func setupBeacon(){
        for beacon in beaconRegions{
            if let uuid = UUID(uuidString:beacon.uid!) {
                /*let beaconReg = CLBeaconRegion(
                    proximityUUID: uuid,
                    identifier:beacon.uid!)*/
                //beaconRegion = beaconReg <- Ya estaba comentado
                //locationManager.startMonitoring(for: beaconReg)
                //locationManager.startRangingBeacons(in: beaconReg)
                let region = KTKBeaconRegion(proximityUUID: uuid, identifier: beacon.uid!)
                beaconManager.startMonitoring(for: region)
                beaconManager.startRangingBeacons(in: region)
                //proximityManager.monitorBeaconRegion(region)
            }

        }
        
    }
    
    func  getMerchant(beaconId:String,beaconMajor:String,beaconMinor:String){
        isGettingMerchant = true
        self.activityIndicator.startAnimating()
        MerchantManager.merchantByBeacon(beaconId: beaconId, beaconMajor: beaconMajor){ merchant,error in
            self.activityIndicator.stopAnimating()
            self.isGettingMerchant = false
            if error == nil{
                if merchant != nil{
                    let currentBeacon:BeaconModel = BeaconModel()
                    currentBeacon.uid = beaconId
                    currentBeacon.major = beaconMajor
                    currentBeacon.minor = beaconMinor
                    self.usrDefaults.set(MerchantModel.toDictionary(merchant: merchant!), forKey: "merchantBrand")
                    self.usrDefaults.set(BeaconModel.toDictionary(beacon: currentBeacon), forKey: "currentBeacon")
                    self.usrDefaults.synchronize()
                    self.punchCardHomeDelegate.beaconFound!()
                }
            }
            else{
                self.pulsator.start()
                //self.locationManager.startMonitoring(for: self.beaconRegion)
                //self.locationManager.startRangingBeacons(in: self.beaconRegion)
                
            }
        }
    }
    func getListBeaconRegions() {
        self.activityIndicator.startAnimating()
        PunchsCardsManager.getAllBeacons(){ beaconRegions,error in
            self.activityIndicator.stopAnimating()
            if error == nil{
                if !beaconRegions!.isEmpty{
                    self.beaconRegions = beaconRegions!
                    self.setupBeacon()
                }
            }
            
        }
    }
    
    func beaconInRange(beacon : CLBeacon){
        if beacon.accuracy < 7{
            //locationManager.stopMonitoring(for: region)
            //locationManager.stopRangingBeacons(in: region)
            beaconManager.stopMonitoringForAllRegions()
            beaconManager.stopRangingBeaconsInAllRegions()
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            pulsator.stop()
            if !isGettingMerchant {
                getMerchant(beaconId: beacon.proximityUUID.uuidString.lowercased(), beaconMajor: beacon.major.stringValue, beaconMinor: beacon.minor.stringValue)
            }
        }
    }

}
extension PunchCardHomeVC:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if let beaconRegion = region as? CLBeaconRegion {
            print("DID ENTER REGION: uuid: \(beaconRegion.proximityUUID.uuidString)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if let beaconRegion = region as? CLBeaconRegion {
            print("DID EXIT REGION: uuid: \(beaconRegion.proximityUUID.uuidString)")
        }
    }
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        for beacon in beacons {
            print("proximidad del beacon: \(beacon.proximity)")

            var beaconProximity: String;
            switch (beacon.proximity) {
            case CLProximity.unknown:    beaconProximity = "Unknown";
            case CLProximity.far:        beaconProximity = "Far";
            case CLProximity.near:
                beaconProximity = "Near"
                if beacon.accuracy < 1.2{
                    //locationManager.stopMonitoring(for: region)
                    //locationManager.stopRangingBeacons(in: region)
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    pulsator.stop()
                    getMerchant(beaconId: beacon.proximityUUID.uuidString.lowercased(), beaconMajor: beacon.major.stringValue, beaconMinor: beacon.minor.stringValue)
                }
            
                break
            case CLProximity.immediate:
                beaconProximity = "Immediate";
            break
            }
            
            print("BEACON RANGED: uuid: \(beacon.proximityUUID.uuidString) major: \(beacon.major)  minor: \(beacon.minor) proximity: \(beaconProximity)")
        }
    }
}

extension PunchCardHomeVC: KTKBeaconManagerDelegate{
    
    func beaconManager(_ manager: KTKBeaconManager, didStartMonitoringFor region: KTKBeaconRegion) {
        // Do something when monitoring for a particular
        // region is successfully initiated
    }
    
    func beaconManager(_ manager: KTKBeaconManager, monitoringDidFailFor region: KTKBeaconRegion?, withError error: Error?) {
        // Handle monitoring failing to start for your region
    }
    
    func beaconManager(_ manager: KTKBeaconManager, didEnter region: KTKBeaconRegion) {
        // Decide what to do when a user enters a range of your region; usually used
        // for triggering a local notification and/or starting a beacon ranging
    }
    
    func beaconManager(_ manager: KTKBeaconManager, didExitRegion region: KTKBeaconRegion) {
        // Decide what to do when a user exits a range of your region; usually used
        // for triggering a local notification and stoping a beacon ranging
    }
    
    func beaconManager(_ manager: KTKBeaconManager, didRangeBeacons beacons: [CLBeacon], in region: KTKBeaconRegion) {
        for beacon in beacons {
            print("proximidad del beacon: \(beacon.accuracy)")
            
            var beaconProximity: String;
            switch (beacon.proximity) {
            case CLProximity.unknown:    beaconProximity = "Unknown";
            case CLProximity.far:        beaconProximity = "Far";
                beaconProximity = "Near"
                beaconInRange(beacon: beacon)
            case CLProximity.near:
                beaconProximity = "Near"
                beaconInRange(beacon: beacon)
            case CLProximity.immediate:
                beaconProximity = "Immediate";
                beaconInRange(beacon: beacon)
                break
            }
            
            print("BEACON RANGED: uuid: \(beacon.proximityUUID.uuidString) major: \(beacon.major)  minor: \(beacon.minor) proximity: \(beaconProximity) accuracy: \(beacon.accuracy)")
        }
    }
}

extension PunchCardHomeVC: KTKProximityManagerDelegate {
    
    func proximityManager(_ manager: KTKProximityManager, didEnter region: KTKBeaconRegion, with beacons: [CLBeacon]) {
        // Entered region with iBeacons
    }
    
    func proximityManager(_ manager: KTKProximityManager, didExitBeaconRegion region: KTKBeaconRegion) {
        // Exit region
    }
    
    func proximityManager(_ manager: KTKProximityManager, didFailMonitoringFor region: KTKBeaconRegion?, withError error: Error) {
        // Monitoring did fail
    }
}
