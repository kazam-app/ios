//
//  InteracionOnBoardingMainVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 8/8/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

class InteracionOnBoardingMainVC: UIViewController {
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var btnNext: UIButton!

    @IBOutlet weak var pageCotrol: UIPageControl!
    
    
    var interactionOnBoardingVPVC: InteractionOnBoardingVPVC? {
        didSet {
            interactionOnBoardingVPVC?.tutorialDelegate = self
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tutorialPageViewController = segue.destination as? InteractionOnBoardingVPVC {
            self.interactionOnBoardingVPVC = tutorialPageViewController
        }
    }
    /**
     Fired when the user taps on the pageControl to change its current page.
     */
    func didChangePageControlValue() {
        interactionOnBoardingVPVC?.scrollToViewController(index: pageCotrol.currentPage)
    }

    
    @IBAction func closeAction(_ sender: Any) {
        let usrDefaults = UserDefaults.standard
        usrDefaults.set(true, forKey: "onboardingInteraction")
        usrDefaults.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if pageCotrol.currentPage == 3{
            let usrDefaults = UserDefaults.standard
            usrDefaults.set(true, forKey: "onboardingInteraction")
            usrDefaults.synchronize()
            self.dismiss(animated: true, completion: nil)
        }
        else{
            interactionOnBoardingVPVC?.scrollToNextViewController()
        }
    }
}
extension InteracionOnBoardingMainVC:TutorialPageViewControllerDelegate {
    
    func tutorialPageViewController(_ tutorialPageViewController: InteractionOnBoardingVPVC,
                                    didUpdatePageCount count: Int) {
        pageCotrol.numberOfPages = count
    }
    
    func tutorialPageViewController(_ tutorialPageViewController: InteractionOnBoardingVPVC,
                                    didUpdatePageIndex index: Int) {
        pageCotrol.currentPage = index
        if (index == 3){
            btnNext.setTitle("Terminar", for:.normal)
        }
        else{
             btnNext.setTitle("Siguiente", for:.normal)
        }
    }
}
