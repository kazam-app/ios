//
//  ChangeNotificationsVC.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/11/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Eureka
class ChangeNotificationsVC: FormViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        createUserForm()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func createUserForm(){
        form +++ Section(header: "", footer: "Activa las notificaciones que quieras recibir, y desactiva las que no.")
        form +++ Section(header: "NOTIFICACIONES GENERALES", footer: "Mantén estas notificaciones activadas para tener una mejor experiencia.")
            <<< SwitchRow("notificationGeneral") { row in      // initializer
                row.title = "Avisos Generales (Importantes)"
                row.value = true
                }.onChange { row in
                    //row.title = (row.value ?? false) ? "The title expands when on" : "The title"
                    row.updateCell()
                }.cellSetup { cell, row in
                    
                    cell.switchControl?.onTintColor = UIColor.uiCornflowerBlue
                }
            <<< SwitchRow("notificationPromotions") { row in      // initializer
                row.title = "Promociones Cercanas"
                row.value = true
                }.onChange { row in
                    //row.title = (row.value ?? false) ? "The title expands when on" : "The title"
                    row.updateCell()
                }.cellSetup { cell, row in
                    
                    cell.switchControl?.onTintColor = UIColor.uiCornflowerBlue
                }
            
            
            /*+++ Section(header: "NOTICIAS", footer: "")
            <<< SwitchRow("notificationMorning") { row in      // initializer
                row.title = "Encabezados del Día (Mañana)"
                row.value = true
                }.onChange { row in
                    //row.title = (row.value ?? false) ? "The title expands when on" : "The title"
                    row.updateCell()
                }.cellSetup { cell, row in
                    
                    cell.switchControl?.onTintColor = UIColor.uiCornflowerBlue
            }
            <<< SwitchRow("notificationAfternon") { row in      // initializer
                row.title = "Resumen del Día (Noche)"
                row.value = true
                }.onChange { row in
                    //row.title = (row.value ?? false) ? "The title expands when on" : "The title"
                    row.updateCell()
                }.cellSetup { cell, row in
                    
                    cell.switchControl?.onTintColor = UIColor.uiCornflowerBlue
        }*/
    }
}
