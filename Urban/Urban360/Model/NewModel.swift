//
//  NewModel.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import SwiftyJSON
class NewModel{
    var newId:String?
    var title:String?
    var date:String?
    var urlImage:String?
    var category:String?
    var html:String?
    var categoryColor:String?
    
    
    
    
    init() {
    }
    class  func fromJSON(json: JSON) ->NewModel{
        let news = NewModel()
        news.newId = json["id"].string 
        news.title = json["title"].string
        news.category = json["category"].string
        let dateLong = json["date"].string
        let dateFormat = dateLong?.convertDate(dateLong!)
        news.date = dateFormat!.timeAgo(date: dateFormat!)
        news.urlImage = (json["image"].string?.isValidUrl())! ? json["image"].string:""
        news.html = json["content"].string
        news.categoryColor = json["category_color"].string
        return news
    }
   /* class func toJSON(category:NewModel)-> [String:JSON]{
        var json:[String:JSON] = [:]
        //json["id"]!.int32 = category.categoryId
        //json["name"]!.string = category.categoryName
        return json
    }*/
}
