//
//  PunchCardListModel.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/23/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import SwiftyJSON
class PunchCardListModel{
    
    var punchCardId:String?
    var identity:Int?
    var prize:String?
    var punchCount:Int?
    var punchLimit:Int?
    var merchantId:String?
    var merchantName:String?
    var urlImage:String?
    
    init() {
    }
    class  func fromJSON(json: JSON) ->PunchCardListModel{
        let punchCard = PunchCardListModel()
        punchCard.punchCardId = json["id"].string
        punchCard.identity = json["identity"].int
        punchCard.prize = json["prize"].string
        punchCard.punchCount = json["punch_count"].int
        punchCard.punchLimit = json["punch_limit"].int
        punchCard.merchantId = json["merchant_id"].string
        punchCard.merchantName = json["merchant_name"].string
        punchCard.urlImage = json["merchant_image_url"].string
        return punchCard
    }
    /* class func toJSON(category:NewModel)-> [String:JSON]{
     var json:[String:JSON] = [:]
     //json["id"]!.int32 = category.categoryId
     //json["name"]!.string = category.categoryName
     return json
     }*/
}
