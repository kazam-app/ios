//
//  MerchantModel.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/20/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import SwiftyJSON
class MerchantModel{
    
    var merchantId:String?
    var name:String?
    var urlImage:String?
    var category:String?
    var budgetRating:Int?
    var color:String?

    init() {
    }
    class  func fromJSON(json: JSON) ->MerchantModel{
        let merchant = MerchantModel()
        merchant.merchantId = json["id"].string
        merchant.name = json["name"].string
        merchant.category = json["category"].string
        merchant.urlImage = json["image_url"].string
        merchant.color = json["color"].string
        merchant.budgetRating = json["budget_rating"].int
        return merchant
    }
    /* class func toJSON(category:NewModel)-> [String:JSON]{
     var json:[String:JSON] = [:]
     //json["id"]!.int32 = category.categoryId
     //json["name"]!.string = category.categoryName
     return json
     }*/
    
    class func toDictionary(merchant:MerchantModel)->[String:AnyObject]{
        var merchantDic:[String:AnyObject] = [:]
        merchantDic["id"] = merchant.merchantId! as AnyObject
        merchantDic["name"] = merchant.name! as AnyObject
        merchantDic["category"] = merchant.category! as AnyObject
        merchantDic["image_url"] = merchant.urlImage! as AnyObject
        merchantDic["budget_rating"] = merchant.budgetRating! as AnyObject
        
        if let color = merchant.color{
            merchantDic["color"] = color as AnyObject
        }
    return merchantDic
    }
    
    class func toMerchantModel(merchantDic:[String:AnyObject])-> MerchantModel{
        let merchant = MerchantModel()
        merchant.merchantId = merchantDic["id"] as? String
        merchant.name = merchantDic["name"] as? String
        merchant.category = merchantDic["category"] as? String
        merchant.urlImage = merchantDic["image_url"] as? String
        
        merchant.budgetRating = merchantDic["budget_rating"] as? Int
        
        
        if let color = merchantDic["color"]{
            merchant.color  = color as? String
        }
        return merchant
    }

}
