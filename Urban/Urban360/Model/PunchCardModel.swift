//
//  PunchCardModel.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 7/24/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import SwiftyJSON
class PunchCardModel{
    
    var punchCardId:String?
    var identity:Int?
    var prize:String?
    var punchCount:Int?
    var punchLimit:Int?
    var merchantId:String?
    var merchantName:String?
    var urlImage:String?
    var expiresAt:String?
    var validationDate:Date?
    var terms:String?
    var rules:String?
    var reedemCode:String?
    init() {
    }
    class  func fromJSON(json: JSON) ->PunchCardModel{
        let punchCard = PunchCardModel()
        punchCard.punchCardId = json["id"].string
        punchCard.identity = json["identity"].int
        punchCard.prize = json["prize"].string
        punchCard.punchCount = json["punch_count"].int
        punchCard.punchLimit = json["punch_limit"].int
        punchCard.merchantId = json["merchant_id"].string
        punchCard.merchantName = json["merchant_name"].string
        punchCard.urlImage = json["merchant_image_url"].string
        punchCard.terms = json["terms"].string
        punchCard.rules = json["rules"].string
        if let expires = json["expires_at"].string{
            let date:Date = Date().dateFromISOString(string: expires)
            punchCard.expiresAt = date.stDateString(format: .date_only_signup)
        }
        
        if let validation = json["validation_date"].string{
            let date:Date = Date().dateFromISOString(string: validation)
            punchCard.validationDate = date
        }
        
        punchCard.reedemCode = json["redeem_code"].string
        return punchCard
    }
    /* class func toJSON(category:NewModel)-> [String:JSON]{
     var json:[String:JSON] = [:]
     //json["id"]!.int32 = category.categoryId
     //json["name"]!.string = category.categoryName
     return json
     }*/
}
