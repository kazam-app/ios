//
//  BeaconModel.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 8/3/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import SwiftyJSON
class BeaconModel{
    
    var uid:String?
    var major:String?
    var minor:String?
    
    init() {
    }
    class  func fromJSON(json: JSON) ->BeaconModel{
        let beacon = BeaconModel()
        beacon.uid = json["uid"].string
        beacon.major = json["major"].string
        beacon.minor = json["minor"].string
        return beacon
    }
    /* class func toJSON(category:NewModel)-> [String:JSON]{
     var json:[String:JSON] = [:]
     //json["id"]!.int32 = category.categoryId
     //json["name"]!.string = category.categoryName
     return json
     }*/
    
    class func toDictionary(beacon:BeaconModel)->[String:AnyObject]{
        var beaconDic:[String:AnyObject] = [:]
        beaconDic["uid"] = beacon.uid! as AnyObject
        beaconDic["major"] = beacon.major! as AnyObject
        beaconDic["minor"] = beacon.minor! as AnyObject
        return beaconDic
    }
    
    class func toBeaconModel(beaconDic:[String:AnyObject])-> BeaconModel{
        let beacon = BeaconModel()
        beacon.uid = beaconDic["uid"] as? String
        beacon.major = beaconDic["major"] as? String
        beacon.minor = beaconDic["minor"] as? String
        return beacon
    }
}
