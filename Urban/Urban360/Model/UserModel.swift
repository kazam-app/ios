//
//  UserModel.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/13/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import SwiftyJSON
class UserModel{
    var userId:String?
    var name:String?
    var lastNames:String?
    var email:String?
    var birthDate:String?
    var gender:String?
    var fbId:String?
    var fbToken:String?
    var fbExpiresAt:String?
    var inviteCode:String?
    var pwd:String?
    var confirmPwd:String?
    var sessiontoken:String?
    var createdAt: String?
    var merchant:MerchantModel?
    
    init() {
    }
    class  func fromJSON(json: JSON) ->UserModel{
        let userSession = UserModel()
        userSession.userId  = json["id"].string
        userSession.name = json["name"].string
        userSession.lastNames = json["last_names"].string
        userSession.createdAt = json["created_at"].string ?? ""
        userSession.email = json["email"].string
        if let gender = json["gender"].int{
             userSession.gender = "\(gender as Int) "
        }
        if let birthDate = json["birthdate"].string{
            let date:Date = Date().dateFromString(dateString: birthDate, format: .date_only)
            let currentBirthDate:String = date.stDateString(format: .date_only_signup)
            userSession.birthDate = currentBirthDate
        }
        if json["merchant"].dictionary != nil{
            userSession.merchant = MerchantModel.fromJSON(json: json["merchant"])
        }
        userSession.sessiontoken = json["token"].string
        return userSession
    }
    
    
     class func toJSON(user:UserModel)-> [String:AnyObject]{
     var json:[String:AnyObject] = [:]
     
     json["name"] = user.name! as AnyObject
     json["last_names"] = user.lastNames! as AnyObject
     json["email"] = user.email! as AnyObject
        if let birthDate = user.birthDate{
            let date = Date().dateFromString(dateString: birthDate, format: .date_only_signup)
            let dateSt:String = Date().ISOStringFromDate(date: date)
            json["birthdate"] = dateSt as AnyObject
        }
        if let gender = user.gender{
            
            if gender == "Hombre" {
                json["gender"] =  1  as AnyObject
            }
            else if gender == "Mujer" {
                json["gender"] =  0  as AnyObject
            }
        }
        if let pwd = user.pwd{
            json["password"] = pwd as AnyObject
        }
        if let confirmPwd = user.confirmPwd{
            json["password_confirmation"] = confirmPwd as AnyObject
        }
        if let fbId = user.fbId{
            json["fb_id"] = fbId as AnyObject
        }
        if let fbToken = user.fbToken{
            json["fb_token"] = fbToken as AnyObject
        }
        if let fbExpiresAt = user.fbExpiresAt{
            let date = Date().dateFromString(dateString: fbExpiresAt, format: .date_only_signup)
            let dateSt:String = Date().ISOStringFromDate(date: date)

            json["fb_expires_at"] = dateSt as AnyObject
        }
        if let inviteCode = user.inviteCode {
            if !inviteCode.isEmpty{
                json["invite_code"] = inviteCode as AnyObject

            }
        }
        json["created_at"] = user.createdAt as AnyObject
        let jsonC:[String:AnyObject] = ["user":json as AnyObject]
     return jsonC
     }
    
    class func loginToJSON(user:UserModel)-> [String:AnyObject]{
        var json:[String:AnyObject] = [:]
        if let email = user.email{
            json["email"] = email as AnyObject
        }
        if let pwd = user.pwd{
            json["password"] = pwd as AnyObject
        }
        if let fbId = user.fbId{
            json["fb_id"] = fbId as AnyObject
        }
        if let fbToken = user.fbToken{
            json["fb_token"] = fbToken as AnyObject
        }
        if let fbExpiresAt = user.fbExpiresAt{
            json["fb_expires_at"] = fbExpiresAt as AnyObject
        }
        return json
    }
    
    class func toDictionary(user:UserModel)->[String:AnyObject]{
        var userDic:[String:AnyObject] = [:]
        userDic["name"] = user.name! as AnyObject
        userDic["last_names"] = user.lastNames! as AnyObject
        userDic["created_at"] = user.createdAt as AnyObject
        userDic["email"] = user.email! as AnyObject
        if let birthDate = user.birthDate{
            userDic["birthDate"] = birthDate as AnyObject
        }
        if let gender = user.gender{
            if gender.trim() == "0"{
                 userDic["gender"] = "0" as AnyObject
            }
            else if gender.trim() == "1"{
                userDic["gender"] = "1" as AnyObject

            }
        }
        if let sessiontoken = user.sessiontoken{
            userDic["sessiontoken"] = sessiontoken as AnyObject
        }
        if let fbId = user.fbId{
            userDic["fb_id"] = fbId as AnyObject
        }
        if let fbToken = user.fbToken{
            userDic["fb_token"] = fbToken as AnyObject
        }
        if let fbExpiresAt = user.fbExpiresAt{
            userDic["fb_expires_at"] = fbExpiresAt as AnyObject
        }
        return userDic
    }
    class func toUserModel(userDic:[String:AnyObject])-> UserModel{
        let userSession = UserModel()
        userSession.name = userDic["name"] as? String
        userSession.createdAt = userDic["created_at"] as? String
         userSession.lastNames = userDic["last_names"] as? String
        userSession.email = userDic["email"] as? String
        if let birthDate = userDic["birthDate"]{
           userSession.birthDate  = birthDate as? String
        }
        if let gender = userDic["gender"]{
            userSession.gender = gender as? String
        }
        if let sessiontoken = userDic["sessiontoken"] {
           userSession.sessiontoken = sessiontoken as? String
        }
        if let fbId =  userDic["fb_id"] {
          userSession.fbId = fbId as? String
        }
        if let fbToken = userDic["fb_token"]{
           userSession.fbToken  = fbToken as? String
        }
        if let fbExpiresAt = userDic["fb_expires_at"]{
            userSession.fbExpiresAt = fbExpiresAt as? String
        }
        return userSession
    }
}
