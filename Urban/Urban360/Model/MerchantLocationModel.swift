//
//  MerchantLocationModel.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/21/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import SwiftyJSON
class MerchantLocationModel{
    
    var id:String?
    var merchantId:String?
    var merchantName:String?
    var name:String?
    var shopCluster:String?
    var category:String?
    var merchantDistance:String?
    var merchantBudgetRaiting:Int?
    var urlImage:String?
    var merchantColor:String?

    init() {
    }
    class  func fromJSON(json: JSON) ->MerchantLocationModel{
        let merchant = MerchantLocationModel()
        merchant.id = json["id"].string
        merchant.merchantId = json["merchant_id"].string
        merchant.merchantName = json["merchant_name"].string
        merchant.name = json["name"].string
        merchant.category = json["category"].string
        merchant.merchantDistance = json["distance"].string
        merchant.shopCluster = json["shop_cluster"].string
        merchant.urlImage = json["merchant_image_url"].string
        merchant.merchantBudgetRaiting = json["merchant_budget_rating"].int
        merchant.merchantColor = json["merchant_color"].string
        return merchant
    }
    /* class func toJSON(category:NewModel)-> [String:JSON]{
     var json:[String:JSON] = [:]
     //json["id"]!.int32 = category.categoryId
     //json["name"]!.string = category.categoryName
     return json
     }*/
}
