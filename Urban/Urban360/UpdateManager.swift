//
//  UpdateManager.swift
//  Urban360
//
//  Created by Hector Maya on 13/10/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation

class UpdateManager: NSObject {
    
    static let KEY_VERSION = "version"
    
    static func isChangeVersión() -> Bool {
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        if UserDefaults.standard.object(forKey: KEY_VERSION) != nil{
            let lastSaverVersion = UserDefaults.standard.object(forKey: KEY_VERSION) as? String
            if lastSaverVersion == version{
                return false
            }else{
                UserDefaults.standard.set(version, forKey: KEY_VERSION)
                return true
            }
        }else{
            UserDefaults.standard.set(version, forKey: KEY_VERSION)
            return true
        }
    }
    
}
