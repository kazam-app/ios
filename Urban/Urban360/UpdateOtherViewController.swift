//
//  UpdateOtherViewController.swift
//  Urban360
//
//  Created by Hector Maya on 13/10/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

class UpdateOtherViewController: UIViewController {
    
    @IBOutlet weak var btnUpdate: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnUpdate.layer.shadowOffset = CGSize(width: 0, height: 2)
        btnUpdate.layer.shadowOpacity = 0.3
        btnUpdate.layer.shadowRadius = 4
        btnUpdate.layer.cornerRadius = 7.0
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func update(){
        
        guard let url = URL(string: String.init(format: "itms-apps://itunes.apple.com/us/app/apple-store/%@?mt=8", Bundle.main.bundleIdentifier!)) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
