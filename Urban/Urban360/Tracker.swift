//
//  FacebookTracker.swift
//  Urban360
//
//  Created by Hector Maya on 25/10/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import FBSDKCoreKit

class Tracker {
    
    
     static let EVENT_APP_OPEN = "app_open"
     static let EVENT_APP_CLOSE = "app_close"
     static let EVENT_SIGNUP_FB_START = "signup_fb_start"
     static let EVENT_SIGNUP_FB_EXTRA_INFO = "signup_fb_extra_info"
     static let EVENT_SIGNUP_FB_LOCATION = "signup_fb_location"
     static let EVENT_SIGNUP_FB_LOCATION_PERMISSION =  "signup_fb_location_permission"
     static let EVENT_SIGNUP_FB_NOTIFICATION =  "signup_fb_notification"
     static let EVENT_SIGNUP_FB_NOTIFICATION_PERMISSION = "signup_fb_notification_permission"
     static let EVENT_SIGNUP_FB_INVITE_CODE = "signup_fb_invite_code"
     static let EVENT_SIGNUP_FB_SUCCESS = "signup_fb_success"
     static let EVENT_SIGNUP_EMAIL_START =  "signup_email_start"
     static let EVENT_SIGNUP_EMAIL_CREDENTIALS = "signup_email_credentials"
     static let EVENT_SIGNUP_EMAIL_EXTRA_INFO = "signup_email_extra_info"
     static let EVENT_SIGNUP_EMAIL_LOCATION = "signup_email_location"
     static let EVENT_SIGNUP_EMAIL_LOCATION_PERMISSION = "signup_email_location_permission"
     static let EVENT_SIGNUP_EMAIL_NOTIFICATION = "signup_email_notification"
     static let EVENT_SIGNUP_EMAIL_NOTIFICATION_PERMISSION = "signup_email_notification_permission"
     static let EVENT_SIGNUP_EMAIL_INVITE_CODE = "signup_email_invite_code"
     static let EVENT_SIGNUP_EMAIL_SUCCESS = "signup_email_success"
     static let EVENT_LOGIN_FB = "login_fb"
     static let EVENT_LOGIN_EMAIL = "login_email"
     static let EVENT_LOGOUT = "logout"
     static let EVENT_PUNCH_LIMIT_REACHED = "punch_limit_reached"
     static let EVENT_MERCHANT_DETECTED = "merchant_detected"
     static let EVENT_PUNCHCARD_TRY_PUNCH = "punchcard_try_punch"
     static let EVENT_PUNCHCARD_PUNCHED = "punchcard_punched"
     static let EVENT_PUNCHCARD_TRY_REEDEM = "punchcard_try_redeem"
     static let EVENT_PUNCHCARD_REDEEMED = "punchcard_redeemed"
     static let EVENT_MO_WELCOME = "mo_welcome"
     static let EVENT_MO_ENTER_STORE = "mo_enter_store"
     static let EVENT_MO_PUNCH_CARD = "mo_punch_card"
     static let EVENT_MO_GET_PRIZES = "mo_get_prizes"
     static let EVENT_IO_HOW_IT_WORKS = "io_how_it_works"
     static let EVENT_IO_STAMP_CARDS = "io_stamp_cards"
     static let EVENT_IO_VALIDATE = "io_validate"
     static let EVENT_IO_GET_PRIZES = "io_get_prize"
 
    
    static func trackEvent(event: String, parameters: [String : String]?){
        print("Event : \(event) --- Parameters :  \(parameters ?? ["No Keys":"No Parameter"])")
        /*if parameters != nil {
            FBSDKAppEvents.logEvent(event, parameters: parameters )
        }else{
            FBSDKAppEvents.logEvent(event)
        }*/
    }
    
}
