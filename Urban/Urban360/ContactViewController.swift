//
//  ContactViewController.swift
//  Urban360
//
//  Created by Hector Maya on 12/10/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: UIViewController {
    
    @IBOutlet weak var btnFacebookMessenger: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnFacebookMessenger.layer.borderWidth = 1
        btnFacebookMessenger.layer.cornerRadius = 7.0
        btnFacebookMessenger.layer.borderColor = UIColor.init(red: 1.0/255.0, green: 132.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
    }
    
    @IBAction func emailAction(sender: Any?){
        //ayuda@kzm.mx
        if !MFMailComposeViewController.canSendMail() {
            self.displayErrorAlert(title: "generalErrorMessageKey".localized,message:"No tienes configurada una cuenta de correo")
            return
        }
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["ayuda@kzm.mx"])
        composeVC.setSubject("Contacto")
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    @IBAction func facebookMessengerAction(sender: Any?){
        guard let url = URL(string: "https://m.me//kzmapp") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func close(sender: Any?){
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension ContactViewController : MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {}
    }
}
