//
//  BaseViewController.swift
//  Urban360
//
//  Created by Hector Maya on 29/08/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var reachabilityInternet: ReachabilityUtil!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        suscribeToReachability()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unsuscribeToReachability()
    }
    
    func suscribeToReachability(){
            //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        reachabilityInternet = ReachabilityUtil.reachabilityForInternetConnection()
        reachabilityInternet?.startNotifier()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(notification:)), name: NSNotification.Name(rawValue: "kNetworkReachabilityChangedNotification"), object: nil)
    }
    
    func unsuscribeToReachability(){
        reachabilityInternet.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "kNetworkReachabilityChangedNotification"), object: nil)
    }
    
    func reachabilityChanged(notification: Notification){
        let curReach = notification.object as! ReachabilityUtil
        updateInterface(reachability: curReach)
    }
    
    func updateInterface(reachability: ReachabilityUtil) {
        switch reachability.currentReachabilityStatus() {
        case NotReachable:
            notInternetConnection()
            break
        default:
            connectedToInternet()
            break
        }
    }
    
    func notInternetConnection() {
        
    }
    
    func connectedToInternet(){
    
    }
    
}
