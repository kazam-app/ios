//
//  ReedemManager.swift
//  Urban360
//
//  Created by Hector Maya on 18/09/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation

class ReedemManager: NSObject {
    
    static let KEY_REEDEM_PUNCH = "punch_reedem"
    
    static let KEY_REEDEM_CODE = "code"
    static let KEY_REEDEM_TITLE = "title"
    static let KEY_REEDEM_END_DATE_PROMOTION = "end_date_promotion"
    static let KEY_REEDEM_RULES_PROMOTION = "end_rules_promotion"
    static let KEY_REEDEM_CONDITIONS_PROMOTION = "end_conditions_promotion"
    static let KEY_REEDEM_MERCHANT_ID = "merchant_id"
    static let KEY_REEDEM_TIME = "time"
    
    //5 Mins
    static let MAX_SECONDS_REEDEM = Double(300)
    
    static func getReedemList() -> Any?{
        checkReedemList()
        return UserDefaults.standard.object(forKey: KEY_REEDEM_PUNCH)
    }
    
    static func addReedemItem(dic: Any?){
        var arr :[Any]!
        if getReedemList() == nil{
            arr = [dic!]
        }else{
            arr = getReedemList() as! [Any]
            arr.append(dic!)
        }
        saveReedemList(list: arr)
    }
    
    static func saveReedemList(list: Any?){
        UserDefaults.standard.set(list, forKey: KEY_REEDEM_PUNCH)
    }
    
    static func checkReedemList(){
        if UserDefaults.standard.object(forKey: KEY_REEDEM_PUNCH) != nil{
            let reedemList = UserDefaults.standard.object(forKey: KEY_REEDEM_PUNCH)as! [[String : Any?]]
            var newReedemList = [[String : Any?]]()
            for reedemItem in reedemList{
                let time = reedemItem[KEY_REEDEM_TIME] as! Double
                if isValidTimeReedem(time: time){
                    newReedemList.append(reedemItem)
                }
            }
            
            if newReedemList.count > 0{
                saveReedemList(list: newReedemList)
            }else{
                UserDefaults.standard.removeObject(forKey: KEY_REEDEM_PUNCH)
            }
        }
    }
    
    static func isValidTimeReedem(time: Double) -> Bool{
        let now = NSDate().timeIntervalSince1970
        return (now - time) < MAX_SECONDS_REEDEM
    }
    
}
