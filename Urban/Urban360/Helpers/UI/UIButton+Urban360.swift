//
//  UIButton+Urban360.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/21/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

extension UIButton {
    
    //Color Oficial :
    //Hexadecimal: #FF7E82
    // RGB: red:255.0/255.0, green:126.0/255.0, blue:130.0/255.0, alpha: 1.0
    
    // MARK: button default styles
    
    
    
    // MARK: - common button styles
    

    
    func borderImage(){
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.white.cgColor
    }
    func stButtonBorder(){
        layer.cornerRadius = 5.0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.uiCornflowerBlue.cgColor
        setTitleColor(UIColor.uiCornflowerBlue, for: .normal)
    }
    func roundButton() {
        layer.cornerRadius = 5.0
    }
}
