//
//  UIViewController+Urbarn360.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation
import KontaktSDK

extension UIViewController:UIPopoverPresentationControllerDelegate,APIReloadData {
    
    // MARK: - activity indicator
    func activityIndicator(view:UIView)->NVActivityIndicatorView{
        let x = Int((view.bounds.width/2) - 20)
        let y = Int((view.bounds.height/2) - 20)
        let frame = CGRect(x: x, y: y, width: 40, height: 40)
        let activityIndicator = NVActivityIndicatorView(frame: frame, type: .ballRotateChase, color: UIColor.uiCornflowerBlue, padding: 2)
        view.addSubview(activityIndicator)
        return activityIndicator
    }

    // MARK: - standard alerts
    func displayErrorAlert(title: String, message: String) {
        displayOKAlert(title: title, message: message)
    }
    
    func displayOKAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayHandlAlert(title: String, message: String, completion: @escaping (UIAlertAction) -> Void ){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .default, handler: completion)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    func displayConfirmHandlAlert(title: String, message: String, completion: @escaping (UIAlertAction) -> Void ){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .default, handler: completion)
        let actionCancel =  UIAlertAction(title:"Cancelar",style:.cancel)
        alertController.addAction(action)
        alertController.addAction(actionCancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showErrorMessageGeneral(title:String,icon:String,descriptionMessage:String,titleKeyButton:String?,showButton:Bool) {
        let messageController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        messageController.titleMessage = title
        messageController.icon = icon
        messageController.descriptionMessage = descriptionMessage
        let message = messageController.view as UIView
        message.frame = CGRect(x: 0, y: 0, width: messageController.container.bounds.size.width, height:250)
        message.center = self.view.center
        message.tag = 10
        message.alpha = 0

        self.view.addSubview(message)
        if showButton{
            let rect = CGRect(x: 0, y: 0, width:  messageController.container.bounds.size.width, height: message.bounds.size.height)
            let midX = rect.midX //replacement of CGRectGetMidX
            let btnRetry = UIButton(frame: CGRect(x:(midX-104) , y: 210, width: 207, height: 36))
            btnRetry.addTarget(self, action: #selector(hideErrorMessge), for: .touchUpInside)
            if let titleKey:String = titleKeyButton{
                btnRetry.setTitle(titleKey.localized, for: .normal)
            }
            else{
                btnRetry.setTitle("btnRetryKey".localized, for: .normal)
            }
            
            btnRetry.setTitleColor( UIColor.uiCornflowerBlue, for: .normal)
            btnRetry.stButtonBorder()
             message.insertSubview(btnRetry, at: 1)
        }
        message.fadeIn(withDuration: 0.5)
    }
    
    public func hideErrorMessge(){
        UIView.animate(withDuration: 1, animations: {self.view.viewWithTag(10)?.alpha = 0.0},
                                   completion: {(value: Bool) in
                                    self.view.viewWithTag(10)?.removeFromSuperview()
                                    self.reloadData()
        })
    }
    public func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    //MARK: - delegate method
    func reloadData() {
        
    }
    func validateReachability()->Bool{
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            return true
        } else {
            return false
        }
    }
    
    func addButtonOnNaviationBarRight(title:String){
        let btn1 = UIButton(type: .custom)
        //btn1.setImage(UIImage(named: "imagename"), for: .normal)
        btn1.setTitle(title, for: .normal)
        btn1.setTitleColor(UIColor.uiCornflowerBlue, for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 90, height: 30)
        btn1.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
    }
    
    func addButtonOnNaviationBarLeft(title:String){
        let btn1 = UIButton(type: .custom)
        //btn1.setImage(UIImage(named: "imagename"), for: .normal)
        btn1.setTitle(title, for: .normal)
        btn1.setTitleColor(UIColor.uiCornflowerBlue, for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 90, height: 30)
        btn1.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)
    }
    
    //MARK: - delegate method
    func nextAction() {
        
    }
    
    func validateLocationServices() -> Bool {
        var isEnabled = false
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                isEnabled = true
            }
        } else {
            print("Location services are not enabled")
        }

        return isEnabled
    }
    
    /*func validateKontaktLocationServices() -> Bool {
        var isEnabled = false
        switch(KTKBeaconManager.locationAuthorizationStatus()) {
        case .notDetermined, .restricted, .denied:
            print("No access")
        case .authorizedAlways, .authorizedWhenInUse:
            print("Access")
            isEnabled = true
        }
        return isEnabled
    }*/
    
}

