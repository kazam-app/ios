//
//  UIColor+Urbarn360.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/1/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

extension UIColor {
    class var uiAquamarine: UIColor {
        return UIColor(red: 1.0 / 255.0, green: 207.0 / 255.0, blue: 191.0 / 255.0, alpha: 1.0)
    }
    
    class var uiTealish: UIColor {
        return UIColor(red: 34.0 / 255.0, green: 208.0 / 255.0, blue: 165.0 / 255.0, alpha: 1.0)
    }
    
    class var uiDullBlue: UIColor {
        return UIColor(red: 72.0 / 255.0, green: 108.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0)
    }
    
    class var uiCoralPink: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 89.0 / 255.0, blue: 100.0 / 255.0, alpha: 1.0)
    }
    
    class var uiWarmGrey20: UIColor {
        return UIColor(white: 146.0 / 255.0, alpha: 0.2)
    }
    
    class var uiCornflowerBlue: UIColor {
        return UIColor(red: 68.0 / 255.0, green: 129.0 / 255.0, blue: 209.0 / 255.0, alpha: 1.0)
    }
    
    class var uigray: UIColor {
        return UIColor(red: 189.0 / 255.0, green: 189.0 / 255.0, blue: 189.0 / 255.0, alpha: 1.0)
    }
    class var uiSilver: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 0.6)
    }
    
    class var uiSilverBG: UIColor {
        return UIColor(red: 234.0 / 255.0, green: 234.0 / 255.0, blue: 234.0 / 255.0, alpha: 1.0)
    }
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
