//
//  UIView+Urbarn360.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/31/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
public extension UIView {
    /**
     Fade in a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeIn(withDuration duration: TimeInterval ) {
        UIView.animate(withDuration:duration, animations: {
            self.alpha = 1.0
        })
    }
    
    /**
     Fade out a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeOut(withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
}
