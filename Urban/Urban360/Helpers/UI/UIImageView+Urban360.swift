//
//  UIImageView+Urban360.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import AlamofireImage
extension UIImageView{
    
    func addBlackGradientLayer(){
        let gradient = CAGradientLayer()
        let frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        gradient.frame = frame
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradient.locations = [0.0, 0.5]
        gradient.opacity = 0.6
        self.layer.addSublayer(gradient)
    }
    
    func addLayer(color: UIColor){
        
        let gradient = CALayer()
        let frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        gradient.frame = frame
        gradient.backgroundColor = color.cgColor
        gradient.opacity = 0.4
        self.layer.addSublayer(gradient)
    }
    func addStyleForLogo(){
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0.9
    }
    
    
}

