//
//  String+Urban360.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//
import Foundation
import UIKit
extension String {
    
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        return characters.count < 6
    }
    
    func isValidUrl() -> Bool {
        if let _ = URL(string: self) {
            let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
            let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
            
            return predicate.evaluate(with: self)
        }
        return false
    }
     func is24h() -> Bool {
        var is24h: Bool
        let formatter2 = DateFormatter()
        formatter2.dateStyle = .none
        formatter2.timeStyle = .short
        let dateString2: String = formatter2.string(from: Date())
        let amRange: NSRange = (dateString2 as NSString).range(of: formatter2.amSymbol)
        let pmRange: NSRange = (dateString2 as NSString).range(of: formatter2.pmSymbol)
        is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound)
        return is24h
    }

    func convertDate(_ dateConvert: String) -> Date {
        var dateString: String = "\(dateConvert)"
        dateString = ((dateString as? NSString)?.substring(to: (dateString.characters.count ) - 5))!
        let formatter = DateFormatter()
        var date2 = Date()
        if !self.is24h() && (formatter.amSymbol == "a.m.") || (formatter.amSymbol == "p.m.") {
            let timeString = dateString.components(separatedBy: "T")[1]
            let hourString = timeString.components(separatedBy: ":")[0]
            let datePost = dateString.components(separatedBy: "T")[0]
            let minutes = timeString.components(separatedBy: ":")[1]
            let hourValue: Int = Int(hourString)!
            if hourValue < 12 {
                //AM
                dateString = "\(datePost) \(hourString):\(minutes) \("a.m.")"
                formatter.dateFormat = "yyyy-MM-dd hh:mm a"
                date2 = formatter.date(from: dateString)!
            }
            else {
                //PM
                var finalHour: String
                if hourValue < 22 {
                    finalHour = "0\(Int(hourValue) - 12)"
                }
                else {
                    finalHour = "\(Int(hourValue) - 12)"
                }
                dateString = "\(datePost) \(finalHour):\(minutes)\(" p.m.")"
                formatter.dateFormat = "yyyy-MM-dd hh:mm a"
                date2 = formatter.date(from: dateString)!
            }
        }
        else {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'zzz'"
            //Change this to your date format
            date2 = formatter.date(from: dateString)!
        }
        return date2
    }
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func rateText(rate: Int, colorHighlight: UIColor) -> NSMutableAttributedString{
        var rateToUse = rate
        if rate > self.characters.count{
            rateToUse = (self.characters.count)
        }
        let mutableString = NSMutableAttributedString.init(string: self)
        let start = rateToUse == 0 ? 0 : rateToUse
        let length = self.characters.count - rateToUse
        mutableString.addAttributes([NSForegroundColorAttributeName : colorHighlight.withAlphaComponent(0.5)], range: NSRange.init(location: start, length: length))
        return mutableString
    }
    
    func formatteToDate(startPattern: String, returnPattern: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = startPattern
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = returnPattern
        return dateFormatter.string(from: date!)
    }
    
}
