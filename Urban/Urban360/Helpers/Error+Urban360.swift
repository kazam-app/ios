//
//  Error+Urban360.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

// MARK: - Error enumerations

/**
 Custom default error.
 */
enum DefaultError: Error {
    case unknownError
}

/**
 Custom error generated when request does not return data
 */
enum RequestError: Error {
    case noData
    case noHeaders
    case missingHeaderParameter(parameter: String)
}

/**
 Error in JSON server response
 
 Not in the JSON data itself!
 */
enum JSONResponseError: Error {
    case missingParameter(parameter: String)
    case wrongParamterType(parameter: String)
    case serverError(message: String)
    case invalidPage
}

/**
 Custom error generated when JSON parsing fails
 */
enum JSONError: Error {
    case parserMissingParameter(parameter: String)                          // expected parameter not part of parsed JSON
}
