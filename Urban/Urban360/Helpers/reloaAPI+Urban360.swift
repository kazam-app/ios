//
//  reloaAPI+Urban360.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/1/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
@objc protocol APIReloadData {
    @objc optional func reloadData()
}
