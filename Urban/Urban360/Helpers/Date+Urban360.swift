//
//  Date+Urban360.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//
import Foundation

enum STDateFormat: String {
    case date_only = "yyyy'-'MM'-'dd"
    case date_time = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
    case display_date_only = "MMMM' 'dd', 'yyyy"
    case display_date_time = "MMMM' 'dd', 'yyyy' - 'HH':'mm':'ss"
    case picker_date_format = "yyyy-MM-dd"
    case picker_hour_format = "h:mm a"
    case date_only_signup = "dd'/'MM'/'yyyy"
    case date_only_m_h = "MMM yyyy"
}


extension Date {
    
     func dateFromString(dateString: String, format: STDateFormat) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        let s = dateFormatter.date(from:dateString)
        print(s!)
        return s!
    }
    
    func ISOStringFromDateUTC(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        return dateFormatter.string(from: date as Date).appending("Z")
    }
    
    func ISOStringFromDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT")! as TimeZone
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        return dateFormatter.string(from: date as Date).appending("Z")
    }
    
    func ISOStringFromDateX(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT")! as TimeZone
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        return dateFormatter.string(from: date as Date)
    }
    
    func dateFromISOString(string: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT")! as TimeZone
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        return dateFormatter.date(from: string)!
    }
    
    func stDateString(format: STDateFormat) -> String {
        let formatter = DateFormatter()
        
        // formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = format.rawValue
        return formatter.string(from: self)
    }
    func timeAgo(date:Date) -> String {
        let now = date
        //    NSLog(@"NOW: %@", now);
        let deltaSeconds: Double = fabs(now.timeIntervalSinceNow)
        let deltaMinutes: Double = deltaSeconds / 60.0
        if deltaSeconds < 5 {
            return NSLocalizedString("dateKeyNow", comment: "")
        }
        else if deltaSeconds < 60 {
            return String(format: NSLocalizedString("dateKeySecondsAgo", comment: ""), Int(deltaSeconds))
        }
        else if deltaSeconds < 120 {
            return NSLocalizedString("dateKeyOneMinute", comment: "")
        }
        else if deltaMinutes < 60 {
            return String(format: NSLocalizedString("dateKeyMinutesAgo", comment: ""), Int(deltaMinutes))
        }
        else if deltaMinutes < 120 {
            return NSLocalizedString("dateKeyOneHour", comment: "")
        }
        else if deltaMinutes < (24 * 60) {
            return String(format: NSLocalizedString("dateKeyHoursAgo", comment: ""), Int(floor(deltaMinutes / 60)))
        }
        else if deltaMinutes < (24 * 60 * 2) {
            return NSLocalizedString("dateKeyYesterday", comment: "")
        }
        else if deltaMinutes < (24 * 60 * 7) {
            return String(format: NSLocalizedString("dateKeyDaysAgo", comment: ""), Int(floor(deltaMinutes / (60 * 24))))
        }
        else if deltaMinutes < (24 * 60 * 14) {
            return NSLocalizedString("dateKeyLastWeek", comment: "")
        }
        else if deltaMinutes < (24 * 60 * 31) {
            return String(format: NSLocalizedString("dateKeyWeeksAgo", comment: ""), Int(floor(deltaMinutes / (60 * 24 * 7))))
        }
        else if deltaMinutes < (24 * 60 * 61) {
            return NSLocalizedString("dateKeyLastMonth", comment: "")
        }
        else if deltaMinutes < (24 * 60 * 365.25) {
            return String(format: NSLocalizedString("dateKeyMonthsAgo", comment: ""), Int(floor(deltaMinutes / (60 * 24 * 30))))
        }
        else if deltaMinutes < (24 * 60 * 731) {
            return NSLocalizedString("dateKeyYearsAgo", comment: "")
        }
        
        return String(format: NSLocalizedString("dateKeyNow", comment: ""), Int(floor(deltaMinutes / (60 * 24 * 365))))
    }

}
