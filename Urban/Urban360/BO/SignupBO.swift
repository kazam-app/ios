//
//  SignupBO.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/26/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import  UIKit
import FBSDKLoginKit

class SignupBO {
  class  func singupFB(controller:UIViewController){
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile","email"], from: controller, handler: { (result, error) in
            if error != nil{
                print("Custom FB Login failed:", error!)
                return
            }
            getFBUserData(controller:controller)
        })
    }
   class func signupMail(controller:UIViewController) {
        let stb = UIStoryboard(name: "Profile", bundle: nil)
        let navController = stb.instantiateViewController(withIdentifier: "RegisterUserVC")
        controller.present(navController, animated: true, completion: nil)
    }
   class private func getFBUserData(controller:UIViewController){
        if FBSDKAccessToken.current() != nil {
            let parameters = ["fields": "id, name, first_name, last_name, picture.type(large), email"]
            FBSDKGraphRequest(graphPath: "me", parameters: parameters ).start(completionHandler: { (conection, result, error) in
                if error == nil{
                    let results = result as! NSDictionary
                    let name = results.value(forKey: "first_name")
                    let lastNames = results.value(forKey: "last_name")
                    let user =  UserModel()
                    user.name  = name as? String
                    user.lastNames = lastNames as? String
                    if let email:String = results.value(forKey: "email") as? String {
                        user.email = email
                    }
                    user.fbId = results.value(forKey: "id") as? String
                    user.fbToken = FBSDKAccessToken.current().tokenString
                    user.fbExpiresAt = FBSDKAccessToken.current().expirationDate.stDateString(format: STDateFormat.date_only_signup)
                    let stb = UIStoryboard(name: "Profile", bundle: nil)
                    let navController = stb.instantiateViewController(withIdentifier: "RegisterUserVC") as! UINavigationController
                    let registerForm = stb.instantiateViewController(withIdentifier: "RegisterUserLastVC") as! RegisterUserLastVC
                    navController.viewControllers = [registerForm]
                    registerForm.userLoginFacebook = UserModel()
                    registerForm.userLoginFacebook = user
                    controller.present(navController, animated: true, completion: nil)
                }
            })
        }
    }
}
