//
//  ValidationBO.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/28/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import KontaktSDK


class ValidationBO:NSObject {
    static let sharedInstance = ValidationBO()
    private override init() {}
    //var locationManager: CLLocationManager!
    var beaconManager: KTKBeaconManager!
    var isLocationEnabled:Bool = false
    var isvalidateReachabiliy = false
    
    
    /*func requestLocationWhenUse(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    func requestLocationAlways(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }*/
    
    func requestLocationWhenUse(){
        beaconManager = KTKBeaconManager(delegate: self)
        beaconManager.requestLocationWhenInUseAuthorization()
    }
    func requestLocationAlways(){
        beaconManager = KTKBeaconManager(delegate: self)
        beaconManager.requestLocationAlwaysAuthorization()
    }
    
}

/*extension ValidationBO:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    isLocationEnabled = true
                }
            }

            break
        case .authorizedWhenInUse:
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    isLocationEnabled = true
                }
            }
            break
        default:
            break
        }
    }
}*/

extension ValidationBO: KTKBeaconManagerDelegate{
    
    func beaconManager(_ manager: KTKBeaconManager, didChangeLocationAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            if KTKBeaconManager.isMonitoringAvailable() {
                if KTKBeaconManager.isRangingAvailable() {
                    isLocationEnabled = true
                }
            }
            
            break
        case .authorizedWhenInUse:
            if KTKBeaconManager.isMonitoringAvailable() {
                if KTKBeaconManager.isRangingAvailable() {
                    isLocationEnabled = true
                }
            }
            break
        default:
            break
        }
    }
    
}
