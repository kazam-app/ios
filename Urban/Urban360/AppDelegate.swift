//
//  AppDelegate.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/10/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import FBSDKCoreKit
import OneSignal
import IQKeyboardManagerSwift
import KontaktSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var reachabilityInternet: ReachabilityUtil!
    
    static let notificationNetworkOnName = "NotificationNetworkOn"
    static let notificationNetworkOffName = "NotificationNetworkOff"
    static let notificationApplicationDidBecomeActive = "notificationApplicationDidBecomeActive"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true
        Fabric.with([Crashlytics.self])
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        let tabBar:UITabBarController = self.window!.rootViewController as! UITabBarController
        tabBar.selectedIndex = 1;
         let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "d7641e0e-008a-4026-bf80-a6c48718d73e",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        UserDefaults.standard.removeObject(forKey: "merchantBrand")
        UserDefaults.standard.synchronize()
        
        Kontakt.setAPIKey("GruheEBwnUbzaODbMcOhXQFdEBpgkYHw")
        Tracker.trackEvent(event: Tracker.EVENT_APP_OPEN, parameters: nil)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        unsuscribeToReachability()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        suscribeToReachability()
        FBSDKAppEvents.activateApp()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppDelegate.notificationApplicationDidBecomeActive), object: nil)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        Tracker.trackEvent(event: Tracker.EVENT_APP_CLOSE, parameters: nil)
    }

    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        // Mark: Facebook
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options [UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        return handled
    }

    func validateSession(){
        if FBSDKAccessToken.current() != nil{
            
        }
    }
    
    //Mark Internet checker
    func suscribeToReachability(){
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        reachabilityInternet = ReachabilityUtil.reachabilityForInternetConnection()
        reachabilityInternet?.startNotifier()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(notification:)), name: NSNotification.Name(rawValue: "kNetworkReachabilityChangedNotification"), object: nil)
    }
    
    func unsuscribeToReachability(){
        reachabilityInternet.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "kNetworkReachabilityChangedNotification"), object: nil)
    }
    
    func reachabilityChanged(notification: Notification){
        let curReach = notification.object as! ReachabilityUtil
        updateInterface(reachability: curReach)
    }
    
    func updateInterface(reachability: ReachabilityUtil) {
        switch reachability.currentReachabilityStatus() {
        case NotReachable:
            print("No Connected...")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOffName), object: nil)
            break
        default:
            print("Connected To Internet...")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppDelegate.notificationNetworkOnName), object: nil)
            break
        }
    }
}

