//
//  PunchCardsViewController.swift
//  Urban360
//
//  Created by Hector Maya on 14/09/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PunchCardsViewController: UIViewController {
    
    @IBOutlet weak var pagerControl: UIPageControl!
    @IBOutlet weak var pagerContainer: UIView!
    @IBOutlet weak var emptyPunchCard: UIView!
    @IBOutlet weak var lblEmptyPunchCard: UILabel!
    
    var merchant:MerchantModel!
    let usrDefaults = UserDefaults.standard
    var activityIndicator : NVActivityIndicatorView!
    var punchsCards:[PunchCardModel] = []
    var isBluetoothScan = true    
    var isEmmbed = false
    
    var pager : UIPageViewController!
    var arrayControllers = [PunchCardViewController]()
    var index = 0
    
    var reedemLocalItems = [[String : Any?]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isEmmbed{
            let merchantDic:[String:AnyObject] = self.usrDefaults.object( forKey: "merchantBrand") as! [String : AnyObject]
            merchant = MerchantModel.toMerchantModel(merchantDic: merchantDic)
        }
        activityIndicator = activityIndicator(view: pagerContainer)
        if usrDefaults.dictionary(forKey: "userSession") != nil{
            if self.validateReachability(){
                loadPunchsCards()
            }
        }
    }
    
    func loadPunchsCards()  {
        self.activityIndicator.startAnimating()
        PunchsCardsManager.punchsCardsByMerchant(merchantId: merchant.merchantId!){ results, error in
            self.activityIndicator.stopAnimating()
            if error == nil {
                self.punchsCards = []
                self.punchsCards = results!
                self.emptyPunchCard.fadeOut(withDuration: 0.3)
                self.showPunchCards()
            }
            else{
                self.emptyPunchCard.fadeIn(withDuration: 0.5)
                self.lblEmptyPunchCard.text = NSLocalizedString("emptyPunchCard", comment: "emptyPunchCard").replacingOccurrences(of: "{MERCHANT}", with: self.merchant.name!)
            }
        }
    }
    
    
    func showPunchCards() {
        
        if self.pager == nil{
            self.pager = UIPageViewController.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
            self.pager?.delegate = self
            self.pager?.dataSource = self
            self.pager.view.backgroundColor = UIColor.clear
            self.pagerContainer.addSubview((self.pager?.view)!)
            self.pager?.view.frame = pagerContainer.bounds
        }
        self.addChildViewController(self.pager!)
        pager.view.setNeedsLayout()
        pager.view.updateConstraintsIfNeeded()
        
        if punchsCards.count == 0 {
            return
        }
        
        var counter = 0
        self.arrayControllers = []
        for punchCard in punchsCards{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PunchCardViewController") as! PunchCardViewController
            vc.indexViewController = counter
            arrayControllers.append(vc)
            vc.parentVC = self
            vc.punchCard = punchCard
            vc.merchant = merchant
            counter = counter + 1
        }
        
        //Gettin Local saved reedem
        if ReedemManager.getReedemList() != nil{
            reedemLocalItems = ReedemManager.getReedemList() as! [[String : Any?]]
            
            for reedemLocalItem in reedemLocalItems{
                let merchantId = reedemLocalItem[ReedemManager.KEY_REEDEM_MERCHANT_ID] as? String ?? "-1"
                if merchantId == merchant.merchantId{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PunchCardReedemViewController") as! PunchCardViewController
                    vc.indexViewController = counter
                    arrayControllers.append(vc)
                    vc.isReedem = true
                    vc.parentVC = self
                    vc.reedemPunch = reedemLocalItem
                    vc.delegate = self
                    vc.merchant = merchant
                    counter = counter + 1
                }
            }
        }
        

        pagerControl.numberOfPages = arrayControllers.count
        if arrayControllers.count > 0{
            self.pager?.setViewControllers([arrayControllers[arrayControllers.count - 1]], direction: UIPageViewControllerNavigationDirection.forward, animated: arrayControllers.count > 1 && pagerControl.currentPage != arrayControllers.count - 1, completion: nil)
            pagerControl.currentPage = arrayControllers.count - 1
        }
        
    }
    
}

extension PunchCardsViewController : UIPageViewControllerDelegate, UIPageViewControllerDataSource{
    //MARK UIPageViewControllerDelegate methods
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        index = (viewController as! PunchCardViewController).indexViewController
        
        if (index == -1) {
            return nil;
        }
        
        index += 1;
        if (index == arrayControllers.count) {
            return nil;
        }
        return self.viewControllerAtIndex()
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        index = (viewController as! PunchCardViewController).indexViewController
        index -= 1;
        if (index == -1) {
            return nil;
        }
        return self.viewControllerAtIndex()
    }
    
    /*func presentationCount(for pageViewController: UIPageViewController) -> Int {
     return (arrayControllers?.count)!
     }*/
    
    func viewControllerAtIndex() -> PunchCardViewController? {
        if (((arrayControllers.count) == 0) || (index >= (arrayControllers.count))) {
            return nil;
        }
        let vc  = arrayControllers[index]
        vc.indexViewController = index
        return vc
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool){
        
        if completed {
            let vc = pageViewController.viewControllers?.last as! PunchCardViewController
            pagerControl.currentPage = vc.indexViewController
        }
    }

}

extension PunchCardsViewController : ReedemTimeDelegate{

    func update() {
        loadPunchsCards()
    }
}

extension PunchCardsViewController : PunchCardDelegateUpdate{
    func updateView() {
        activityIndicator = activityIndicator(view: view)
        if self.validateReachability(){
            loadPunchsCards()
        }
    }
}
