//
//  APIRequest.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//
import Foundation
import Alamofire

struct APIRequest {
    static let newsURL = "http://news.u360.in/"
    static let baseURL = "http://50.112.50.215:3500/v1/"
    
    static let apiURL = baseURL
    var url: String?
    var method: HTTPMethod?
    var params: Parameters?
    var encoding: ParameterEncoding?
    var headers: HTTPHeaders?
    var token:String?
    
    // MARK: - initialization
    
    init(_ url: String, method: Alamofire.HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.httpBody
        , headers: HTTPHeaders? = nil) {
        self.url = url
        self.method = method
        self.params = parameters
        self.encoding = encoding
        self.headers = headers
    }
    
    init() {
    }
    
    // MARK: - request
    
    func send() -> Alamofire.DataRequest {
        return Alamofire.request(url!, method: method!, parameters: params, encoding: encoding!, headers: headers)
    }
    func cancelAllRequests() {
        print("cancelling NetworkHelper requests")
    }
    // MARK: - News
    
    static func getNews() -> APIRequest {
        return APIRequest(newsURL + "news/ios",
                          method: .get,
                          parameters:nil,
                          encoding:URLEncoding.queryString,
                          headers: nil)
    }
    // MARK: - About
    static func listFAQ()->APIRequest{
        return APIRequest(apiURL + "faq",
                          method:.get,
                          parameters:nil,
                          encoding:URLEncoding.queryString,
                          headers:nil)
        
    }
    
    static func listPolicy()->APIRequest{
        return APIRequest(apiURL + "policy",
                          method:.get,
                          parameters:nil,
                          encoding:URLEncoding.queryString,
                          headers:nil)
        
    }
    
    static func listTerms()->APIRequest{
        return APIRequest(apiURL + "terms",
                          method:.get,
                          parameters:nil,
                          encoding:URLEncoding.queryString,
                          headers:nil)
        
    }

     // MARK: - Profile
    
    static func validateSignup(params:[String:Any]) -> APIRequest{
        return APIRequest(apiURL + "check",
                          method:.post,
                          parameters: params,
                          encoding:JSONEncoding.default,
                          headers:nil)
    }
    
    static func signup(params:[String:Any]) -> APIRequest{
        return APIRequest(apiURL + "sign_up",
                          method:.post,
                          parameters: params,
                          encoding:JSONEncoding.default,
                          headers:nil)
    }
    
    static func updateAccount(params:[String:Any]) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "profile",
                          method:.put,
                          parameters: params,
                          encoding:JSONEncoding.default,
                          headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    static func getProfile() -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "profile",
                          method:.get,
                          parameters: nil,
                          encoding:JSONEncoding.default,
                          headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    static func updateMail(params:[String:Any]) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "profile/email",
                          method:.put,
                          parameters: params,
                          encoding:JSONEncoding.default,
                          headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    static func updatePassword(params:[String:Any]) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "profile/password",
                          method:.put,
                          parameters: params,
                          encoding:JSONEncoding.default,
                          headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    static func login(params:[String:Any]) -> APIRequest{
        return APIRequest(apiURL + "login",
                          method:.post,
                          parameters: params,
                          encoding:JSONEncoding.default,
                          headers:nil)
    }
    static func recoveryPassword(params:[String:Any]) -> APIRequest{
        return APIRequest(apiURL + "recovery",
                          method:.post,
                          parameters: params,
                          encoding:JSONEncoding.default,
                          headers:nil)
    }
    static func closeSession() -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "log_out",
            method:.delete,
            parameters:nil,
            encoding:URLEncoding.queryString,
            headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    //MARK: -Merchants
    static func listMerchants()->APIRequest{
        return APIRequest(apiURL + "merchants",
                          method:.get,
                          parameters: nil,
                          encoding:URLEncoding.queryString,
                          headers:nil)

    }
    static func listMerchantsNear(location:[String:Float]?)->APIRequest{
        return APIRequest(apiURL + "shops/near",
                          method:.get,
                          parameters:location,
                          encoding:URLEncoding.queryString,
                          headers:nil)
        
    }
    static func listPunchsCards()->APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "punch_cards",
                          method:.get,
                          parameters: nil,
                          encoding:URLEncoding.queryString,
                          headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
        
    }
    static func listPunchsCardsCompleted()->APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "punch_cards/ready",
                          method:.get,
                          parameters: nil,
                          encoding:URLEncoding.queryString,
                          headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    static func listShopsByMerchant (merchantId:String) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "merchants/\(merchantId)/shops",
                          method:.get,
                          parameters:nil,
                          encoding:URLEncoding.queryString,
                          headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
   
    static func merchantByQR (qrCode:String) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "merchants/qr/\(qrCode)",
            method:.get,
            parameters:nil,
            encoding:URLEncoding.queryString,
            headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    static func merchantById (merchantId:String) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "merchants/\(merchantId)",
            method:.get,
            parameters:nil,
            encoding:URLEncoding.queryString,
            headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    static func merchantByBeacon (beaconId:String,beaconMajor:String) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        let params = ["uid":beaconId,"major":beaconMajor]
        return APIRequest(apiURL + "merchants/beacon",
            method:.get,
            parameters:params,
            encoding:URLEncoding.queryString,
            headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    static func punchsCardsByMerchant (merchantId:String) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "merchants/\(merchantId)/punch_cards",
            method:.get,
            parameters:nil,
            encoding:URLEncoding.queryString,
            headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    static func punchCard(merchantId:String,punchCard:PunchCardModel,QRToken:String?,beaconId:String?,beaconMajor:String?,beaconMinor:String?) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        var params:[String:Any] = [:]
        if let qrCode:String = QRToken{
            params = ["id":punchCard.punchCardId!,"merchant_id":merchantId,"token":qrCode]
        }
        else{
            params = ["id":punchCard.punchCardId!,"merchant_id":merchantId,"uid":beaconId!,"major":beaconMajor!,"minor":beaconMinor!]
        }
        return APIRequest(apiURL + "merchants/\(merchantId)/punch/\(punchCard.punchCardId!)",
            method:.post,
            parameters:params,
            encoding:JSONEncoding.default,
            headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    static func reedemPunchCard(merchantId:String,punchCard:PunchCardModel,QRToken:String?,beaconId:String?,beaconMajor:String?,beaconMinor:String?) -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        var params:[String:Any] = [:]
        if let qrCode:String = QRToken{
            params = ["id":punchCard.punchCardId!,"merchant_id":merchantId,"token":qrCode]
        }
        else{
            params = ["id":punchCard.punchCardId!,"merchant_id":merchantId,"uid":beaconId!,"major":beaconMajor!,"minor":beaconMinor!]
        }
        
        return APIRequest(apiURL + "merchants/\(merchantId)/redeem/\(punchCard.punchCardId!)",
            method:.post,
            parameters:params,
            encoding:JSONEncoding.default,
            headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    // MARK: Beacons
    //beacons/regions
    static func listRegionsBeacons() -> APIRequest{
        let usrDefaults = UserDefaults.standard
        let userDic = usrDefaults.dictionary(forKey: "userSession")
        return APIRequest(apiURL + "beacons/regions",
            method:.get,
            parameters:nil,
            encoding:URLEncoding.queryString,
            
            
            headers: ["Authorization": "Bearer \(userDic!["sessiontoken"] as! String)"])
    }
    
    // MARK: Update
    static func updateApp() -> APIRequest{
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        return APIRequest(apiURL + "version?version=\(version)",
                          method:.get,
                          parameters:nil,
                          encoding:URLEncoding.queryString,
                          headers: nil)
    }
    
    static func cancelAllRequest(){
        return APIRequest.cancelAllRequest()
    }
}
