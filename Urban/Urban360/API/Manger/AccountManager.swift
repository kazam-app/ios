//
//  AccountManager.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/13/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AccountManager {
    
    
    class func validateUser(user:UserModel,completion: @escaping (Bool?, Error?) -> Void) {
        let request = APIRequest.validateSignup(params: UserModel.toJSON(user: user))
        request.send().responseJSON { response in
            parseResponsePassrecovery(response: response, completion: completion)
        }
        
    }
    
    class func getAccount(completion: @escaping (UserModel?, Error?) -> Void){
        let request = APIRequest.getProfile()
        request.send().responseJSON { response in
            parseResponse(response: response, completion: completion)
        }
    }
    
    class func createAccount(user:UserModel,completion: @escaping (UserModel?, Error?, String?) -> Void) {
        
        let request = APIRequest.signup(params: UserModel.toJSON(user: user))
        request.send().responseJSON { response in
            parseResponseCreateAccount(response: response, completion: completion)
            //parseResponse(response: response, completion: completion)
        }

    }
    
    class func updateAccount(user:UserModel,completion: @escaping (UserModel?, Error?) -> Void) {
        
        let request = APIRequest.updateAccount(params: UserModel.toJSON(user: user))
        request.send().responseJSON { response in
            parseResponse(response: response, completion: completion)
        }
        
    }
    
    class func updateMail(mail:String,password:String, completion: @escaping (Bool?, Error?, String?) -> Void) {
        let  json:[String:AnyObject] = ["user":["email":mail ,"password" : password] as AnyObject]
        let request = APIRequest.updateMail(params: json)
        request.send().responseJSON { response in
            parseResponseUpdateMAil(response: response, completion: completion)
        }
        
    }

    
    class func updatePassword(oldPassword:String, newPassword:String, confirmPassword:String,completion: @escaping (Bool?, Error?) -> Void) {
        var  json:[String:AnyObject] = ["old_password":oldPassword as AnyObject]
        json["password"] = newPassword as AnyObject
        json["password_confirmation"] = confirmPassword as AnyObject
        let request = APIRequest.updatePassword(params: json)
        request.send().responseJSON { response in
            parseResponsePassrecovery(response: response, completion: completion)
        }
        
    }
    
    
    class func updateApp(completition: @escaping (Bool?) -> Void) {
        let request = APIRequest.updateApp()
        request.send().responseJSON { (response) in
            if response.result.error == nil{
                guard let value = response.result.value else {
                    completition(false)
                    return
                }
                do {
                    let json = JSON(value)
                    let update = json["update"].null == nil ? json["update"].bool : false
                    completition(update)
                } catch {
                    completition(false)
                }
            }else{
                completition(false)
            }
        }
    }

    
    class func login(user:UserModel,completion: @escaping (UserModel?, Error?) -> Void) {
        let request = APIRequest.login(params: UserModel.loginToJSON(user: user))
        request.send().responseJSON { response in
            parseResponse(response: response, completion: completion)
        }
    }
    class func recoveryPassword(email:[String:String],completion: @escaping (Bool?, Error?) -> Void) {
        let request = APIRequest.recoveryPassword(params: email)
        request.send().responseJSON { response in
            parseResponsePassrecovery(response: response, completion: completion)
        }
    }
    class func closeSession(completion: @escaping (Bool?, Error?, Int?) -> Void) {
        let request = APIRequest.closeSession()
        request.send().responseJSON { response in
            parseResponseCloseSession(response: response, completion: completion)
        }
    }
    
    class private func parseResponseCloseSession(response: DataResponse<Any>, completion: @escaping (Bool?, Error?, Int?) -> Void) {
        
        
        guard response.result.error == nil else {
            completion(nil,response.result.error, response.response?.statusCode)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData, response.response?.statusCode)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parseResultsRecoveryJSON(json: json)
            completion(results,nil, response.response?.statusCode)
        } catch {
            completion(nil, error, response.response?.statusCode)
        }
    }
    
    // MARK: helper
    
    class private func parseResponse(response: DataResponse<Any>, completion: @escaping (UserModel?, Error?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parseResultsJSON(json:json)
            completion(results,nil)
        } catch {
            completion(nil, error)
        }
    }
    
    class private func parseResponseCreateAccount(response: DataResponse<Any>, completion: @escaping (UserModel?, Error?, String?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error, nil)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData, nil)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parseResultsJSON(json:json)
            completion(results,nil, nil)
        } catch {
            let json = JSON(value)
            let message = json["message"].null == nil ? json["message"].string : nil
            completion(nil, error, message)
        }
    }
    
    class private func parseResponsePassrecovery(response: DataResponse<Any>, completion: @escaping (Bool?, Error?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parseResultsRecoveryJSON(json: json)
            completion(results,nil)
        } catch {
            completion(nil, error)
        }
    }
    
    class private func parseResponseUpdateMAil(response: DataResponse<Any>, completion: @escaping (Bool?, Error?, String?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error, "")
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData, "")
            return
        }
        do {
            let json = JSON(value)
            //try  checkResponseJSON(json:json)
            let results = parseResultsRecoveryJSON(json: json)
            let message = json["message"].stringValue
            completion(results,nil, message)
        } catch {
            completion(nil, error, "")
        }
    }

    
    class private func checkResponseJSON(json: JSON) throws {
        let data = json["code"]
        guard !data.exists() else {
            let errors = json["errors"]
            if errors.exists() {
                throw JSONResponseError.serverError(message: errors[0].string ?? "No error message")
            } else {
                throw JSONResponseError.missingParameter(parameter: "data")
            }
        }
    }
    
    class private func parseResultsJSON(json: JSON) -> UserModel{
        let userSession = UserModel.fromJSON(json: json)
        return userSession
    }
    class private func parseResultsRecoveryJSON(json: JSON) -> Bool{
        let recovery = json.boolValue
        return recovery
    }
}
