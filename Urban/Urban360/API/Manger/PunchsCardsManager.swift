 //
//  PunchsCardsManager.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/23/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PunchsCardsManager {
    
    // MARK: appointment
    
    
    class func getAllPunchsCards(completion: @escaping ([PunchCardListModel]?, Error?) -> Void) {
        
        let request = APIRequest.listPunchsCards()
        request.send().responseJSON { response in
            parseResponse(response: response, completion: completion)
        }
    }
    class func getAllPunchsCardsCompleted(completion: @escaping ([PunchCardListModel]?, Error?) -> Void) {
        
        let request = APIRequest.listPunchsCardsCompleted()
        request.send().responseJSON { response in
            parseResponse(response: response, completion: completion)
        }
    }
    
    class func punchsCardsByMerchant(merchantId:String,completion: @escaping ([PunchCardModel]?, Error?) -> Void) {
        
        let request = APIRequest.punchsCardsByMerchant(merchantId: merchantId)
        request.send().responseJSON { response in
            parsePunchsCardsResponse(response: response, completion: completion)
        }
    }
    class func punchCard(merchantId:String,QRToken:String?,beaconId:String?,beaconMajor:String?,beaconMinor:String?,punchCard:PunchCardModel,completion: @escaping (PunchCardModel?, Error?, Int?) -> Void) {
        let request = APIRequest.punchCard(merchantId: merchantId, punchCard: punchCard, QRToken: QRToken, beaconId: beaconId, beaconMajor: beaconMajor, beaconMinor: beaconMinor)
        request.send().responseJSON { response in
            parsePunchCardResponse(response: response, completion: completion)
        }
    }
    
    class func reedemPunchCard(merchantId:String,QRToken:String?,beaconId:String?,beaconMajor:String?,beaconMinor:String?,punchCard:PunchCardModel,completion: @escaping (PunchCardModel?, Error?, Int?) -> Void) {
        let request = APIRequest.reedemPunchCard(merchantId: merchantId, punchCard: punchCard, QRToken: QRToken, beaconId: beaconId, beaconMajor: beaconMajor, beaconMinor: beaconMinor)
        request.send().responseJSON { response in
            parsePunchCardResponse(response: response, completion: completion)
        }
    }
    
    class func getAllBeacons(completion: @escaping ([BeaconModel]?, Error?) -> Void) {
        
        let request = APIRequest.listRegionsBeacons()
        request.send().responseJSON { response in
            parseBeaconRegionsResponse(response: response, completion: completion)
        }
    }
    
    
    // MARK: helper
    class private func parseResponse(response: DataResponse<Any>, completion: @escaping ([PunchCardListModel]?, Error?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parseResultsJSON(json:json)
            completion(results,nil)
        } catch {
            completion(nil, error)
        }
    }
    
    class private func checkResponseJSON(json: JSON) throws {
        if  let data = json.array {
            guard (!data.isEmpty) else {
                let errors = json["errors"]
                if errors.exists() {
                    throw JSONResponseError.serverError(message: errors[0].string ?? "No error message")
                } else {
                    throw JSONResponseError.missingParameter(parameter: "data")
                }
            }
        }
        else {
            if let data = json["code"].string{
                let errors = json["errors"]
                if errors.exists() {
                    throw JSONResponseError.serverError(message: errors[0].string ?? "No error message")
                } else {
                    throw JSONResponseError.missingParameter(parameter: data)
                }
            }
        }
    }
    
    class private func parsePunchsCardsResponse(response: DataResponse<Any>, completion: @escaping ([PunchCardModel]?, Error?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parsePunchsCardsResultsJSON(json:json)
            completion(results,nil)
        } catch {
            completion(nil, error)
        }
    }
    
    class private func parsePunchCardResponse(response: DataResponse<Any>, completion: @escaping (PunchCardModel?, Error?, Int?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error, response.response?.statusCode)
            return
        }
        //check status valid_limit
        guard let value = response.result.value else {
            completion(nil,RequestError.noData, response.response?.statusCode)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parsePunchCardResultsJSON(json:json)
            completion(results,nil,response.response?.statusCode)
        } catch {
            completion(nil, error, response.response?.statusCode)
        }
    }
    
    class private func parseBeaconRegionsResponse(response: DataResponse<Any>, completion: @escaping ([BeaconModel]?, Error?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parseBeaconRegionsResultsJSON(json:json)
            completion(results,nil)
        } catch {
            completion(nil, error)
        }
    }
    
    
    class private func parseResultsJSON(json: JSON) -> [PunchCardListModel]{
        var results:[PunchCardListModel] = []
        for punchCardJSON in json.array! {
            let punchCard = PunchCardListModel.fromJSON(json: punchCardJSON)
            results.append(punchCard)
        }
        return results
    }
    
    class private func parsePunchsCardsResultsJSON(json: JSON) -> [PunchCardModel]{
        var results:[PunchCardModel] = []
        for punchCardJSON in json.array! {
            let punchCard = PunchCardModel.fromJSON(json: punchCardJSON)
            results.append(punchCard)
        }
        return results
    }
    class private func parsePunchCardResultsJSON(json: JSON) -> PunchCardModel{
        var results:PunchCardModel!
         results = PunchCardModel.fromJSON(json: json)
        return results
    }
    
    class private func parseBeaconRegionsResultsJSON(json: JSON) -> [BeaconModel]{
        var results:[BeaconModel] = []
        for beaconJSON in json.array! {
            let beacon = BeaconModel.fromJSON(json: beaconJSON)
            results.append(beacon)
        }
        return results
    }
}
