//
//  MerchantManager.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 6/20/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class MerchantManager {
    
    // MARK: appointment
   
    
    class func getListMerchants(completion: @escaping ([MerchantModel]?, Error?) -> Void) {
        
        let request = APIRequest.listMerchants()
        request.send().responseJSON { response in
            parseResponse(response: response, completion: completion)
        }
    }
    
    
    
    class func getListMerchantsNear(location:[String:Float]?,completion: @escaping ([MerchantLocationModel]?, Error?) -> Void) {
        let request = APIRequest.listMerchantsNear(location: location)
        request.send().responseJSON { response in
            parseMerchantLocationResponse(response: response, completion: completion)
        }
    }
    
    class func getListShopsByMerchant(merchantId:String,completion:@escaping([MerchantLocationModel]?,Error?) -> Void){
        let request = APIRequest.listShopsByMerchant(merchantId: merchantId)
        request.send().responseJSON { response in
            parseMerchantLocationResponse(response: response, completion: completion)
        }
    }
    
    class func merchantById(merchantId:String,completion:@escaping(MerchantModel?,Error?) -> Void){
        let request = APIRequest.merchantById(merchantId: merchantId)
        request.send().responseJSON { response in
            parseResponseMerchantQRBeacon(response: response, completion: completion)
        }
    }
    
    class func merchantByQR(qrCode:String,completion:@escaping(MerchantModel?,Error?) -> Void){
        let request = APIRequest.merchantByQR(qrCode: qrCode)
        request.send().responseJSON { response in
            parseResponseMerchantQRBeacon(response: response, completion: completion)
        }
    }

    class func merchantByBeacon(beaconId:String,beaconMajor:String,completion:@escaping(MerchantModel?,Error?) -> Void){
        let request = APIRequest.merchantByBeacon(beaconId: beaconId, beaconMajor: beaconMajor)
        request.send().responseJSON { response in
            parseResponseMerchantQRBeacon(response: response, completion: completion)
        }
    }

    // MARK: helper
    class private func checkResponseJSON(json: JSON) throws {
        if  let data = json.array {
            guard (!data.isEmpty) else {
                let errors = json["errors"]
                if errors.exists() {
                    throw JSONResponseError.serverError(message: errors[0].string ?? "No error message")
                } else {
                    throw JSONResponseError.missingParameter(parameter: "data")
                }
            }
        }
        else {
            if let data = json["code"].string{
                let errors = json["errors"]
                if errors.exists() {
                    throw JSONResponseError.serverError(message: errors[0].string ?? "No error message")
                } else {
                    throw JSONResponseError.missingParameter(parameter: data)
                }

            }
            
        }
    }
    
    class private func checkResponseMerchantJSON(json: JSON) throws {
        let data = json["code"]
        guard !data.exists() else {
            let errors = json["errors"]
            if errors.exists() {
                throw JSONResponseError.serverError(message: errors[0].string ?? "No error message")
            } else {
                throw JSONResponseError.missingParameter(parameter: "data")
            }
        }
    }

    
    class private func parseResponse(response: DataResponse<Any>, completion: @escaping ([MerchantModel]?, Error?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parseResultsJSON(json:json)
            completion(results,nil)
        } catch {
            completion(nil, error)
        }
    }
    
    
    class private func parseResultsJSON(json: JSON) -> [MerchantModel]{
        var results:[MerchantModel] = []
        for merchantsJSON in json.array! {
            let merchant = MerchantModel.fromJSON(json: merchantsJSON)
            results.append(merchant)
        }
        return results
    }
    
    class private func parseMerchantLocationResponse(response: DataResponse<Any>, completion: @escaping ([MerchantLocationModel]?, Error?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parseMerchantLocationResultsJSON(json:json)
            completion(results,nil)
        } catch {
            completion(nil, error)
        }
    }
    
    
    class private func parseMerchantLocationResultsJSON(json: JSON) -> [MerchantLocationModel]{
        var results:[MerchantLocationModel] = []
        for merchantsJSON in json.array! {
            let merchant = MerchantLocationModel.fromJSON(json: merchantsJSON)
            results.append(merchant)
        }
        return results
    }

    class private func parseResponseMerchantQRBeacon(response: DataResponse<Any>, completion: @escaping (MerchantModel?, Error?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseMerchantJSON(json:json)
            let results = parseResultsMerchantJSON(json:json)
            completion(results,nil)
        } catch {
            completion(nil, error)
        }
    }
    
    
    class private func parseResultsMerchantJSON(json: JSON) -> MerchantModel{
        var merchant:MerchantModel!
        merchant = MerchantModel.fromJSON(json: json)
        return merchant
    }

    
}

