//
//  NewsManager.swift
//  Urban360
//
//  Created by sp4rt4n_0 on 5/16/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class NewsManager {
    
    // MARK: appointment
    
    
    class func getNews(completion: @escaping ([NewModel]?, Error?) -> Void) {
        
        let request = APIRequest.getNews()
        request.send().responseJSON { response in
            parseResponse(response: response, completion: completion)
        }
    }
    
    // MARK: helper
    
    class private func parseResponse(response: DataResponse<Any>, completion: @escaping ([NewModel]?, Error?) -> Void) {
        guard response.result.error == nil else {
            completion(nil,response.result.error)
            return
        }
        guard let value = response.result.value else {
            completion(nil,RequestError.noData)
            return
        }
        do {
            let json = JSON(value)
            try  checkResponseJSON(json:json)
            let results = parseResultsJSON(json:json)
            completion(results,nil)
        } catch {
            completion(nil, error)
        }
    }
    
    class private func checkResponseJSON(json: JSON) throws {
        if  let data = json.array {
            guard (!data.isEmpty) else {
                let errors = json["errors"]
                if errors.exists() {
                    throw JSONResponseError.serverError(message: errors[0].string ?? "No error message")
                } else {
                    throw JSONResponseError.missingParameter(parameter: "data")
                }
            }
        }
        else {
            if let data = json["code"].string{
                let errors = json["errors"]
                if errors.exists() {
                    throw JSONResponseError.serverError(message: errors[0].string ?? "No error message")
                } else {
                    throw JSONResponseError.missingParameter(parameter: data)
                }
                
            }
            
        }
    }
    
    class private func parseResultsJSON(json: JSON) -> [NewModel]{
        var results:[NewModel] = []
        for newsJSON in json.array! {
            let new = NewModel.fromJSON(json: newsJSON)
            results.append(new)
        }
        return results
    }
    
}
