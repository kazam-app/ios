//
//  PunchCardViewController.swift
//  Urban360
//
//  Created by Hector Maya on 14/09/17.
//  Copyright © 2017 Urban360. All rights reserved.
//

import UIKit

protocol ReedemTimeDelegate {
    func update()
}

class PunchCardViewController: UIViewController {
    
    var punchCard:PunchCardModel!
    var merchant:MerchantModel!
    var isReedem = false
    var parentVC : PunchCardsViewController!
    var timer : Timer!
    var delegate: ReedemTimeDelegate!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnPunchCard: UIButton!
    @IBOutlet weak var btnReedem: UIButton!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var internalView: UIView!
    
    @IBOutlet weak var firstViewPunchHeight: NSLayoutConstraint!
    @IBOutlet weak var secondViewPunchHeight: NSLayoutConstraint!
    @IBOutlet weak var thirdViewPunchHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTimeRemaning: UILabel!
    @IBOutlet weak var lblReedemCode: UILabel!
    
    @IBOutlet weak var btnReedemHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var btnSellHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var bottomPunchsConstaint: NSLayoutConstraint!
    @IBOutlet weak var topPunchsConstaint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentCardView: UIView!
    var isComplet = false
    var popupVC:PopupVC!
    var punchVC:PunchInitialVC!
    var reedemPunch: [String : Any?]!
    
    let sb = UIStoryboard(name: "General", bundle: nil)
    let sbPunch = UIStoryboard(name: "PunchsCards", bundle: nil)
    
    var indexViewController = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !isReedem{
            
            if UIDevice.current.screenType == .iPhone5{
                btnReedemHeightContraint.constant = 32
                btnSellHeightContraint.constant = 32
                bottomPunchsConstaint.constant = 8
                topPunchsConstaint.constant = 8
            }else if UIDevice.current.screenType == .iPhone6 ||  UIDevice.current.screenType == .iPhone6Plus{
                firstViewPunchHeight.constant = 42
                secondViewPunchHeight.constant = 42
                thirdViewPunchHeight.constant = 42
            }else{
                firstViewPunchHeight.constant = 46
                secondViewPunchHeight.constant = 46
                thirdViewPunchHeight.constant = 46
            }
            
            lblTitle.text = punchCard.prize!
            let punchCount = Int(punchCard.punchCount ?? 0)
            let limit = Int(punchCard.punchLimit ?? 0) + 1
            let maxNumber = 16
            for i in 1 ..< maxNumber {
                if view.viewWithTag(i) != nil{
                    let img = view.viewWithTag(i) as! UIImageView
                    img.image = UIImage.init(named: i <= punchCount ? "punchCardFull" : "punchCardEmpty")
                    img.alpha = i < limit ? 1.0 : 0.0
                }
            }
            

            //imgPunch.image = #imageLiteral(resourceName: "punchCardFull")
            //imgPunch.image = #imageLiteral(resourceName: "punchCardEmpty")
            //Custom height for punch
            if punchCard.punchLimit! < 6{
                secondViewPunchHeight.constant = 0
                thirdViewPunchHeight.constant = 0
            }else if punchCard.punchLimit! < 11{
                thirdViewPunchHeight.constant = 0
            }
            
            //From tools
            btnPunchCard.backgroundColor = UIColor.uiTealish
            btnReedem.backgroundColor = UIColor.uigray
            if self.punchCard.punchCount == self.punchCard.punchLimit {
                btnPunchCard.backgroundColor = UIColor.uigray
                btnReedem.backgroundColor = UIColor.uiTealish
                isComplet = true
            }
            self.popupVC = sb.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
            self.popupVC.modalPresentationStyle = .overFullScreen
            self.popupVC.popupDelegate = self as PopupDelegate
        }else{
            let punchCardPrize = reedemPunch[ReedemManager.KEY_REEDEM_TITLE] as! String
            let code = reedemPunch[ReedemManager.KEY_REEDEM_CODE] as! String
            lblTitle.text = punchCardPrize
            lblReedemCode.text = code
        }

        
        // corner radius
        cardView.layer.cornerRadius = 10
        internalView.layer.cornerRadius = 10
        
        // border
        cardView.layer.borderWidth = 1.0
        cardView.layer.borderColor = UIColor.clear.cgColor
        
        // shadow
        cardView.layer.shadowColor = UIColor.darkGray.cgColor
        cardView.layer.shadowOffset = CGSize(width: 2, height: 2)
        cardView.layer.shadowOpacity = 0.5
        cardView.layer.shadowRadius = 5
        
        scrollView.contentSize = CGSize.init(width: self.view.bounds.width, height: contentCardView.bounds.size.height)
        //scrollView.contentInset = UIEdgeInsets.init(top: 10, left: 0, bottom: 0, right: 0);
        //scrollView.contentOffset = CGPoint.init(x: 0, y: -10)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isReedem{
            updateUI()
            startTimer()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isReedem{
            stopTimer()
        }
    }
    
    func startTimer() {
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateUI), userInfo: nil, repeats: true)
    }
    
    func updateUI() {
        var message = ""
        let time = reedemPunch[ReedemManager.KEY_REEDEM_TIME] as! Double
        if ReedemManager.isValidTimeReedem(time: time){
            let remaining = ReedemManager.MAX_SECONDS_REEDEM - (NSDate().timeIntervalSince1970 - time)
            let minutes = Int((remaining.truncatingRemainder(dividingBy: 3600)) / 60)
            let seconds = Int(remaining.truncatingRemainder(dividingBy: 3600).truncatingRemainder(dividingBy: 60))
            
            let minutesStr = String(minutes)
            let secondsStr = seconds < 10 ? ("0" + String(seconds)) : String(seconds)
            
            message = "Esta tarjeta desaparecerá en:\n" + minutesStr + ":" + secondsStr
        }else{
            message = "Esta tarjeta desaparecerá en:\n0:00"
            DispatchQueue.main.async {
                self.stopTimer()
                if self.delegate != nil{
                    self.delegate.update()
                }
            }
        }
        lblTimeRemaning.text = message
    }
    
    func stopTimer(){
        if timer != nil{
            timer.invalidate()
            timer = nil
        }
    }
    
    @IBAction func punchAction(_ sender: Any) {
        if isComplet{
            self.popupVC.titlePopup = "Tu Tarjeta está Completa"
            self.popupVC.detailPopup = "¡Felicidades, ya puedes canjear esta tarjeta por tu premio!"
            self.popupVC.isSingle = true
            parentVC.present(self.popupVC, animated: true, completion: nil)
        }
        else{
            let nowDateStr = Date().ISOStringFromDateUTC(date: Date())
            let now = Date().dateFromISOString(string: nowDateStr)
            if punchCard.validationDate != nil && (now.timeIntervalSince1970 < (punchCard.validationDate?.timeIntervalSince1970)!) {
                self.popupVC = self.sb.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
                self.popupVC.titlePopup = "¡Oops! Llegaste al Límite"
                self.popupVC.detailPopup = "Has llegado al límite de sellos que puedes obtener de esta marca hoy, vuelve mañana."
                self.popupVC.modalPresentationStyle = .overFullScreen
                self.popupVC.popupDelegate = self as PopupDelegate
                self.present(self.popupVC, animated: true, completion: nil)
                return
            }
            
            punchVC = sbPunch.instantiateViewController(withIdentifier: "PunchInitialVC") as! PunchInitialVC
            punchVC.currentPunchCard = punchCard
            punchVC.currentMerchant = merchant
            //punchVC.delegateUpdate = parentVC as! PunchCardDelegateUpdate!
            //punchVC.modalPresentationStyle = .overCurrentContext
            punchVC.punchCardDelegate = self as PunchCardDelegate
            parentVC.present(punchVC, animated: true, completion: nil)
        }
    }
    

    @IBAction func reedemAction(_ sender: Any) {
        if isComplet{
            
            punchVC = sbPunch.instantiateViewController(withIdentifier: "PunchInitialVC") as! PunchInitialVC
            punchVC.currentPunchCard = punchCard
            punchVC.currentMerchant = merchant
            //punchVC.modalPresentationStyle = .overCurrentContext
            punchVC.isReedem = true
            //punchVC.delegateUpdate = parentVC as! PunchCardDelegateUpdate!
            punchVC.punchCardDelegate = self as PunchCardDelegate
            self.present(punchVC, animated: true, completion: nil)
        }
        else{
            self.popupVC.titlePopup = "Completa tu Tarjeta"
            self.popupVC.detailPopup = "Asegúrate de tener todos los sellos necesarios antes de canjear tu premio."
            self.popupVC.isSingle = true
            parentVC.present(self.popupVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func conditionsAction(_ sender: Any) {
        let termsNC = sbPunch.instantiateViewController(withIdentifier: "PunchCardRulesVC") as! UINavigationController
        let termsVC = termsNC.viewControllers[0] as! PunchCardRulesVC
        if isReedem{
            let punchCardTmp = PunchCardModel.init()
            let expires = reedemPunch[ReedemManager.KEY_REEDEM_END_DATE_PROMOTION] as! String
            let rules = reedemPunch[ReedemManager.KEY_REEDEM_RULES_PROMOTION] as! String
            let conditions = reedemPunch[ReedemManager.KEY_REEDEM_CONDITIONS_PROMOTION] as! String
            punchCardTmp.expiresAt = expires
            punchCardTmp.rules = rules
            punchCardTmp.terms = conditions
            termsVC.punchCard = punchCardTmp
        }else{
            termsVC.punchCard = punchCard
        }
        //termsNC.modalPresentationStyle = .overCurrentContext
        parentVC.present(termsNC, animated: true, completion: nil)
    }
}

extension PunchCardViewController : PopupDelegate{
    func confirmSingle() {
        self.popupVC.dismiss(animated: true, completion: nil)
    }
}

extension PunchCardViewController : PunchCardDelegate{
    func reedem() {
        
    }
}
